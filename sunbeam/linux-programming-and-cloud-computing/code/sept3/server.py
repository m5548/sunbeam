from flask import Flask, render_template, request
from regression import predict_salary_from_experience


# create a flask application
app = Flask(__name__)


# route: http method and path
@app.route("/", methods=["GET"])
def root():
    # return "<h1>Welcome to Flask</h1>"
    return render_template("index.html")


# this function will be executed when user enter
# experience and hits the submit button
@app.route("/predict", methods=["POST"])
def predict_salary():
    # get the experience entered by user
    experience = request.form.get("experience")

    # predict the user salary
    salary = predict_salary_from_experience(experience)

    return f"<h1>You have experience of {experience} yrs</h1><h2>You may get a salary = {salary} rs.</h2>"


# start the flask web server
app.run(host="0.0.0.0", port=3000, debug=True)
