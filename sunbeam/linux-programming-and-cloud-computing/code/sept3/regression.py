import pandas as pd


def predict_salary_from_experience(experience):

    df = pd.read_csv('Salary_Data.csv')

    # decide x and y
    x = df.drop('Salary', axis=1)
    y = df['Salary']

    # split the data into train and test set
    from sklearn.model_selection import train_test_split

    x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.8, random_state=123456)

    from sklearn.linear_model import LinearRegression

    # create a model
    model = LinearRegression()

    # train the model
    # create the formula by studying the relationship between x_train and y_train
    model.fit(x_train, y_train)

    predictions = model.predict([[experience]])
    return predictions[0]
