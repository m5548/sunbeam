#!/usr/bin/bash

# get user input
read -p "enter number of files to be created: " num
read -p "enter the directory where the files should be created: " directory

# check if directory exists
if [ -e $directory ]; then
	echo "directory exists"
else
	echo "creating directory"
	mkdir $directory
fi

for (( index=0; index<$num; index++ )); do
	file="$directory/file_${index}"
	echo "creating file $file"
	touch $file
done
