#!/usr/bin/bash

# variable declaration
num=100


# get the value of the num
echo $num

# print(f"num = {num}")
echo "num = $num"
echo "num =" $num


# discard the variable
unset num

echo "num = $num"
