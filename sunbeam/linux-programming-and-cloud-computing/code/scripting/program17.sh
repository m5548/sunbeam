#!/usr/bin/bash

# parameterless function declaration
# def function1():
function1() {
	echo "inside function1"
}

# calling a function
# function1


# parameterized function
# def function2(param1, param2): 
function2() {
	param1=$1
	param2=$2

	echo "inside function2"
	echo "param1 = $param1"
	echo "param2 = $param2"
}

# calling a function with parameters
# function2(10, 20)
# function2 10 20


