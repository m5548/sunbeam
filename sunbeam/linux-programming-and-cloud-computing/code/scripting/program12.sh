#!/usr/bin/bash

file="/tmp/test/myfile"

# check if file is readable
if [ -r $file ]; then
	echo "file is readable"
else
	echo "file is not readable"
fi


# check if file is writable
if [ -w $file ]; then
	echo "file is writable"
else
	echo "file is not writable"
fi


# check if file is executable
if [ -x $file ]; then
	echo "file is executable"
else
	echo "file is not executable"
fi

