#!/usr/bin/bash

directory="/tmp/test2"
backup_file="/tmp/backup.tar"

# check if directory exists
if [ -e $directory ]; then
	echo "directory exists"
else
	mkdir -p $directory
fi


# check if the backup file exists
if [ -e $backup_file ]; then
	echo "backup file exists"
	rm $backup_file
fi

# tar the entire directory
tar -cvf $backup_file $directory
