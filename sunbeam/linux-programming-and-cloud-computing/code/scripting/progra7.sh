#!/usr/bin/bash

echo "-- math operations --"

echo "parameters: $@"

# addition
# addtion = p1 + p2
addition=$(expr $1 + $2)
echo "Addition = $addition"

multiplication=$(expr $1 \* $2)
echo "multiplication = $multiplication"

subtraction=$(expr $1 - $2)
echo "Subrtaction = $subtraction"
