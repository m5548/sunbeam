#!/usr/bin/bash

echo "-- math operations --"

# check if user has passed any parameter
echo "number of parameters: $#"
if [ $# -eq 2 ]
then
	echo "parameters: $@"

	# addition
	# addtion = p1 + p2
	addition=$(expr $1 + $2)
	echo "Addition = $addition"

	multiplication=$(expr $1 \* $2)
	echo "multiplication = $multiplication"

	subtraction=$(expr $1 - $2)
	echo "Subrtaction = $subtraction"
else
	echo "you need to pass two parameters"
fi


