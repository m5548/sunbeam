#!/usr/bin/bash

file="/tmp/test/myfile"

# check if a file exists in /tmp/test/
if [ -e $file ]; then
	echo "file exists"
else
	echo "file does not exist, creating a new file"
	
	# create an empty file
	touch $file
fi			


# check if the file is empty or not
if [ -s $file ]; then
	echo "the file is non empty"
else
	echo "the file is empty"
fi


# check if the file is a file or directory
if [ -f $file ]; then
	echo "this is a file"
else
	echo "this is not a file"
fi

# check if the file is a directory
if [ -d $file ]; then
	echo "this is a directory"
else
	echo "this is not a directory"
fi
