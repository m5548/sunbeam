#!/usr/bin/bash

str1="test1"
str2="test2"


# check if str1 == str2
if [ $str1 = $str2 ]
then
	echo "$str1 == $str2"
else
	echo "$str1 != $str2"
fi


str3="test3"

# check if str3 is empty or not
if [ -z $str3 ]
then
	echo "zero characters"
else
	echo "non zero characters"
fi
