#!/usr/bin/bash

if [ $# -eq 2 ]
then
	if [ $1 -gt $2 ]
	then
		echo "$1 > $2"
	else
		echo "$2 > $1"
	fi
else
	# get input from user
	read -p "enter first number: " p1
	read -p "enter second number: " p2

	echo "p1 = $p1"
	echo "p2 = $p2"

	if [ $p1 -gt $p2 ]
	then
		echo "$p1 > $p2"
	else
		echo "$p2 > $p1"
	fi
fi
