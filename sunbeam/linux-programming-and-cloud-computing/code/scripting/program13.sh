#!/usr/bin/bash

# array

# variable
num=100

echo "num = $num"

# array
array=( 10 20 30 40 50 60 70 80 90 100 )

# prints only first value
echo "array = $array"

# prints all the values in the array
echo "array = ${array[@]}"

echo "all the values in the array: "
#echo ${array[0]}
#echo ${array[1]}
#echo ${array[2]}
#echo ${array[3]}
#echo ${array[4]}
#echo ${array[5]}
#echo ${array[6]}
#echo ${array[7]}
#echo ${array[8]}
#echo ${array[9]}

# array iteration
for value in ${array[@]}
do
	echo "value = $value"
done


















