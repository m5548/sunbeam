#!/usr/bin/bash

add1() {
	echo "inside add1 function"
	addition=$(expr $1 + $2)
	echo "addition = $addition"
}

#add1 10 20
#add1 30 40
#add1 50 60


add2() {
	echo "inside add2 function"

	# return the addition	
	return $(expr $1 + $2)
}

# call the function with required parameters
add2 10 20

# get the return value
echo "addition of 10 and 20 = $?"



multiply() {
	echo "inside multiplication"

	return $(expr $1 \* $2)
}

# call multiply function
multiply 3 5

# get the return value of multiply
echo "multiplication of 3 and 5 = $?"








