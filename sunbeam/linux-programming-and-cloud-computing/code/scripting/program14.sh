#!/usr/bin/bash

# files
files=( file1 file2 file3 file4 file5 )

# create every file in /tmp/test
for file in ${files[@]}; do
	touch "/tmp/test/$file"
done
