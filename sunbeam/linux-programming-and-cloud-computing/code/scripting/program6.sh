#!/usr/bin/bash

echo "--- Shell variables ---"
echo "program name: $0"
echo "process id: $$"
echo "number of command line parameters: $#"
echo "command line arguments: $@"
echo "command line arguments: $*"
echo "first argument: $1"
echo "second argument: $2"
