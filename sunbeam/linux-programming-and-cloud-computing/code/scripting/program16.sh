#!/usr/bin/bash

read -p "how many users you want to create: " num

for (( index=0; index<$num; index++ )); do
	user="user_$index"
	echo "creating user $user"

	# home directory
	home="/home/$user"

	# create user first	
	sudo useradd $user --home-dir $home --shell /usr/bin/bash

	# create home directory for the user
	sudo mkdir $home
	
	# let the user own the directory
	sudo chown $user:$user $home

done
