# import the required packages
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

%matplotlib inline

x = np.array([1, 2, 3, 4, 5])
y = np.array([1, 4, 9, 16, 25])

# find covariace
np.cov(x, y)

# find correlation
np.corrcoef(x, y)

# plot a simple graph
plt.plot(x, y, label="relation")
plt.legend()
plt.xlabel('X')
plt.ylabel('Y')
plt.title("X vs Y")
plt.tight_layout()

df = pd.DataFrame([
        {"x": 1, "y": 1},
        {"x": 2, "y": 4},
        {"x": 3, "y": 9},
        {"x": 4, "y": 16},
        {"x": 5, "y": 25},
])

# find relationship (correlation)
c= orr df.corr()
print(corr)

sns.heatmap(corr)

sns.pairplot(df)