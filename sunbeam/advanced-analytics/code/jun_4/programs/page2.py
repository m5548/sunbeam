# import required packages
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

x = np.array([3, 4, 8, 7, 2])
y = np.array([11, 12, 9, 3, 5])

def calculate_byx(x, y):
    sum_xy = sum(x * y)
    sum_x = sum(x)
    sum_y = sum(y)
    sum_x_square = sum(x ** 2)
    square_sum_x = sum_x ** 2
    n = x.size
    byx = ((n * sum_xy) - (sum_x * sum_y)) / ((n * sum_x_square) - square_sum_x)
    print(f"byx = {byx}")
    return byx


byx = calculate_byx(x, y)
mean_x = x.mean()
mean_y = y.mean()

new_value_x = 10
y = byx * (new_value_x - mean_x) + mean_y
print(f"y = {y}")