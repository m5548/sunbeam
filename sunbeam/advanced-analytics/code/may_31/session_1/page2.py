import numpy as np


def variance(*args):
    # create an array
    x = np.array(args)

    # find the mean
    mean = x.mean()

    # find the sum of square of difference between every value and mean
    numerator = sum([np.square(value - mean) for value in x])

    # divide the sum by total number of values
    var = numerator / x.size
    print(f"variance = {var}")

    # get variance from np
    print(f"variance = {x.var()}")
    print(f"variance = {np.var(x)}")


# variance(1, 2, 3, 4, 5)


def standard_deviation(*args):
    # create an array
    x = np.array(args)

    # find the mean
    mean = x.mean()

    # find the sum of square of difference between every value and mean
    numerator = sum([np.square(value - mean) for value in x])

    # divide the sum by total number of values
    std = np.sqrt(numerator / x.size)
    print(f"standard deviation = {std}")

    # get std from np
    print(f"standard deviation = {x.std()}")
    print(f"standard deviation = {np.std(x)}")


standard_deviation(1, 2, 3, 4, 5)