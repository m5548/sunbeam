import numpy as np
import pandas as pd

# import matplotlib
import matplotlib.pyplot as plt

exp = [1.5, 2, 2.2, 3, 3.4, 5.2, 5.5, 6]
salaries = [20, 25, 27, 30, 35, 40, 50, 60]
expenses = np.array([10, 15, 17, 20, 21, 25, 30, 35])


plt.style.use('seaborn')
plt.plot(exp, salaries, label="Salaries", c="green")
plt.plot(exp, expenses, label="Expenses", c="red")
plt.legend()
plt.xlabel("Experience")
plt.ylabel("Salary and Expense")
plt.title("Exp vs Salary and Expense")
plt.tight_layout()


plt.show()