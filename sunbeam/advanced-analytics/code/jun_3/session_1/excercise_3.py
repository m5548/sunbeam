import numpy as np
import pandas as pd

# import matplotlib
import matplotlib.pyplot as plt

# x = np.array([1, 2, 3, 4, 5, 6])
# y = np.repeat([0.16], 6)
# y = np.random.binomial(n=10, p=0.5, size=100)

# y.sort()

y = np.random.poisson(5, 100)
x = np.arange(y.size)

# plt.bar(x, y, label="probability")
plt.hist(y, bins=20, density=True)
plt.legend()
plt.tight_layout()
plt.show()

