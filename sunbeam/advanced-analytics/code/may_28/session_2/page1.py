def mean1(*args):
    sum_of_values = 0
    for value in args:
        sum_of_values += value

    mean = sum_of_values / len(args)
    print(f"mean = {mean}")


# mean1(10, 20, 30, 40, 50, 60, 70)


def mean2(*args):
    sum_of_values = sum(args)
    mean = sum_of_values / len(args)
    print(f"mean = {mean}")


# mean2(10, 20, 30, 40, 50, 60, 70)


def mean3(*args):
    print(f"mean = {sum(args)/len(args)}")


# mean3(10, 20, 30, 40, 50, 60, 70)


def mean4(*args):
    import numpy as np

    a = np.array(args)
    print(f"mean = {np.mean(a)}")
    print(f"mean = {a.mean()}")


mean4(10, 20, 30, 40, 50, 60, 70)

