def single_mode(*args):
    # find the unique values
    unique_values = set(args)

    # dictionary collecting the frequencies of every unique value
    frequencies = {}

    for value in unique_values:
        # find the frequency of evey unique value
        frequencies[value] = args.count(value)

    print(frequencies)

    mode, max_frequency = 0, 0
    for key in frequencies.keys():
        # get frequency of every value
        value_frequency = frequencies[key]

        # check if the frequency is more than the max frequency
        if value_frequency > max_frequency:
            # we found the mode
            mode = key
            max_frequency = value_frequency

    print(f"mode = {mode} appeared {max_frequency} times")


# single_mode(10, 20, 10, 10, 20, 40, 50, 50)


def multi_mode(*args):
    # find the unique values
    unique_values = set(args)

    # dictionary collecting the frequencies of every unique value
    frequencies = {}

    for value in unique_values:
        # find the frequency of evey unique value
        frequencies[value] = args.count(value)

    print(frequencies)

    modes = []
    mode, max_frequency = 0, 0
    for key in frequencies.keys():
        # get frequency of every value
        value_frequency = frequencies[key]

        # check if the frequency is more than the max frequency
        if (value_frequency > max_frequency) or (value_frequency == max_frequency):
            # we found the mode
            mode = key
            max_frequency = value_frequency
            modes.append({
                "mode": key,
                "max_frequency": value_frequency
            })

    print(f"modes = {modes}")
    print(f"modes = {list(map(lambda x: x['mode'], modes))}")


# multi_mode(10, 20, 20, 20, 10, 50, 10, 25, 60)


def no_mode(*args):
    # find the unique values
    unique_values = set(args)

    # dictionary collecting the frequencies of every unique value
    frequencies = {}

    for value in unique_values:
        # find the frequency of evey unique value
        frequencies[value] = args.count(value)

    print(frequencies)

    modes = []
    mode, max_frequency = 0, 0
    for key in frequencies.keys():
        # get frequency of every value
        value_frequency = frequencies[key]

        # check if the frequency is more than the max frequency
        if (value_frequency > max_frequency) or (value_frequency == max_frequency):
            # we found the mode
            mode = key
            max_frequency = value_frequency
            modes.append({
                "mode": key,
                "max_frequency": value_frequency
            })

    if len(modes) == len(args):
        print("No Mode")
        print(f"modes = {list(map(lambda x: x['mode'], modes))}")


no_mode(10, 20, 30, 40, 50, 60, 70, 80)