def median_odd(*args):
    # get the values in sorted order
    sorted_values = sorted(args)

    # find the middle position
    middle_position = (len(args) + 1) // 2

    # value at the middle position is the median
    print(f"median = {sorted_values[middle_position - 1]}")


median_odd(10, 20, 19, 20, 5, 4, 6, 8, 8)


def median_even(*args):
    # get the values in sorted order
    sorted_values = sorted(args)

    # find the middle position
    middle_position = (len(args) + 1) // 2

    # get the average of middle and the next value
    median = (sorted_values[middle_position] + sorted_values[middle_position - 1]) / 2

    # value at the middle position is the median
    print(f"median = {median}")


# median_even(10, 2, 6, 3, 4, 6, 9, 10, 11)

