# declare a variable with name num and value 100
# <variable name> = <initial value>
# all data types are inferred
# - automatically set by python
# - by looking at the value stored inside the variable

# integer
num = 100

# output => num = 100
# print(f"num = {num}")
# print(f"type of num = {type(num)}")
print(f"num = {num}, type of num = {type(num)}")

# float
salary = 15.50
print(f"salary = {salary}, type of salary = {type(salary)}")

# string
# single line
name = "sunbeam"
print(f"name = {name}, type of name = {type(name)}")

# string
# single line
phone = '+9178897997'
print(f"phone = {phone}, type of phone = {type(phone)}")

# string
# multi line
address = """house no - 100,
nanded city,
sinhgad road,
pune"""
print(f"address = {address}, type of address = {type(address)}")

ch = 'a'
print(f"ch = {ch}, type of ch = {type(ch)}")

# boolean
can_vote = False
print(f"can_vote = {can_vote}, type of can_vote = {type(can_vote)}")
