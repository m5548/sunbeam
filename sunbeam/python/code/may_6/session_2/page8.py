def function1(param):
    print("inside function1")
    print(f"param = {param}")


# positional parameters
# function1(10)

# named parameters
# function1(param=10)


def is_even(number):
    if number % 2 == 0:
        print("even")
    else:
        print("no even")


# positional
# is_even(10)

# named
# is_even(number=10)


def add(n1, n2):
    print(f"{n1} + {n2} = {n1 + n2}")


# n1 = 10, n2 = 20
add(10, 20)

# n1 = 20, n2 = 10
add(20, 10)

# n1 = 10, n2 = 20
add(n1=10, n2=20)

# n1 = 10, n2 = 20
add(n2=20, n1=10)

add(n1="test1", n2="test2")