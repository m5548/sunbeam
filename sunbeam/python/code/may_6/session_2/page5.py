# logical operators

# True and True = True
# True and False = False
# False and True = False
# False and False = False
print(f"True and True = {True and True}")
print(f"True and False = {True and False}")
print(f"False and True = {False and True}")
print(f"False and False = {False and False}")

print()

# True or True = True
# True or False = True
# False or True = True
# False or False = False
print(f"True or True = {True or True}")
print(f"True or False = {True or False}")
print(f"False or True = {False or True}")
print(f"False or False = {False or False}")