# functions

# empty function declaration + definition
# function with a empty body
def empty_function():
    """this is a empty function added to show how a function can do nothing"""

    # dont do anything here
    # no operation
    pass


# function invocation/call
# empty_function()


# parameterless function
# which accepts no parameters
def function1():
    print("inside function1")


# function call
# function1()


# parameterized function
# which accepts at least one parameter
def function2(param):
    print(f"inside function2")
    print(f"param = {param}, type = {type(param)}")


# function2(10)
# function2("test string")
# function2(10.59)
# function2(False)
# function2("@#$!$!@!@#$@$@$@#$")

# 1: accept a number and check if the number is even
def is_even(number):
    if number % 2 == 0:
        print(f"{number} is even")
    else:
        print(f"{number} is NOT even")


# is_even(10)


# 2: accept a number and check if the number is odd
def is_odd(number):
    if number % 2 != 0:
        print(f"{number} is odd")
    else:
        print(f"{number} is NOT odd")


# is_odd(10)
# is_odd(11)

# 3: accept age and check if person can vote
def can_vote(age):
    if age >= 18:
        print("Congrats, you are eligible for voting..")
    else:
        print("oops, you are NOT eligible for voting..")


# can_vote(10)
# can_vote(20)


def add(n1, n2):
    print("inside add")
    addition = n1 + n2
    print(f"addition = {addition}")


# add(10, 20)
add("test1", "test2")