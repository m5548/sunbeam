# operators

str1 = "sunbeam"
str2 = "institute"

# math operators with string params

# string concatinition
print(f"{str1} + {str2} = {str1 + ' ' + str2}")

# repeatition
print(f"{str1} * 5 = {str1 * 5}")
print('-' * 50)
print('-*-' * 20)
print((str1 + str2) * 2)

print()

n1 = 100
n2 = 40

# math operations with numbers

# addition
print(f"{n1} + {n2} = {n1 + n2}")

# subtraction
print(f"{n1} - {n2} = {n1 - n2}")

# multiplication
print(f"{n1} * {n2} = {n1 * n2}")

# true division
# returns float value
print(f"{n1} / {n2} = {n1 / n2}")

# floor division
# returns integer value
print(f"{n1} // {n2} = {n1 // n2}")

# power of
print(f"{n1} to the power 2 = {n1 ** 2}")
print(f"{n2} to the power 3 = {n2 ** 3}")

# modulo operator
print(f"{n1} % {n2} = {n1 % n2}")

print()
