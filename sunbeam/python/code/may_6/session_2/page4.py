# comparison operators

n1 = 100
n2 = 50

print(f"{n1} == {n2} = {n1 == n2}")
print(f"{n1} != {n2} = {n1 != n2}")
print(f"{n1} < {n2} = {n1 < n2}")
print(f"{n1} > {n2} = {n1 > n2}")
print(f"{n1} <= {n2} = {n1 <= n2}")
print(f"{n1} >= {n2} = {n1 >= n2}")


num = 100
print(f"num = {num}")

num = 200
print(f"num = {num}")

num = 400
print(f"num = {num}")
