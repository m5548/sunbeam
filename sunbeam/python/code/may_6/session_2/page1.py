# int
num = 100
print(f"num = {num}, type = {type(num)}")

# float
salary = 16.50
print(f"salary = {salary}, type = {type(salary)}")

# str
name = "steve"
print(f"name = {name}, type = {type(name)}")

# bool
can_vote = True
print(f"can_vote = {can_vote}, type = {type(can_vote)}")
