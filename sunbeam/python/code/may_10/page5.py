# tuple


def function1():
    # list
    list1 = [10, 20, 30, 40, 50]
    print(f"list1 = {list1}, type = {type(list1)}")

    # list
    list2 = list([10, 20, 30, 40, 50])
    print(f"list2 = {list2}, type = {type(list2)}")

    # tuple
    tuple1 = (10, 20, 30, 40, 50)
    print(f"tuple1 = {tuple1}, type = {type(tuple1)}")

    # tuple
    tuple2 = tuple([10, 20, 30, 40, 50])
    print(f"tuple2 = {tuple2}, type = {type(tuple2)}")


# function1()


def function2():
    # list with one item
    list1 = [10]
    print(f"list1 = {list1}, type = {type(list1)}")

    list2 = ["india"]
    print(f"list2 = {list2}, type = {type(list2)}")

    # not a tuple with one item
    # instead python considers int_value as a variable of int
    # int_value = (10)

    # tuple with one item
    tuple1 = (10, )
    print(f"tuple1 = {tuple1}, type = {type(tuple1)}")

    # not a tuple with one item
    # instead python considers str_value as a variable of str
    # str_value = ("india")

    tuple2 = ("india", )
    print(f"tuple2 = {tuple2}, type = {type(tuple2)}")


# function2()


def function3():
    numbers = (10, 20, 30, 40, 50)
    print(numbers)

    # append is not supported by tuple
    # numbers.append(60)
    # print(numbers)

    print(f"length of numbers = {len(numbers)}")

    position = numbers.index(30)
    print(f"value 30 is present on {position} position")

    count = numbers.count(10)
    print(f"value 10 is present {count} times")


function3()