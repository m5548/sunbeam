def function1():
    numbers = [20, 10, 4, 2, 41, 30, 20, 10, 12]

    # sort the numbers collection
    # sorts the same collection in ascending order
    # numbers.sort()
    # print(numbers)

    # sorts the same collection in descending order
    numbers.sort(reverse=True)
    print(numbers)


# function1()


def function2():
    numbers = [20, 10, 4, 2, 41, 30, 20, 10, 12]
    print(numbers)

    # reverse the numbers collection
    # reserves the same collection
    numbers.reverse()
    print(numbers)


# function2()


def function3():
    numbers = [20, 10, 4, 2, 41, 30, 20, 10, 12]
    print(numbers)

    # create a new copy of the numbers
    numbers_copy = numbers.copy()
    numbers_copy.sort()
    print(numbers_copy)

    print(numbers)


# function3()


def function4():
    numbers = [20, 10, 4, 2, 41, 30, 20, 10, 12]
    print(numbers)

    # remove all values from the collection
    numbers.clear()

    print(numbers)


# function4()


def function5():
    numbers = [20, 10, 4, 2, 41, 30, 20, 10, 12]
    print(numbers)

    # remove value 4 from original position
    index = numbers.index(4)
    numbers.pop(index)
    print(numbers)

    # and add it in between 10 and 12
    numbers.insert(7, 4)
    print(numbers)


# function5()


def function6():
    numbers1 = [10, 20, 30, 40, 50]
    numbers2 = [60, 70, 80, 90, 100]

    print(numbers1)
    print(numbers2)

    # add numbers2 into numbers1
    # numbers1.append(numbers2)

    # add all the values from one collection to another collection
    # numbers1.extend(numbers2)
    # print(numbers1)
    # print(f"length of numbers1 = {len(numbers1)}")

    # create new collection by adding elements from numbers1 and numbers2 collection
    numbers3 = numbers1 + numbers2
    print(numbers3)


function6()
