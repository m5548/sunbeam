# indexing


def function1():
    # list
    numbers = [10, 20, 30, 40, 50]

    # positive indexing
    print(f"value at 0th position = {numbers[0]}")
    print(f"value at 1st position = {numbers[1]}")
    print(f"value at 2nd position = {numbers[2]}")
    print(f"value at 3rd position = {numbers[3]}")
    print(f"value at 4th position = {numbers[4]}")


# function1()


def function2():
    # list
    numbers = [10, 20, 30, 40, 50]

    # negative indexing
    print(f"value at -1st position = {numbers[-1]}")
    print(f"value at -2nd position = {numbers[-2]}")
    print(f"value at -3rd position = {numbers[-3]}")
    print(f"value at -4th position = {numbers[-4]}")
    print(f"value at -5th position = {numbers[-5]}")


# function2()


def function3():
    # list
    numbers = [10, 20, 30, 40, 50]

    print(f"first value = {numbers[0]} or {numbers[-len(numbers)]}")
    print(f"last value = {numbers[len(numbers) - 1]} or {numbers[-1]}")


function3()


