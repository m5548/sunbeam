# slicing
# - getting a portion of a collection

def function1():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    # [30, 40, 50, 60, 70]
    final_numbers = []
    for index in range(2, 7):
        final_numbers.append(numbers[index])

    print(numbers)
    print(final_numbers)


# function1()


def function2():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    # [2, 3, 4, 5, 6] => range(2, 7)
    print(f"numbers[2:7] = {numbers[2:7]}")

    print()

    # [10, 20, 30, 40, 50]
    print(f"numbers[0:5] = {numbers[0:5]}")
    print(f"numbers[:5]  = {numbers[:5]}")

    print()

    # [60, 70, 80, 90, 100]
    print(f"numbers[5:10] = {numbers[5:10]}")
    print(f"numbers[5:]   = {numbers[5:]}")

    print()

    # [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    print(f"numbers[0:10] = {numbers[0:10]}")
    print(f"numbers[:]    = {numbers[:]}")


# function2()


def function3():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    # [10, 30, 50, 70, 90]
    final_numbers = []
    for index in range(0, 10, 2):
        final_numbers.append(numbers[index])

    print(numbers)
    print(final_numbers)


# function3()


def function4():
    numbers = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    # [10, 30, 50, 70, 90]
    # get values at even positions
    print(f"numbers[0:10:2] = {numbers[0:10:2]}")
    print(f"numbers[:10:2]  = {numbers[:10:2]}")
    print(f"numbers[0::2]   = {numbers[0::2]}")
    print(f"numbers[::2]    = {numbers[::2]}")

    # get values at odd positions
    print(f"numbers[1:10:2] = {numbers[1:10:2]}")
    print(f"numbers[1::2]   = {numbers[1::2]}")

    # get all the values in reverse order
    print(f"reversed = {numbers[::-1]}")


function4()
