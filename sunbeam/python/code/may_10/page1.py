def function1():
    numbers = [10, 20, 30, 40, 50]
    print(f"size of numbers = {len(numbers)}")
    print(numbers)

    # append a value
    numbers.append(60)
    print(numbers)

    # insert a value 45 between 40 and 50
    numbers.insert(4, 45)
    print(numbers)

    # removing last element
    numbers.pop()
    print(numbers)

    # removing an element on 2nd index
    numbers.pop(2)
    print(numbers)

    # remove a value 10
    numbers.remove(10)
    print(numbers)


# function1()


def function2():
    numbers = [10, 20, 30, 40, 50, 60, 20, 20, 30, 10, 20, 30, 40]
    print(numbers)

    # remove value 20
    numbers.remove(20)
    print(numbers)


# function2()


def function3():
    numbers = [10, 20, 30, 40, 50, 60, 20, 20, 30, 10, 20, 30, 40]
    print(numbers)

    # how many times the value 20 is repeated in the collection?
    print(f"value 20 got repeated {numbers.count(20)} times")

    # find the index of 20
    # print(f"20 is present on {numbers.index(20)} index")
    print(f"20 is present on {numbers.index(20, 0)} index")
    print(f"20 is present on {numbers.index(20, 2)} index")
    print(f"20 is present on {numbers.index(20, 7)} index")
    print(f"20 is present on {numbers.index(20, 8)} index")

    # error as value 20 does not exist after 11th position
    # print(f"20 is present on {numbers.index(20, 11)} index")


# function3()


def function4():
    numbers = [10, 20, 30, 40, 50, 60, 20, 20, 30, 10, 20, 30, 40]
    print(numbers)

    # find the count of value 20
    count = numbers.count(20)

    position = 0
    for _ in range(count):

        # get the position of value 20
        position = numbers.index(20, position)
        print(f"value 20 is present on {position} index")

        # continue searching value 20 after this position
        position += 1


# function4()


def function5():
    numbers = [10, 20, 30, 40, 50, 60, 20, 20, 30, 10, 20, 30, 40]
    print(numbers)

    # find the number of times 20 exists in the numbers
    count = numbers.count(20)

    # delete all occurrances of 20 using index() and count()

    # using remove()
    # for index in range(count):
    #     numbers.remove(20)
    # print(numbers)

    # using pop()
    for index in range(count):
        position = numbers.index(20)
        numbers.pop(position)
    print(numbers)


# function5()

