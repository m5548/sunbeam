class Person:

    # initializer
    def __init__(self):
        print("inside __init__")
        self.name = ""
        self.address = ""
        self.age = 0

    def set_details(self, name, address, age):
        self.name = name
        self.address = address
        self.age = age

    def print_details(self):
        print(f"mame = {self.name}")
        print(f"address = {self.address}")
        print(f"age = {self.age}")


# step 1: allocates the memory for the object
# step 2: calls the method __init__() on the newly allocated object
#         initializes the object [p1.__init__()]
p1 = Person()
# p1.set_details("person1", "pune", 40)
p1.print_details()


p2 = Person()
p2.print_details()