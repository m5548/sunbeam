class Person:
    def __init__(self, name='', age=0):
        # public attributes
        # - can be accessed outside of the class
        # self.name = name
        # self.age = age

        # private attributes
        # - can be accessed only within the same class
        # - name mangling: changing the attribute/data member name implicitly
        self.__name = name
        self.__age = age

    def print_details(self):
        # print(f"name = {self._Person__name}")
        print(f"name = {self.__name}")

        # print(f"name = {self._Person__age}")
        print(f"age = {self.__age}")
        print()

    def set_age(self, age):
        if (age > 25) and (age < 60):
            self.__age = age
        else:
            print("invalid age")

        # self.print_details()


p1 = Person('person1', 40)
print(p1.__dict__)
p1.print_details()
p1.__age = 40000
p1.print_details()
# p1._Person__age = 5000
p1.set_age(40000)
# p1.print_details()
print(p1.__dict__)
