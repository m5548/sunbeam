# __init__()
# - initializer: used to initialize the object
# - every class can have one and only one initializer
# - if explicit __init__() is not provided, python adds a default one
# - gets called implicitly (automatically)
# - types
#   - default: parameterless
#   - custom : parameterized

class Car:

    def __init__(self, model='', company='', price=0):
        # attributes: will get added to the object
        # can be accessed anywhere in the class
        self.model = model
        self.company = company
        self.price = price

        # local variable: scope local
        # num = 10

        # will get called at the time
        # of creating/initializing  an object
        self.my_method()

    def my_method(self):
        print("inside my_method")

    def print_details(self):
        print(f"model = {self.model}")
        print(f"company = {self.company}")
        print(f"price = {self.price}")

        # can NOT access variable declared in other method(s)
        # print(f"num = {num}")


# object's memory gets allocated
# object gets initialized
# Car.__init__(c1, "nano", "tata", 2.5)
# c1.__init__("nano", "tata", 2.5)
c1 = Car("nano", "tata", 2.5)
print(f"c1 = {c1.__dict__}")
c1.print_details()

c2 = Car()
c2.print_details()