# create a Mobile class
# attributes: model, cpu, ram
# methods: __init__, print_details

class Mobile:

    def __init__(self, model='', cpu='', ram='0GB'):
        self.model = model
        self.cpu = cpu
        self.ram = ram

    def print_details(self):
        print(f"model = {self.model}")
        print(f"cpu = {self.cpu}")
        print(f"ram = {self.ram}")


iPhone = Mobile('iPhone XS max', 'A12', '6GB')
print(iPhone.__dict__)
iPhone.print_details()
