# create a class Car
# - private attributes : model, company, price
# - methods = __init__, print_details
# - create an object with "nano", "tata", 2.5
# - modify the price to 3.5


class Car:

    # initializer
    def __init__(self, model, company, price):
        self.__model = model
        self.__company = company
        self.__price = price

    # facilitator
    def print_details(self):
        print(f"model = {self.__model}")
        print(f"company = {self.__company}")
        print(f"price = {self.__price}")
        print()

    # setter
    def set_price(self, price):
        if price < 0:
            print(f"price can not be negative")
        else:
            self.__price = price


c1 = Car('nano', 'tata', 2.5)
c1.print_details()

# can not change the private member outside the class
# c1.__price = 3.5
c1.set_price(3.5)
c1.print_details()

c1.set_price(-3.5)
c1.print_details()
