class Person:

    # method
    def set_person_details(self, name, address, email):
        # add attributes to the object
        setattr(self, "name", name)
        setattr(self, "address", address)
        setattr(self, "email", email)

    # method
    def print_person_details(self):
        # get the attribute values
        print(f"name = {getattr(self, 'name')}")
        print(f"address = {getattr(self, 'address')}")
        print(f"email = {getattr(self, 'email')}")
        print()


steve = Person()

# Person.set_person_details(steve, "steve jobs", "usa", "steve@apple.com")
steve.set_person_details("steve jobs", "usa", "steve@apple.com")

# Person.print_person_details(steve)
steve.print_person_details()

print()

bill = Person()

# Person.set_person_details(bill, "bill gates", "usa", "bill@ms.com")
bill.set_person_details(address="usa", name="bill gates", email="bill@ms.com")

# Person.print_person_details(bill)
bill.print_person_details()
