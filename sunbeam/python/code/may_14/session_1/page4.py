# create a class for Car
# - attributes: model, company, price, fuel_type
# - methods: set_details, print_details, can_afford (price < 10)

class Car:

    def set_details(self, model, company, price, fuel_type):
        setattr(self, "model", model)
        setattr(self, "company", company)
        setattr(self, "price", price)
        setattr(self, "fuel_type", fuel_type)

    def print_details(self):
        print(f"model = {getattr(self, 'model')}")
        print(f"company = {getattr(self, 'company')}")
        print(f"price = {getattr(self, 'price')}")
        print(f"fuel_type = {getattr(self, 'fuel_type')}")

    def can_afford(self):
        if getattr(self, "price") <= 10:
            print(f"Yes {getattr(self, 'model')} is affordable :)")
        else:
            print(f"Nope {getattr(self, 'model')} is NOT affordable :(")


car1 = Car()
car1.set_details("nano", "tata", 2.5, "petrol")
car1.print_details()
car1.can_afford()
