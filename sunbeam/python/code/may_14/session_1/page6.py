class Person:

    # please please please call this method
    # immediately after creating an object .....
    def set_details(self, name, address, age):
        self.name = name
        self.address = address
        self.age = age

    def print_details(self):
        print(f"name = {self.name}")
        print(f"address = {self.address}")
        print(f"age = {self.age}")
        print()

    def can_vote(self):
        if self.age >= 18:
            print(f"Yes.. {self.name} can vote")
        else:
            print(f"Nope.. {self.name} can NOT vote")


p1 = Person()
name = input("Enter your name: ")
address = input("Enter your address: ")
age = int(input("Enter your age: "))
# p1.set_details(name, address, age)
p1.print_details()
p1.can_vote()