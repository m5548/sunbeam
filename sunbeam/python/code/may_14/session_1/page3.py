class Mobile:

    def set_details(self, model, company, price):
        setattr(self, "model", model)
        setattr(self, "company", company)
        setattr(self, "price", price)

    def print_details(self):
        print(f"model = {getattr(self, 'model')}")
        print(f"company = {getattr(self, 'company')}")
        print(f"price = {getattr(self, 'price')}")


# object of Mobile class
m1 = Mobile()
m1.set_details("iPhone XS max", "Apple", 144000)
m1.print_details()

m2 = Mobile()
m2.set_details("One Plus 8", "One Plus", 49000)
m2.print_details()

