# what changes goes inside object       : data
# what does not change goes into class  : function

# class definition
class Person:
    pass


# please please please do not call this function
# by passing other than Person class object
def set_person_details(person, name, address, email):
    # add attributes to the object
    setattr(person, "name", name)
    setattr(person, "address", address)
    setattr(person, "email", email)


def print_person_details(person):
    # get the attribute values
    print(f"name = {getattr(person, 'name')}")
    print(f"address = {getattr(person, 'address')}")
    print(f"email = {getattr(person, 'email')}")
    print()


# instantiation
# - creating an instance/object of the class
# Person(): creates object in the memory
# person1 : reference
steve = Person()
# print(f"person1 = {person1}, type = {type(person1)}")
# print(f"contents of person1 = {person1.__dict__}")

set_person_details(steve, "steve jobs", "usa", "steve@apple.com")
print_person_details(steve)

bill = Person()
set_person_details(bill, "bill gates", "usa", "bill@ms.com")
print_person_details(bill)

# set_person_details("test", "person3", "satara", "person3@test.com")
