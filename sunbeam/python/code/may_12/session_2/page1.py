def function1():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # get even numbers from the collection
    even_numbers = []
    for number in numbers:
        # check if the number is even
        if number % 2 == 0:
            even_numbers.append(number)

    print(numbers)
    print(even_numbers)


# function1()


def is_even(number):
    return number % 2 == 0


def function2():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # is_even = lambda x: x % 2 == 0

    # get even numbers from the collection
    even_numbers = list(filter(is_even, numbers))
    print(numbers)
    print(even_numbers)


# function2()


def function3():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    is_odd = lambda x: x % 2 != 0

    # get all odd numbers
    # map or filter => filter
    odd_numbers = list(filter(is_odd, numbers))
    print(numbers)
    print(odd_numbers)


# function3()


def function4():
    marks = [20, 30, 25, 35, 30, 50, 60, 70, 35, 80, 90]

    # get the list of passed students
    is_passed = lambda m: m >= 35
    passed_students = list(filter(is_passed, marks))

    # get the list of failed students
    is_failed = lambda m: m < 35
    failed_students = list(filter(is_failed, marks))

    print(marks)
    print(passed_students)
    print(failed_students)


# function4()


def function5():
    cars = [
        {"model": "i20", "price": 7.5, "company": "hyundai", "color": "gray"},
        {"model": "i10", "price": 5.5, "company": "hyundai", "color": "red"},
        {"model": "nano", "price": 2.5, "company": "tata", "color": "yellow"},
        {"model": "fabia", "price": 5.5, "company": "skoda", "color": "white"},
        {"model": "X5", "price": 39, "company": "BMW", "color": "blue"},
        {"model": "GLS 450", "price": 95, "company": "Mercedez", "color": "white"},
        {"model": "Discovery", "price": 85, "company": "Range Rover", "color": "black"}
    ]

    # list of affordable (price <= 10) cars
    is_affordable = lambda car: car['price'] <= 10
    affordable_cars = list(filter(is_affordable, cars))

    # list of non-affordable (price > 10) cars
    is_non_affordable = lambda car: car['price'] > 10
    non_affordable_cars = list(filter(is_non_affordable, cars))

    print(cars)
    print(affordable_cars)
    print(non_affordable_cars)


function5()
