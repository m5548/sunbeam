num = 100

def function1():
    print("inside function1")

    # scope: local
    num = 10
    print(f"num = {num}, type = {type(num)}")


# function1()

# local members can not be accessed outside of the function
# print(f"num = {num}, type = {type(num)}")


# outer function
def function2():
    print("inside function2")

    # scope: local
    num = 10

    # inner function
    # local function
    # nested function
    def inner_function2():
        print("inside inner_function2")

    # calling inner function
    inner_function2()


# function2()

# inner_function2()


# outer function
def function3():
    print("inside function3")

    def inner_function1():
        print("inside inner_function1")

    def inner_function2():
        print("inside inner_function2")

    def inner_function3():
        print("inside inner_function3")

    inner_function1()
    inner_function2()
    inner_function3()


# function3()

# inner function(s) can NOT be accessed outside of outer function
# inner_function1()
# inner_function2()
# inner_function3()


def outer():
    print("inside outer function")

    num = 10
    print(f"num = {num}")

    def inner():
        print("inside inner function")

        # inner function can access all the members of outer function
        print(f"num = {num}")

        inner_num = 100
        print(f"inner_num = {inner_num}")

        def inner_inner():
            print(f"inside inner_inner")

        inner_inner()

    inner()

    # outer function can NOT access any member of inner function
    # print(f"inner_num = {inner_num}")

    # inner_inner()


outer()

