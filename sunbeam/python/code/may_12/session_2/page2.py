def function1():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # square of even numbers

    # step 1: find the even numbers
    # - filter
    is_even = lambda x: x % 2 == 0
    even_numbers = list(filter(is_even, numbers))

    # step 2: get square of each even number
    # - map
    square = lambda x: x ** 2
    square_even_numbers = list(map(square, even_numbers))

    print(square_even_numbers)


function1()


def function2():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # cube of odd numbers


# function2()


def function3():
    cars = [
        {"model": "i20", "price": 7.5, "company": "hyundai", "color": "gray"},
        {"model": "i10", "price": 5.5, "company": "hyundai", "color": "red"},
        {"model": "nano", "price": 2.5, "company": "tata", "color": "yellow"},
        {"model": "fabia", "price": 5.5, "company": "skoda", "color": "white"},
        {"model": "X5", "price": 39, "company": "BMW", "color": "blue"},
        {"model": "GLS 450", "price": 95, "company": "Mercedez", "color": "white"},
        {"model": "Discovery", "price": 85, "company": "Range Rover", "color": "black"}
    ]

    # find the following information of affordable cars
    # - model, price

    # find the following information of non-affordable cars
    # - model, company


# function3()


def function4():
    students = [
        {"roll": 1, "name": "student 1", "marks": 80},
        {"roll": 2, "name": "student 2", "marks": 20},
        {"roll": 3, "name": "student 3", "marks": 30},
        {"roll": 4, "name": "student 4", "marks": 70},
        {"roll": 5, "name": "student 5", "marks": 40},
        {"roll": 6, "name": "student 6", "marks": 90}
    ]

    # find the following information about passed students
    # - name, marks

    # find the following information about failed students
    # - roll, name


function4()

