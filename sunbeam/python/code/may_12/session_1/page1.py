# named function
def function1():
    print("inside function1")


# function1()

print(f"function1 = {function1}, type = {type(function1)}")

# function alias
my_function = function1
print(f"my_function = {my_function}, type = {type(my_function)}")

my_function()

