def function1():
    numbers = [1, 2, 3, 4, 5]

    # print square of every number
    # for number in numbers:
    #     print(f"square = {number ** 2}")

    square = lambda x: x ** 2
    for number in numbers:
        print(f"square of {number} = {square(number)}")


# function1()


def function2():
    numbers = [1, 2, 3, 4, 5]

    # get square of every number and collect it in a collection
    squares = []
    for number in numbers:
        squares.append(number ** 2)

    print(numbers)
    print(squares)


# function2()


def function3():
    temperatures_f = [97, 96, 93, 94, 96, 99, 100]
    to_celsius = lambda t: (t - 32) * (5/9)

    # convert every f temp to c
    temperatures_c = []
    for temperature in temperatures_f:
        temperatures_c.append(to_celsius(temperature))

    print(temperatures_f)
    print(temperatures_c)


# function3()

# summary
# - iterate over a collection
# - process every value
# - appending the processed value to a new collection


# map
# - function which will
#   - iterates over a collection
#   - calls a function to process every value
#   - appends the processed value to a new collection
#   - returns thew newly created collection with all processed values

def function4():
    numbers = [1, 2, 3, 4, 5]
    square = lambda x: x ** 2

    # get square of every number and collect it in a collection
    squares = list(map(square, numbers))

    print(numbers)
    print(squares)


# function4()


def function5():
    numbers = [1, 2, 3, 4, 5]
    cube = lambda x: x ** 3
    cubes = list(map(cube, numbers))

    print(numbers)
    print(cubes)


# function5()


def function6():
    temperatures_f = [97, 96, 93, 94, 96, 99, 100]
    to_celsius = lambda t: (t - 32) * (5 / 9)

    temperatures_c = list(map(to_celsius, temperatures_f))
    print(temperatures_f)
    print(temperatures_c)


# function6()


def function7():
    cars = [
        {"model": "i20", "price": 7.5, "company": "hyundai", "color": "gray"},
        {"model": "i10", "price": 5.5, "company": "hyundai", "color": "red"},
        {"model": "nano", "price": 2.5, "company": "tata", "color": "yellow"},
        {"model": "fabia", "price": 5.5, "company": "skoda", "color": "white"},
        {"model": "X5", "price": 39, "company": "BMW", "color": "blue"},
        {"model": "GLS 450", "price": 95, "company": "Mercedez", "color": "white"},
        {"model": "Discovery", "price": 85, "company": "Range Rover", "color": "black"}
    ]

    for car in cars:
        print(car)

    print()

    # print(cars)
    # get every car's model and price
    cars_1 = []
    for car in cars:
        cars_1.append({
            "model": car["model"],
            "price": car["price"],
            "tax": car["price"] * 0.10,
            "total_price": car["price"] + (car["price"] * 0.10)
        })

    print(cars_1)

    print()

    process_car = lambda car: {
        "model": car["model"],
        "price": car["price"],
        "tax": car["price"] * 0.10,
        "total_price": car["price"] + (car["price"] * 0.10)
    }
    cars_2 = list(map(process_car, cars))
    print(cars_2)


# function7()


def function8():
    students = [
        {"roll": 1, "name": "student 1", "marks": 80},
        {"roll": 2, "name": "student 2", "marks": 20},
        {"roll": 3, "name": "student 3", "marks": 30},
        {"roll": 4, "name": "student 4", "marks": 70},
        {"roll": 5, "name": "student 5", "marks": 40},
        {"roll": 6, "name": "student 6", "marks": 90}
    ]

    # create a new collection with following information of every student
    # - name, marks, result (pass or fail) - passing marks = 35
    students_info = []
    for student in students:
        students_info.append({
            "name": student["name"],
            "marks": student["marks"],
            "result": "pass" if student["marks"] >= 35 else "fail"
        })

    print(students)
    print(students_info)


# function8()


def declare_student_result(student):
    print(f"inside declare_student_result, student = {student}")
    return {
        "name": student["name"],
        "marks": student["marks"],
        "result": "pass" if student["marks"] >= 35 else "fail"
    }


def function9():
    students = [
        {"roll": 1, "name": "student 1", "marks": 80},
        {"roll": 2, "name": "student 2", "marks": 20},
        {"roll": 3, "name": "student 3", "marks": 30},
        {"roll": 4, "name": "student 4", "marks": 70},
        {"roll": 5, "name": "student 5", "marks": 40},
        {"roll": 6, "name": "student 6", "marks": 90}
    ]

    # declare_student_result = lambda student: {
    #         "name": student["name"],
    #         "marks": student["marks"],
    #         "result": "pass" if student["marks"] >= 35 else "fail"
    #     }

    students_info = list(map(declare_student_result, students))
    print(students)
    print(students_info)


function9()


def get_square(number):
    print(f"inside get_square, number = {number}")
    return number ** 2


def function10():
    numbers = [1, 2, 3, 4, 5]
    squares = list(map(get_square, numbers))
    print(squares)


# function10()
