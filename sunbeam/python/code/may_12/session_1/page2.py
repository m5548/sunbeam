# write a function which will take a param (number) and return square of the param
def square_1(number):
    return number ** 2


# print(f"square of 2 = {square_1(2)}")
# print(f"square of 5 = {square_1(5)}")

# requirements of lambda
# - it must accepts at least one parameter
# - the body must contain only one line of code
# - the body must return a value


# inline function (lambda)
# anonymous/unnamed function
square_2 = lambda number: number ** 2

# print(f"square_2 = {square_2}, type = {type(square_2)}")
# print(f"square of 2 = {2 ** 2}")
# print(f"square of 2 = {square_2(2)}")

# cube of a number
cube = lambda n: n ** 3
# print(f"cube of 2 = {cube(2)}")


# write a lambda to add two values
add = lambda n1, n2: n1 + n2
print(f"10 + 20 = {add(10, 20)}")

# write a lambda to multiply two values
multiply = lambda n1, n2: n1 * n2
print(f"10 * 20 = {multiply(10, 20)}")

# write a lambda to check if a number is even
is_even = lambda n: n % 2 == 0
print(f"is 10 even = {is_even(10)}")

# write a lambda to check if a number is odd
# is_odd = lambda n: n % 2 != 0
is_odd = lambda n: not is_even(n)
print(f"is 10 odd = {is_odd(10)}")

number = int(input("enter a number: "))
print(f"{number} is even = {is_even(number)}")

# print(f"not of True = {not True}")
# print(f"not of False = {not False}")
