# create a class named Phone
class Phone:
    """this is a dummy phone class"""
    pass


# create a new object named phone1
# a new memory will be allocated
phone1 = Phone()

# - add/set attributes: model, company, price
setattr(phone1, "model", "iPhone XS max")
setattr(phone1, "company", "apple")
setattr(phone1, "price", 144000)

# - get attributes: model, company, price
print(f"model = {getattr(phone1, 'model')}")
print(f"company = {getattr(phone1, 'company')}")
print(f"price = {getattr(phone1, 'price')}")

print(f"phone1 = {phone1.__dict__}")

# create a new object named phone2
# a new memory will be allocated
phone2 = Phone()

# - add/set attributes: model, company, price
setattr(phone2, "model", "iPhone 7 plus")
setattr(phone2, "company", "apple")
setattr(phone2, "price", 99000)

# - get attributes: model, company, price
print(f"model = {getattr(phone2, 'model')}")
print(f"company = {getattr(phone2, 'company')}")
print(f"price = {getattr(phone2, 'price')}")

print(f"phone2 = {phone2.__dict__}")