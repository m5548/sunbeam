# example of bad code

class Person:
    pass


person1 = Person()

# add an attribute
setattr(person1, "name", "person1")
setattr(person1, "address", "pune")
setattr(person1, "age", 40)

print(f"name = {getattr(person1, 'name')}")
print(f"address = {getattr(person1, 'address')}")
print(f"age = {getattr(person1, 'age')}")

if getattr(person1, 'age') >= 18:
    print(f"{getattr(person1, 'name')} is eligible for voting")
else:
    print(f"{getattr(person1, 'name')} is NOT eligible for voting")

print()

person2 = Person()

# add an attribute
setattr(person2, "name", "person2")
setattr(person2, "address", "mumbai")
setattr(person2, "age", 10)

print(f"name = {getattr(person2, 'name')}")
print(f"address = {getattr(person2, 'address')}")
print(f"age = {getattr(person2, 'age')}")

if getattr(person2, 'age') >= 18:
    print(f"{getattr(person2, 'name')} is eligible for voting")
else:
    print(f"{getattr(person2, 'name')} is NOT eligible for voting")
