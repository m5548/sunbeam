class Person:
    pass


def set_details(person, address, age, name):
    # person  = person2
    # name    = "person2"
    # address = "mumbai"
    # age     = 10
    setattr(person, "name", name)
    setattr(person, "address", address)
    setattr(person, "age", age)


def print_details(person):
    print(f"name = {getattr(person, 'name')}")
    print(f"address = {getattr(person, 'address')}")
    print(f"age = {getattr(person, 'age')}")


def can_vote(person):
    if getattr(person, 'age') >= 18:
        print(f"{getattr(person, 'name')} is eligible for voting")
    else:
        print(f"{getattr(person, 'name')} is NOT eligible for voting")


person1 = Person()
set_details(person1, "pune", 40, "person1")
print_details(person1)
can_vote(person1)

print()

person2 = Person()
set_details(name="person2", address="mumbai", age=10, person=person2)
print_details(person2)
can_vote(person2)

# print_details(10)

