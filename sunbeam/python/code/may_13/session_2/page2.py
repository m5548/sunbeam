def print_details(person):
    print(f"name = {person['name']}")
    print(f"address = {person['address']}")
    print(f"age = {person['age']}")
    print()


def can_vote(person):
    if person['age'] >= 18:
        print(f"{person['name']} is eligible for voting")
    else:
        print(f"{person['name']} is NOT eligible for voting")


person1 = {"name": "person1", "age": 40, "address": "pune"}
can_vote(person1)
print_details(person1)

person2 = {"name": "person2", "age": 10, "address": "mumbai"}
can_vote(person2)
print_details(person2)

person3 = {"name": "person3", "age": 20, "address": "satara"}
can_vote(person3)
print_details(person3)

# print_details("test")
# print_details({"model": "iPhone XS Max", "storage": "512GB"})
