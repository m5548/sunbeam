# empty class
class Person:
    """this is a test class"""
    pass


# class variable
# - object of class Person
# - instance of class Person
# syntax
# - <reference name> = <Class Name>()
# instanciation
# - process of creating an object of a class
person1 = Person()
print(f"person1 = {person1}, type = {type(person1)}")
print(f"contents of person1 = {person1.__dict__}")

# add an attribute
setattr(person1, "name", "person1")
setattr(person1, "address", "pune")
setattr(person1, "age", 40)
setattr(person1, "phone", None)

print(f"contents of person1 = {person1.__dict__}")

# get the attribute(s)
print(f"name = {getattr(person1, 'name')}")
print(f"address = {getattr(person1, 'address')}")
print(f"age = {getattr(person1, 'age')}")
print(f"phone = {getattr(person1, 'phone')}")


# num = 100
# print(f"num = {num}, type = {type(num)}")
