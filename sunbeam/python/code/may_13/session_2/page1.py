# example of bad code

person1 = {"name": "person1", "age": 40, "address": "pune"}
print(f"name = {person1['name']}")
print(f"address = {person1['address']}")
print(f"age = {person1['age']}")

if person1['age'] >= 18:
    print(f"{person1['name']} is eligible for voting")
else:
    print(f"{person1['name']} is NOT eligible for voting")

print()

person2 = {"name": "person2", "age": 10, "address": "mumbai"}
print(f"name = {person2['name']}")
print(f"address = {person2['address']}")
print(f"age = {person2['age']}")

if person2['age'] >= 18:
    print(f"{person2['name']} is eligible for voting")
else:
    print(f"{person2['name']} is NOT eligible for voting")

print()

person3 = {"name": "person3", "age": 20, "address": "satara"}
print(f"name = {person3['name']}")
print(f"address = {person3['address']}")
print(f"age = {person3['age']}")

if person3['age'] >= 18:
    print(f"{person3['name']} is eligible for voting")
else:
    print(f"{person3['name']} is NOT eligible for voting")