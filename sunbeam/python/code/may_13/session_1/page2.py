# comprehension
# - list comprehension as map
# - tuple comprehension as map


def function1():
    numbers = [1, 2, 3, 4, 5]

    # get square of every number and store it in a list

    # squares_1 = []
    # for n in numbers:
    #     squares_1.append(n ** 2)

    # squares_2 = list(map(lambda n: n ** 2, numbers))

    # list comprehension
    squares_3 = [n ** 2 for n in numbers]

    print(numbers)
    # print(squares_1)
    # print(squares_2)
    print(squares_3)


# function1()


def function2():
    numbers = [1, 2, 3, 4, 5]

    cubes_map = list(map(lambda n: n ** 3, numbers))
    # cubes_comprehension = list(n ** 3 for n in numbers)
    cubes_comprehension = [n ** 3 for n in numbers]

    print(numbers)
    print(cubes_map)
    print(cubes_comprehension)


# function2()


def function3():
    numbers = [1, 2, 3, 4, 5]

    cubes_map = tuple(map(lambda n: n ** 3, numbers))
    cubes_comprehension = tuple(n ** 3 for n in numbers)

    print(numbers)
    print(cubes_map)
    print(cubes_comprehension)


# function3()


def function4():
    cars = [
        {"model": "i20", "price": 7.5, "company": "hyundai", "color": "gray"},
        {"model": "i10", "price": 5.5, "company": "hyundai", "color": "red"},
        {"model": "nano", "price": 2.5, "company": "tata", "color": "yellow"},
        {"model": "fabia", "price": 5.5, "company": "skoda", "color": "white"},
        {"model": "X5", "price": 39, "company": "BMW", "color": "blue"},
        {"model": "GLS 450", "price": 95, "company": "Mercedez", "color": "white"},
        {"model": "Discovery", "price": 85, "company": "Range Rover", "color": "black"}
    ]

    # get every car's model and price
    cars_1 = []
    for car in cars:
        cars_1.append({
            "model": car['model'],
            "price": car['price']
        })

    cars_2 = list(map(lambda car: {"model": car['model'], "price": car['price']}, cars))
    cars_3 = [{"model": car['model'], "price": car['price']} for car in cars]

    print(cars)
    print(cars_1)
    print(cars_2)
    print(cars_3)


# function4()


def function5():
    students = [
        {"roll": 1, "name": "student 1", "marks": 80},
        {"roll": 2, "name": "student 2", "marks": 20},
        {"roll": 3, "name": "student 3", "marks": 30},
        {"roll": 4, "name": "student 4", "marks": 70},
        {"roll": 5, "name": "student 5", "marks": 40},
        {"roll": 6, "name": "student 6", "marks": 90}
    ]

    # create a new collection with following information of every student
    # - name, marks, result (pass or fail) - passing marks = 35

    # using for loop
    students_loop = []
    for student in students:
        students_loop.append({
            "name": student['name'],
            "marks": student['marks'],
            "result": 'pass' if student['marks'] >= 35 else "fail"
        })

    # using map
    students_map = list(map(lambda student: {
            "name": student['name'],
            "marks": student['marks'],
            "result": 'pass' if student['marks'] >= 35 else "fail"
        }, students))

    # using list comprehension
    students_comprehension = [{
            "name": student['name'],
            "marks": student['marks'],
            "result": 'pass' if student['marks'] >= 35 else "fail"
        } for student in students]

    print(students)
    print(students_loop)
    print(students_map)
    print(students_comprehension)


function5()
