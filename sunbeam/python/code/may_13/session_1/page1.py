def function1():
    numbers = [1, 2, 3, 4, 5]

    # add 100 to every number in the numbers collection
    # lambda_100 = lambda n: n + 100
    # numbers_100 = list(map(lambda_100, numbers))

    # use lambda directly as the first parameter
    numbers_100 = list(map(lambda n: n + 100, numbers))

    print(numbers)
    print(numbers_100)


# function1()


def function2():
    numbers = [3, 4, 6, 9, 38, 12, 45, 2, 6, 9, 90, 34, 57]

    # get the numbers divisible by 3
    # lambda_3 = lambda n: n % 3 == 0
    # numbers_3 = list(filter(lambda_3, numbers))

    numbers_3 = list(filter(lambda n: n % 3 == 0, numbers))
    print(numbers)
    print(numbers_3)


# function2()


def function3():
    numbers = [3, 4, 6, 9, 38, 12, 45, 2, 6, 9, 90, 34, 57]

    # add 40 to every even number from numbers collection
    # even_numbers = filter(lambda n: n % 2 == 0, numbers)
    # numbers_40 = list(map(lambda n: n + 40, even_numbers))
    numbers_40 = list(map(lambda n: n + 40, filter(lambda n: n % 2 == 0, numbers)))

    print(numbers)
    print(numbers_40)


# function3()


def function4():
    numbers = [3, 4, 6, 9, 38, 12, 45, 2, 6, 9, 90, 34, 57]

    even_numbers = list(map(lambda n: "even" if n % 2 == 0 else "odd", numbers))
    print(even_numbers)


# function4()
