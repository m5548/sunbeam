def add1(p1, p2):
    addition = p1 + p2
    print(f"addition = {addition}")


# add1(10, 20)
# add1(10, 20, 30)


def add2(p1, p2, p3):
    addition = p1 + p2 + p3
    print(f"addition = {addition}")


# add2(10, 20, 30)
# add2(10, 20, 30, 40)


# variable length arguments function
# function can accept any number of parameters
def add(*args):
    print("inside add")
    print(f"args = {args}, type = {type(args)}")

    addition = 0
    for number in args:
        addition += number
    print(f"addition = {addition}")
    print()


# add(10, 20)
# add(10, 20, 30)
# add(10, 20, 30, 40)


def print_numbers(*numbers):
    print(f"numbers = {numbers}, type = {type(numbers)}")


# print_numbers(10, 20)
# print_numbers(10, 20, 30)
# print_numbers(10, 20, 30, 40)


# variable length arguments function
def print_parameters(*args, **kwargs):
    print(f"args = {args}, type = {type(args)}")
    print(f"kwargs = {kwargs}, type = {type(kwargs)}")


# args = (10, 20), kwargs = {}
# print_parameters(10, 20)

# args = (), kwargs = {'p1': 10, 'p2': 20}
# print_parameters(p1=10, p2=20)

# args = (10, 20), kwargs = {'p1': 30, 'p3': 40}
# print_parameters(10, 20, 40, 50, p1=30, p3=40)


def add3(*args, **kwargs):
    addition = 0

    # read all the numbers from args
    for number in args:
        addition += number

    # read all the key-value pairs from kwargs
    keys = kwargs.keys()
    for key in keys:
        addition += kwargs[key]

    print(f"addition = {addition}")


add3(10, 20, 30, p1=40, p2=50)