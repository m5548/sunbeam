# comprehension
# - list comprehension as filter
# - tuple comprehension as filter

def function1():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # find even numbers
    even_numbers_loop = []
    for number in numbers:
        if number % 2 == 0:
            even_numbers_loop.append(number)

    # using a filter function
    even_numbers_filter = list(filter(lambda n: n % 2 == 0, numbers))

    # using list comprehension
    even_numbers_comprehension = [number for number in numbers if number % 2 == 0]

    print(numbers)
    print(even_numbers_comprehension)


# function1()


def function2():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    odd_numbers = [number for number in numbers if number % 2 != 0]

    print(numbers)
    print(odd_numbers)


# function2()


def function3():
    cars = [
        {"model": "i20", "price": 7.5, "company": "hyundai", "color": "gray"},
        {"model": "i10", "price": 5.5, "company": "hyundai", "color": "red"},
        {"model": "nano", "price": 2.5, "company": "tata", "color": "yellow"},
        {"model": "fabia", "price": 5.5, "company": "skoda", "color": "white"},
        {"model": "X5", "price": 39, "company": "BMW", "color": "blue"},
        {"model": "GLS 450", "price": 95, "company": "Mercedez", "color": "white"},
        {"model": "Discovery", "price": 85, "company": "Range Rover", "color": "black"}
    ]

    # list of affordable (price < 10) cars
    affordable_cars = [car for car in cars if car['price'] < 10]

    # list of non-affordable (price > 10) cars
    non_affordable_cars = [car for car in cars if car['price'] > 10]

    print(cars)
    print(affordable_cars)
    print(non_affordable_cars)


function3()
