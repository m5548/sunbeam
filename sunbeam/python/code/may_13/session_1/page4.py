# comprehension
# - list comprehension as map + filter
# - tuple comprehension as map + filter


def function1():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    # square of even numbers
    square_even_numbers = [n ** 2 for n in numbers if n % 2 == 0]

    # cube of odd numbers
    cube_odd_numbers = [n ** 3 for n in numbers if n % 2 != 0]

    print(numbers)
    print(square_even_numbers)
    print(cube_odd_numbers)


# function1()


def function2():
    cars = [
        {"model": "i20", "price": 7.5, "company": "hyundai", "color": "gray"},
        {"model": "i10", "price": 5.5, "company": "hyundai", "color": "red"},
        {"model": "nano", "price": 2.5, "company": "tata", "color": "yellow"},
        {"model": "fabia", "price": 5.5, "company": "skoda", "color": "white"},
        {"model": "X5", "price": 39, "company": "BMW", "color": "blue"},
        {"model": "GLS 450", "price": 95, "company": "Mercedez", "color": "white"},
        {"model": "Discovery", "price": 85, "company": "Range Rover", "color": "black"}
    ]

    # get model and company of affordable cars
    affordable_cars = [{"model": car['model'], "company": car['company']} for car in cars if car['price'] < 10]

    print(cars)
    print(affordable_cars)


function2()