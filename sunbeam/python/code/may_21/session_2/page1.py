import numpy as np
import pandas as pd


def function1():
    # list
    l1 = [10, 20, 30, 40, 50]
    print(l1)
    print(f"l1 type = {type(l1)}")

    print()

    # numpy array
    a1 = np.array([10, 20, 30, 40, 50])
    print(a1)
    print(f"a1 type = {type(a1)}")

    print()

    # pandas series
    s1 = pd.Series([10, 20, 30, 40, 50])
    print(s1)
    print(f"s1 type = {type(s1)}")


# function1()


def function2():
    s1 = pd.Series([10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150])
    print(s1)
    print(f"s1 type = {type(s1)}")
    print(f"no of dimensions = {s1.ndim}")
    print(f"shape = {s1.shape}")
    print(f"no of items in series = {s1.size}")
    print(f"data type = {s1.dtype}")
    print(f"index positions = {s1.index}")
    print(f"values = {s1.values}")

    # get the first 3 values
    print(f"values in the beginning = {s1.head(3)}")

    # get last 3 values
    print(f"values at the end of the series = {s1.tail(3)}")


# function2()


def function3():
    s1 = pd.Series([10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150])
    print(s1)

    # positive indexing
    print(f"s1[0] = {s1[0]}")
    print(f"s1[5] = {s1[5]}")
    print(f"s1[10] = {s1[10]}")

    # negative indexing
    # -1 position does not exist in the series
    # print(f"s1[-1] = {s1[-1]}")


# function3()


def function4():
    models = ['i20', 'i10', 'nano', 'range rover', 'x5']
    averages = [17, 18, 20, 10, 11]

    # the average value will be stored on the car model respectively
    cars_series_1 = pd.Series(averages, index=models)
    print(cars_series_1)
    print(f"average of i20 = {cars_series_1['i20']}")
    print(f"average of x5 = {cars_series_1['x5']}")
    print(f"index = {cars_series_1.index}")
    print(f"values = {cars_series_1.values}")

    print()

    cars_series_2 = pd.Series(models, index=averages)
    print(cars_series_2)
    print(f"average 18 is of {cars_series_2[18]} car")
    print(f"average 11 is of {cars_series_2[11]} car")
    print(f"index = {cars_series_2.index}")
    print(f"values = {cars_series_2.values}")


function4()