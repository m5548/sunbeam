import pandas as pd


def function1():
    # collection of rows (horizontal) and columns (vertical)
    # collection of Series objects
    df = pd.DataFrame([
        {"name": "person1", "address": "pune", "age": 20, "email": "person1@test.com"},
        {"name": "person2", "address": "mumbai", "age": 22, "email": "person2@test.com"},
        {"name": "person3", "address": "pune", "age": 25, "email": "person3@test.com"},
        {"name": "person4", "address": "pune", "age": 27, "email": "person4@test.com"},
        {"name": "person5", "address": "satara", "age": 29, "email": "person5@test.com"},
        {"name": "person6", "address": "pune", "age": 30, "email": "person6@test.com"},
    ])
    print(df)


# function1()


def function2():
    df = pd.read_csv('Data.csv')
    print(df)
    print()

    # get first 4 rows
    print(f"first 5 rows = {df.head()}")
    print()

    # get last 5 rows
    print(f"last 5 rows = {df.tail()}")
    print()
    print(f"columns = {df.columns}")
    print(f"no of dimensions = {df.ndim}")
    print(f"shape of df = {df.shape}")
    print(f"data types = {df.dtypes}")

    print()

    # get the information about the df
    df.info()

    print()

    # get statistical information about the df
    print(df.describe())


# function2()
