import pandas as pd


def function1():
    df = pd.read_csv('employees.csv')

    # NaN: Not a Number => missing value
    # print(df)
    # print(df.tail())
    # df.info()

    # check if the df has any NaN records
    # print(df.isna())

    # print Team column
    # print(df['Team'])

    # check if Team columns has any NaN records
    print(df['Team'].isna())


# function1()


def function2():
    df = pd.read_csv('employees.csv')
    print(f"shape = {df.shape}")

    print(df)

    print()
    # remove all the NaN records
    # axis = 0 : row
    # axis = 1 : column
    df = df.dropna(axis=0)

    print(f"shape = {df.shape}")
    print(df)


# function2()


def function3():
    df = pd.read_csv('employees.csv')
    print(f"shape = {df.shape}")
    print(df)

    print()

    # fill/replace NaN with some value (0)
    df = df.fillna(0)
    print(f"shape = {df.shape}")
    print(df)


# function3()


def function4():
    df = pd.read_csv('employees.csv')
    print(df.columns)

    # axis = 0: row
    # axis = 1: column
    # df = df.drop('Last Login Time', axis=1)
    df.drop('Last Login Time', axis=1, inplace=True)
    print(df.columns)

    # delete multiple columns
    df.drop(['Start Date', 'Salary', 'Bonus %'], axis=1, inplace=True)
    print(df.columns)

    # save the changes to new_employees.csv file
    df.to_csv("new_employees.csv")


function4()