import pandas as pd


def function1():
    # create series using list
    s1 = pd.Series([10, 20, 30, 40, 50])
    print(s1)

    print()

    # create series using tuple
    s2 = pd.Series((10, 20, 30, 40, 50))
    print(s2)

    print()

    # can not create series using set
    # s3 = pd.Series({10, 20, 30, 40, 50})
    # print(s3)

    # can not create series using frozen set
    # s4 = pd.Series(frozenset([10, 20, 30, 40, 50]))
    # print(s4)

    # create series using dictionary
    s5 = pd.Series({"name": "person1", "address": "pune", "age": 30})
    print(s5)


# function1()


def function2():
    s1 = pd.Series([10, 20, 30, 40, 50])
    print(s1)

    # broadcasting operations
    print(f"s1 > 20 = {s1 > 20}")
    print(f"s1 < 20 = {s1 < 20}")

    # filtering
    print(f"s1[s1 > 20] = {s1[s1 > 20]}")


function2()