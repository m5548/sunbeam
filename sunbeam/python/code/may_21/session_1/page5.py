import numpy as np


def function1():
    a1 = np.array([10, 20, 30, 40, 50])

    # [20, 30, 40]
    print(f"a1[1:4] = {a1[1:4]}")

    # [10, 30, 40]
    print(f"a1[[0, 2, 3]] = {a1[[0, 2, 3]]}")

    # [20, 40, 50]
    print(f"a1[[1, 3, 4]] = {a1[[1, 3, 4]]}")


# function1()


def function2():
    a1 = np.array([10, 20, 30, 40, 50])

    # [20, 30, 40]
    print(f"a1[[False, True, True, True, False]] = {a1[[False, True, True, True, False]]}")

    # [10, 30, 40]
    print(f"a1[[True, False, True, True, False]] = {a1[[True, False, True, True, False]]}")

    # [20, 40, 50]
    print(f"a1[[False, True, False, True, True]] = {a1[[False, True, False, True, True]]}")


# function2()


def function3():
    salaries = [10, 20, 15, 12, 14, 20, 10, 40, 50, 100, 30, 48, 10, 20]

    salaries_above_20 = list(filter(lambda s: s > 20, salaries))
    print(salaries_above_20)

    print([s for s in salaries if s > 20])


# function3()


def function4():
    salaries = np.array([10, 20, 15, 12, 14, 20, 10, 40, 50, 100, 30, 48, 10, 20])

    # filtering
    print(f"salaries > 20: {salaries > 20}")
    print(f"salaries > 20: {salaries[[False, False, False, False, False, False, False, True, True, True, True, True, False, False]]}")
    print(f"salaries > 20: {salaries[salaries > 20]}")


function4()
