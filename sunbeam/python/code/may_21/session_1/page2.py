import numpy as np


def print_array_details(array):
    print(f"array = {array}, type = {type(array)}")
    print(f"no of dimensions = {array.ndim}")
    print(f"data type of every item: {array.dtype}")
    print(f"no of items in the array: {array.size}")
    print(f"size of every item: {array.itemsize}")
    print(f"total memory needed for the array = {array.itemsize * array.size}")
    print(f"shape of the array = {array.shape}")
    print('-' * 50)


def function1():
    a1 = np.array([
        [10, 20],
        [30, 40],
        [50, 60]
    ])
    print_array_details(a1)

    a2 = np.array([
        [10, 20, 30],
        [40, 50, 60]
    ])
    print_array_details(a2)


# function1()


def function2():
    a1 = np.array([
        [10, 20],
        [30, 40],
        [50, 60]
    ])
    print_array_details(a1)

    a2 = a1.reshape((2, 3))
    print_array_details(a2)

    a3 = a1.reshape((1, 6))
    print_array_details(a3)

    a4 = a1.reshape((6, 1))
    print_array_details(a4)


function2()
