import numpy as np


def function1():
    # list
    # - collection of similar or dissimilar values
    # - mutable: can be modified after its creation
    # - can not guarantee whether the elements will be allocated contiguously
    # - slower than np array
    list1 = [10, 20, 30, 40, 50]
    print(f"list1 = {list1}, type = {type(list1)}")

    # array
    # - collection of similar values
    # - immutable: once created, can NOT modified
    # - guaranteed allocation will be contiguously
    # - faster than list (python collection)
    a1 = np.array([10, 20, 30, 40, 50])
    print(f"a1 = {a1}, type = {type(a1)}")


# function1()


def print_array_details(array):
    print(f"array = {array}, type = {type(array)}")
    print(f"no of dimensions = {array.ndim}")
    print(f"data type of every item: {array.dtype}")
    print(f"no of items in the array: {array.size}")
    print(f"size of every item: {array.itemsize}")
    print(f"total memory needed for the array = {array.itemsize * array.size}")
    print(f"shape of the array = {array.shape}")
    print('-' * 50)


def function2():
    # int64
    a1 = np.array([10, 20, 30, 40, 50])
    print_array_details(a1)

    # int64
    a2 = np.array([10, 20, 30, 40, 50], dtype=np.int64)
    print_array_details(a2)

    # int32
    a3 = np.array([10, 20, 30, 40, 50], dtype=np.int32)
    print_array_details(a3)

    # int16
    a4 = np.array([10, 20, 30, 40, 50], dtype=np.int16)
    print_array_details(a4)

    # int8
    a5 = np.array([10, 20, 30, 40, 50], dtype=np.int8)
    print_array_details(a5)


# function2()


def function3():
    a1 = np.array([10.50, 20.50, 30.50, 40.50, 50.50])
    print_array_details(a1)

    a2 = np.array([10.50, 20.50, 30.50, 40.50, 50.50], dtype=np.float64)
    print_array_details(a2)

    a3 = np.array([10.50, 20.50, 30.50, 40.50, 50.50], dtype=np.float32)
    print_array_details(a3)

    a4 = np.array([10.50, 20.50, 30.50, 40.50, 50.50], dtype=np.float16)
    print_array_details(a4)


# function3()


def function4():
    a1 = np.array(["str1", "str2", "str3", "str4", "str5"])
    print_array_details(a1)

    a2 = np.array(["str1", "str2", "str3", "str4", "str5"], dtype='<U2')
    print_array_details(a2)


# function4()


def function5():
    a1 = np.array([10, 20.40, 50, 10.40, 30, 60.0])
    print_array_details(a1)

    a2 = np.array([10, 20.40, 50, 10.40, 30, 60.0], dtype=np.int8)
    print_array_details(a2)

    a3 = np.array([10, 20.40, 50, 10.40, 30, 60.0, 'test1'], dtype='<U3')
    print_array_details(a3)


# function5()