import numpy as np


def function1():
    a1 = np.arange(1, 11)
    print(f"a1 = {a1}")

    # positive indexing
    print(f"a1[0] = {a1[0]}")
    print(f"a1[2] = {a1[2]}")
    print(f"a1[4] = {a1[4]}")
    print(f"a1[9] = {a1[9]}")

    print()

    # negative indexing
    print(f"a1[-10] = {a1[-10]}")
    print(f"a1[-8] = {a1[-8]}")
    print(f"a1[-6] = {a1[-6]}")
    print(f"a1[-1] = {a1[-1]}")


# function1()


def function2():
    a1 = np.arange(1, 11)
    print(f"a1 = {a1}")

    # slicing
    print(f"a1[2:5] = {a1[2:5]}")
    print(f"a1[0:5] = {a1[0:5]}")
    print(f"a1[:5] = {a1[:5]}")
    print(f"a1[5:9] = {a1[5:9]}")
    print(f"a1[5:] = {a1[5:]}")
    print(f"a1[:] = {a1[:]}")


# function2()


def function3():
    l1 = [10, 20, 30, 40, 50]
    print(l1)

    # [110, 120, 130, 140, 150]
    l2 = list(map(lambda x: x + 100, l1))
    print(l2)

    l3 = [x + 100 for x in l1]
    print(l3)


# function3()


def function4():
    a1 = np.array([10, 20, 30, 40, 50])
    print(f"a1 = {a1}")

    # broadcast operation
    # - the operation will be performed on every member of array

    a2 = a1 + 100
    print(f"a2 = {a2}")

    a3 = a1 - 5
    print(f"a3 = {a3}")

    a4 = a1 * 10
    print(f"a4 = {a4}")

    a5 = a1 / 5
    print(f"a5 = {a5}")


# function4()


def function5():
    a1 = np.array([10, 20, 30, 40, 50])
    print(f"a1 = {a1}")

    # broadcast operations
    print(f"a1 < 30 = {a1 < 30}")
    print(f"a1 > 30 = {a1 > 30}")
    print(f"a1 <= 30 = {a1 <= 30}")
    print(f"a1 >= 30 = {a1 >= 30}")
    print(f"a1 == 30 = {a1 == 30}")
    print(f"a1 != 30 = {a1 != 30}")


function5()
