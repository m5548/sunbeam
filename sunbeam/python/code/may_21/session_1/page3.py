import numpy as np


def print_array_details(array):
    print(f"array = {array}, type = {type(array)}")
    print(f"no of dimensions = {array.ndim}")
    print(f"data type of every item: {array.dtype}")
    print(f"no of items in the array: {array.size}")
    print(f"size of every item: {array.itemsize}")
    print(f"total memory needed for the array = {array.itemsize * array.size}")
    print(f"shape of the array = {array.shape}")
    print('-' * 50)


def function1():
    l1 = list(range(10))
    print(f"l1 = {l1}, type = {type(l1)}")


# function1()


def function2():
    a1 = np.arange(10)
    print_array_details(a1)


# function2()


def function3():
    a1 = np.ones(10)
    print_array_details(a1)

    a2 = np.ones(10, dtype=np.int8)
    print_array_details(a2)

    a3 = np.ones((2, 3))
    print_array_details(a3)

    a4 = np.ones((4, 4), dtype=np.int8)
    print_array_details(a4)


# function3()


def function4():
    a1 = np.zeros(10)
    print_array_details(a1)

    a2 = np.zeros(10, dtype=np.int8)
    print_array_details(a2)

    a3 = np.zeros((2, 3))
    print_array_details(a3)

    a4 = np.zeros((4, 4), dtype=np.int8)
    print_array_details(a4)


# function4()


def function5():
    a1 = np.random.randint(1, 100, 10)
    print_array_details(a1)


function5()