class Person:
    # initializer
    def __init__(self, name, address, age):
        self.__name = name
        self.__address = address
        self.__age = age
        print("inside initializer")

    # de-initializer
    def __del__(self):
        print(f"inside de-initializer for = {self.__name}")

    # setter / mutator
    def set_name(self, name):
        self.__name = name

    def set_address(self, address):
        self.__address = address

    def set_age(self, age):
        self.__age = age

    # getter / inspector
    def get_name(self):
        return self.__name

    def get_address(self):
        return self.__address

    def get_age(self):
        return self.__age

    # facilitator
    def can_vote(self):
        if self.__age >= 18:
            print(f"{self.__name} is eligible for voting")
        else:
            print(f"{self.__name} is NOT eligible for voting")


# creating a new object
p1 = Person("person1", "pune", 40)
print(f"name = {p1.get_name()}")
print(f"address = {p1.get_address()}")
print(f"age = {p1.get_age()}")

# deleting p1
# memory allocate for p1 will be freed
del p1

print()

# creating a new object
p2 = Person("person2", "mumbai", 10)
print(f"name = {p2.get_name()}")
print(f"address = {p2.get_address()}")
print(f"age = {p2.get_age()}")

# deleting p2
# memory allocate for p2 will be freed
del p2
