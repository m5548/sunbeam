# association: aggregation

class Address:
    def __init__(self, line1, line2, city, state, country):
        self.__line1 = line1
        self.__line2 = line2
        self.__city = city
        self.__state = state
        self.__country = country

    def print_address(self):
        print(f"line1 = {self.__line1}")
        print(f"line2 = {self.__line2}")
        print(f"city = {self.__city}")
        print(f"state = {self.__state}")
        print(f"country = {self.__country}")


class Person:
    def __init__(self, name, age, address_line1, address_line2, city, state, country):
        self.__name = name
        self.__age = age

        # object of class Address
        self.__address = Address(address_line1, address_line2, city, state, country)

    def print_details(self):
        print(f"name = {self.__name}")
        print(f"age = {self.__age}")

        # calling print_address from Address class
        self.__address.print_address()


class School:
    def __init__(self, name, address_line1, address_line2, city, state, country):
        self.__name = name

        # object of class Address
        self.__address = Address(address_line1, address_line2, city, state, country)

    def print_details(self):
        print(f"name = {self.__name}")

        # calling print_address from Address class
        self.__address.print_address()


p1 = Person("person1", 40, "line1", "line2", "pune", "MH", "india")
p1.print_details()

print()

s1 = School("school 1", "line1", "line2", "mumbai", "MH", "india")
s1.print_details()
