# association: composition


class Engine:
    def __init__(self, name, fuel_type):
        self.__name = name
        self.__fuel_type = fuel_type

    def print_details(self):
        print(f"engine name = {self.__name}")
        print(f"fuel type = {self.__fuel_type}")


class Car:
    def __init__(self, model, company, engine_name, fuel_type):
        self.__model = model
        self.__company = company

        # object of class Engine
        self.__engine = Engine(engine_name, fuel_type)

    def print_details(self):
        print(f"model = {self.__model}")
        print(f"company = {self.__company}")

        # calling print_details of Engine class
        self.__engine.print_details()


c1 = Car('i20', 'hyundai', "v2", "petrol")
c1.print_details()