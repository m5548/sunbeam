# class: Car
# attribute: model, company, price
# methods: initializer, de-initializer, setters, getters, facilitator (can_afford)

class Car:
    # initializer
    def __init__(self, model, company, price):
        self.__model = model
        self.__company = company
        self.__price = price

    # de-initializer
    def __del__(self):
        print(f"inside de-initializer for {self.__model}")

    # setters
    def set_model(self, model):
        self.__model = model

    def set_company(self, company):
        self.__company = company

    def set_price(self, price):
        self.__price = price

    # getters
    def get_model(self):
        return self.__model

    def get_company(self):
        return self.__company

    def get_price(self):
        return self.__price

    # facilitator
    def can_afford(self):
        if self.__price >= 10:
            print(f"Not affordable")
        else:
            print(f"Affordable")


c1 = Car('nano', 'tata', 2.5)
print(f"model = {c1.get_model()}")
print(f"company = {c1.get_company()}")
print(f"price = {c1.get_price()}")
c1.can_afford()
