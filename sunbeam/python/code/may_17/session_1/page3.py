# bad code

class Person:
    def __init__(self, name, age, address, city, state, country):
        self.__name = name
        self.__age = age
        self.__address = address
        self.__city = city
        self.__state = state
        self.__country = country

    def print_details(self):
        print(f"name = {self.__name}")
        print(f"age = {self.__age}")
        print(f"address = {self.__address}")
        print(f"city = {self.__city}")
        print(f"state = {self.__state}")
        print(f"country = {self.__country}")
        print()


p1 = Person("person1", 40, "pune", "pune", "MH", "India")
p1.print_details()

print()


class School:
    def __init__(self, name, address, city, state, country):
        self.__name = name
        self.__address = address
        self.__city = city
        self.__state = state
        self.__country = country

    def print_details(self):
        print(f"name = {self.__name}")
        print(f"address = {self.__address}")
        print(f"city = {self.__city}")
        print(f"state = {self.__state}")
        print(f"country = {self.__country}")
        print()


s1 = School('school 1', "mumbai", "mumbai", 'MH', 'india')
s1.print_details()