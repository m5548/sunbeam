# Person
# attributes: name, address, age
# methods: print_details

# Employee
# attributes: employee_id, company
# methods: print_details

class Person:
    def __init__(self, name, address, age):
        # protected
        self._name = name
        self._address = address
        self._age = age

    def print_details(self):
        print(f"name = {self._name}")
        print(f"address = {self._address}")
        print(f"age = {self._age}")


class Employee(Person):
    def __init__(self, name, address, age, id, company):
        Person.__init__(self, name, address, age)

        # private
        self.__employee_id = id
        self.__company = company

    def print_details(self):
        print(f"name = {self._name}")
        print(f"address = {self._address}")
        print(f"age = {self._age}")
        print(f"id = {self.__employee_id}")
        print(f"company = {self.__company}")


e1 = Employee("emp1 ", "pune", 40, 1, "company 1")
e1.print_details()
