class Person:
    def __init__(self):
        self.name = ""
        self.address = ""

    def print_details(self):
        print(f"name = {self.name}")
        print(f"address = {self.address}")


class Student(Person):
    def __init__(self):
        # have to initialize the Parent class (Person class) object
        Person.__init__(self)
        self.roll_no = 0


p1 = Person()
print(f"p1 = {p1.__dict__}")

s1 = Student()
print(f"s1 = {s1.__dict__}")
