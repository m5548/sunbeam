class Person:
    def __init__(self, name, address):
        # public
        # - can be accessed anywhere: inside derived class and outside of the Person class
        # self.name = name
        # self.address = address

        # protected
        # - can be accessed in the same class and in the derived class
        # - should not be accessed outside of Person and derived class
        self._name = name
        self._address = address

        # private
        # - can not be accessed outside of Person class and in the derived class
        # self.__name = name
        # self.__address = address


class Student(Person):
    def __init__(self, name, address, roll_no):
        Person.__init__(self, name, address)
        self.__roll_no = roll_no

    def print_details(self):
        print(f"roll no = {self.__roll_no}")
        print(f"name = {self._name}")
        print(f"address = {self._address}")


s1 = Student("student1", "pune", 1)
print(f"s1 = {s1.__dict__}")
s1.print_details()
# print(f"address = {s1._address}")
print()
# s1._Person__name = "test"
# s1.print_details()

