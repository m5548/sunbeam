class Person:
    def __init__(self):
        self.name = ""
        self.address = ""


class Student(Person):

    # initializer for Student
    # def __init__(self):
         # initialize the Person class object within student class object
         # Person.__init__(self)


    pass


p1 = Person()
print(f"p1 = {p1.__dict__}")

# objects memory gets allocated at (s1) = 0x10000
# object gets initialized
# - Student.__init__(0x10000)
# - Student.__init__(s1)
# - s1.__init__()
s1 = Student()
print(f"s1 = {s1.__dict__}")

