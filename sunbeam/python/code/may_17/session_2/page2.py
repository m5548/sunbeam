# base class
# parent class
# super class
class Person:
    pass


# Student is-a Person
# Student is inherited from Person
# Student is derived from Person
# Person is the base class for Student

# derived class
# child class
# subclass
class Student(Person):
    pass


print(f"Student's base class = {Student.__base__}")