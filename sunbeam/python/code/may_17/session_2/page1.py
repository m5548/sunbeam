# bad code

# Student __ person
# Car __ Engine
# Player __ person
# Employee __ Person
# Company __ Employee

class Person:
    def __init__(self, name, address, age):
        self.__name = name
        self.__address = address
        self.__age = age

    def print_details(self):
        print(f"name = {self.__name}")
        print(f"address = {self.__address}")
        print(f"age = {self.__age}")


class Student:
    def __init__(self, name, address, age, roll_no, school):
        # self.__name = name
        # self.__address = address
        # self.__age = age

        # Student has-a Person
        self.__person = Person(name, address, age)
        self.__roll_no = roll_no
        self.__school = school

    def print_details(self):
        # print(f"name = {self.__name}")
        # print(f"address = {self.__address}")
        # print(f"age = {self.__age}")
        self.__person.print_details()
        print(f"roll_no = {self.__roll_no}")
        print(f"school = {self.__school}")


p1 = Person("person1", "pune", 40)
p1.print_details()

print()

s1 = Student("student1", "mumbai", 20, 1, "school 1")
s1.print_details()

