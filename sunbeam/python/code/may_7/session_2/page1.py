# collection: list


def function1():
    # empty list
    list1 = []
    print(f"list1 = {list1}, type = {type(list1)}")

    # empty list
    list2 = list()
    print(f"list2 = {list2}, type = {type(list2)}")


# function1()


def function2():
    # list of numbers (int)
    list1 = [10, 20, 30, 40, 50]
    print(f"list1 = {list1}, type = {type(list1)}")

    # list of number (int)
    list2 = list([10, 20, 30, 40, 50])
    print(f"list2 = {list2}, type = {type(list2)}")

    # list of number (int)
    # typecasting
    list3 = list(list1)
    print(f"list3 = {list3}, type = {type(list3)}")


# function2()


def function3():
    # list of strings
    countries1 = ["india", "usa", "uk", "japan"]
    print(f"countries1 = {countries1}, type = {type(countries1)}")

    countries2 = list(["india", "usa", "uk", "japan"])
    print(f"countries2 = {countries2}, type = {type(countries2)}")


# function3()


def function4():
    # list of mixed values
    list1 = [10, True, "india", False, 20.0, "usa", False, 30.0, "uk", 40, "japan"]
    print(f"list1 = {list1}, type = {type(list1)}")

    list2 = list([10, True, "india", False, 20.0, "usa", False, 30.0, "uk", 40, "japan"])
    print(f"list2 = {list2}, type = {type(list2)}")


# function4()


def function5():
    # list of sequential numbers
    list1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    print(f"list1 = {list1}, type = {type(list1)}")

    # list of sequential numbers
    # start: start number (0)
    # stop : stop number (excluded from list)
    # step : way to get the next number (1)
    list2 = list(range(0, 11, 1))
    print(f"list2 = {list2}, type = {type(list2)}")

    list3 = list(range(0, 11, 2))
    print(f"list3 = {list3}, type = {type(list3)}")

    list4 = list(range(1, 11, 2))
    print(f"list4 = {list4}, type = {type(list4)}")

    # start = 0, stop = 11, step = 1
    list5 = list(range(11))
    print(f"list5 = {list5}, type = {type(list5)}")


function5()














