def function1():
    # list of int numbers
    list1 = [10, 20, 30, 40, 50]

    print(f"number of values inside list1 = {len(list1)}")

    # iterate the list1
    for number in list1:
        print(f"number = {number}")


# function1()


def function2():
    # list of int numbers
    list1 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    # for number in list1:
    #     print(f"number = {number}")

    # print(f"value at 0th position = {list1[0]}")
    # print(f"value at 1st position = {list1[1]}")
    # print(f"value at 2nd position = {list1[2]}")

    # list_positions = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    # list_positions = range(0, 10, 1)
    # list_positions = range(10)

    # size = len(list1)
    # list_positions = list(range(size))

    list_positions = list(range(2, 7))
    # for <temp variable> in <collection>:
    for index in list_positions:
        print(f"position = {index}, value = {list1[index]}")


# function2()


def function3():
    countries = ["india", "usa", "uk", "japan"]

    # print all countries using values
    for country in countries:
        print(f"country = {country}")

    print()

    # print all countries using positions
    for index in range(len(countries)):
        print(f"country = {countries[index]}")


# function3()


def function4():
    numbers = [1, 2, 3, 4, 5]

    # print square of every number
    for number in numbers:
        print(f"number = {number}, square = {number ** 2}")

    print()

    # print cube of every number
    for number in numbers:
        print(f"number = {number}, cube = {number ** 3}")


# function4()


def function5():
    numbers = [1, 12, 21, 13, 34, 51, 60, 77, 58, 79, 10]

    # print even numbers
    for number in numbers:
        if number % 2 == 0:
            print(f"number = {number}")

    print()

    # print odd numbers
    for number in numbers:
        if number % 2 != 0:
            print(f"number = {number}")


function5()
