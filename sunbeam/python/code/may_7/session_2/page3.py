def function1():
    numbers = [10, 20, 30, 40, 50]

    print(f"length of numbers = {len(numbers)}")
    print(numbers)

    # appending a value (add a new value as a last value in the list)
    numbers.append(60)
    print(f"length of numbers = {len(numbers)}")
    print(numbers)

    numbers.append(70)
    print(f"length of numbers = {len(numbers)}")
    print(numbers)


# function1()


def function2():
    numbers = [1, 12, 21, 13, 34, 51, 60, 77, 58, 79, 10]

    # separate even and odd numbers into two collection
    # even_numbers and odd_numbers

    even_numbers = []
    odd_numbers = []

    for number in numbers:
        if number % 2 == 0:
            even_numbers.append(number)
        else:
            odd_numbers.append(number)

    print(f"even numbers = {even_numbers}")
    print(f"odd numbers = {odd_numbers}")


# function2()


def function3():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    # add 15 in between 10 and 20 (insert 15 at 1st position)
    numbers.insert(1, 15)
    print(numbers)

    # add 25 in between 20 and 30
    numbers.insert(3, 25)
    print(numbers)

    # add 35 in between 30 and 40
    numbers.insert(5, 35)
    print(numbers)

    # add 45 in between 40 and 50
    numbers.insert(7, 45)
    print(numbers)


# function3()


def function4():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    # numbers.append(60)
    # numbers.insert(5, 60)
    # print(numbers)

    # numbers.append(60)
    # numbers.insert(10, 60)
    print(numbers)
    
    numbers.insert(0, 0)
    print(numbers)


function4()
