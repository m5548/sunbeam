def function1():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    # remove the last value
    numbers.pop()
    print(numbers)

    # remove the last value
    numbers.pop()
    print(numbers)


# function1()


def function2():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    # delete value at position 2
    numbers.pop(2)
    print(numbers)

    numbers.pop(3)
    print(numbers)


# function2()


def function3():
    numbers = [10, 20, 30, 40, 50]
    print(numbers)

    # remove first occurance of value 30
    numbers.remove(30)
    print(numbers)


# function3()


def function4():
    # collection of values
    countries = ["india", "usa", "uk", "china", "japan", "pakistan"]
    print(countries)

    # delete china by using position
    countries.pop(3)
    print(countries)

    # delete pakistan using value
    countries.remove("pakistan")
    print(countries)


function4()
