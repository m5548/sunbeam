# scope

# global
# - which is declared outside of any function or class
# - can be accessed anywhere in the program
# - will stay in the memory till the program lasts
num = 100
print(f"outside of any function num = {num}")

# can modify the value outside of any function
num = 200
print(f"outside of any function num = {num}")

def function1():
    print("inside function1")

    # can be accessed inside any function as well
    print(f"num = {num}")

    # local scope
    # - declared within a function
    # - can be accessed only within the function it is declared
    addition = 10 + 20
    print(f"addition = {addition}")


# function1()

# num is global variable and can be accessed anywhere
# print(f"outside of function1, num = {num}")

# local variable addition can be accessed only within function1
# print(f"outside of function1, addition = {addition}")


def function2():
    print("inside function2")

    addition = 100
    print(f"addition = {addition}")

    # variable num will be re-declared as local variable
    # local
    num = 400
    print(f"num = {num}")


# function2()


def function3():
    print("inside function3")

    # do not re-declare num with local scope
    # instead let me modify the exiting (global) num
    global num
    num = 400
    print(f"num = {num}")


function3()

# num is global variable and can be accessed anywhere
print(f"outside of function3, num = {num}")


def function4():
    print(f"inside function4")
    global num
    num = 500
    print(f"num = {num}")


function4()

# num is global variable and can be accessed anywhere
print(f"outside of function4, num = {num}")















