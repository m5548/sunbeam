# empty
def function1():
    pass


# parameterless function
def function2():
    print("inside function2")


# function2()


# parameterized function
# optional parameters
# - default value of parameter
def function3(p1, p2=20):
    print(f"inside function3")
    print(f"p1 = {p1}, p2 = {p2}")


# positional parameters
# function3(10, 20)
# function3(p1=10, p2=20)
# function3(p2=20, p1=10)

# p2 will use the default value as 20
# function3(10)

# p2 will store value 50
# function3(p1=10, p2=50)


# required: p1, p2
# optional: p3, p4
def function4(p1=10, p2=10, p3=80, p4=100):
    print(f"inside function4")
    print(f"p1 = {p1}, p2 = {p2}, p3 = {p3}, p4 = {p4}")


# p1 = 10, p2 = 20, p3 = 30, p4 = 40
function4(10, 20, 30, 40)

# p1 = 10, p2 = 20, p3 = 30, p4 = 40
function4(p1=10, p2=20, p3=30, p4=40)

# p1 = 10, p2 = 20, p3 = 30, p4 = 100
function4(10, 20, 30)

# p1 = 10, p2 = 20, p3 = 80, p4 = 100
function4(10, 20)

# p1 = 20, p2 = 10, p3 = 80, p4 = 100
function4(p2=10, p1=20)

# p1 = 10, p2 = 20, p3 = 80, p4 = 100
function4(10, 20)

# p1 = 10, p2 = 50, p3 = 30, p4 = 20
function4(p4=20, p1=10, p3=30, p2=50)

# error: p2 is required and its missing
# function4(p1=20, p3=30)
