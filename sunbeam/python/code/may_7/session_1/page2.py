num1 = 100
print(f"num1 = {num1}, type = {type(num1)}")

num2 = num1
print(f"num2 = {num2}, type = {type(num2)}")

num2 = 200
print(f"num1 = {num1}, type = {type(num1)}")
print(f"num2 = {num2}, type = {type(num2)}")


def function1():
    print("inside function1 line-1")
    print("inside function1 line-2")
    print("inside function1 line-3")


# function1()

# function alias
# another function name given to an existing function
# another function reference to existing function
my_function1 = function1
my_function1()

print(f"function1 = {function1}, type = {type(function1)}")
print(f"my_function1 = {my_function1}, type = {type(my_function1)}")


def function2():
    print("inside function2")


# function2()
# function1()
#
# print()
#
# function2 = function1
#
# function2()
# function1()

def function3():
    print("inside function3")


# create a function alias named my_function3 and my_function4 for function3
my_function3 = function3
my_function4 = function3
