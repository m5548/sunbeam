# non-returning function
# - function does not return value
# - every function in python by default returns None (nothing)
# - function does not have any return statement
def add1(p1, p2):
    print(f"{p1} + {p2} = {p1 + p2}")


add1(10, 20)
# addition1 = add1(10, 20)
# print(f"addition1 = {addition1}")
print(f"add1 = {add1(30, 40)}")


# returning function
# - a function which returns a value
def add2(p1, p2):
    return p1 + p2


# addition of parameters is getting captured in a variable addition
# addition = add2(10, 20)
# print(f"addition = {addition}")

# print(f"addition = {add2(10, 20)}")

# None is a python's in built value
# none_value = None
# print(f"non_value = {none_value}, type = {type(none_value)}")
