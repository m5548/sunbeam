# function definition
def function1():
    """this is doc string of function1"""
    print("this is my function1")


# function call
# function1()

num = 100
print(f"num = {num}, type = {type(num)}")
print(f"function1 = {function1}, type = {type(function1)}")


