# numpy
# pandas
# matplotlib
# seaborn
# openCV
# scikit learn
# tensorflow
# selenium
# bs4
# flask
# pytorch
# pillow

# linux or mac
# sudo pip3 install <package name>
# python3 -m pip install <package name>

# windows
# pip3 install <package name>
# python3 -m pip install <package name>
