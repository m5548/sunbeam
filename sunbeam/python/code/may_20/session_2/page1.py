def function1():
    # import my_math module
    # - import all entities (functions/variables/classes) from my_math

    print(f"page1 module name is: {__name__}")
    import my_math

    my_math.add(10, 20)
    my_math.divide(30, 20)
    my_math.multiply(40, 50)
    my_math.subtract(100, 20)


# function1()


def function2():
    # import my_math with an alias mm
    # - alias: temporary name given to an existing module
    import my_math as mm
    mm.add(10, 20)
    mm.divide(30, 20)
    mm.multiply(40, 50)
    mm.subtract(100, 20)


# function2()


def add(p1, p2):
    print(f"add from page1")
    print(f"p1 + p2 = {p1 + p2}")


def function3():
    # only import add function from my_math module
    # from my_math import add
    # add(10, 20)

    # only import divide function from my_math module
    # from my_math import divide
    # divide(30, 10)

    # import only add and divide from my_math
    from my_math import add, divide
    add(10, 20)
    divide(30, 10)


# function3()


def function4():
    # from my_math only import add with an alias my_add
    from my_math import add as my_add
    from my_math import multiply as my_multiply

    my_add(10, 20)
    print()
    add(10, 20)

    my_multiply(30, 40)
    # multiply(30, 40)


function4()
