def function1():
    import os

    print(f"working directory: {os.getcwd()}")
    print(f"cpu count = {os.cpu_count()}")

    # used to delete a file
    # os.unlink("my_dir")
    # os.remove("my_dir")

    # used to delete a directory
    os.rmdir("my_dir")

    # os.mkdir("my_dir")


# function1()


def function2():
    import time

    time.sleep(5)
    print("this is a test statement")


function2()


def function3():
    import os
    import time
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt


function3()