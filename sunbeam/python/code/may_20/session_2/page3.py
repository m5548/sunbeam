def function1():
    # import entire module
    import sunbeam.faculty_pkg.faculty

    f1 = sunbeam.faculty_pkg.faculty.Faculty("f1", "s1")
    f1.print_details()


# function1()


def function2():
    # import entire module with an alias
    import sunbeam.faculty_pkg.faculty as mf

    f1 = mf.Faculty("f1", "s1")
    f1.print_details()


# function2()


def function3():
    # import only faculty module from faculty_pkg
    from sunbeam.faculty_pkg import faculty

    f1 = faculty.Faculty('f1', 's2')
    f1.print_details()


# function3()


def function4():
    from sunbeam.faculty_pkg.faculty import Faculty as MyFaculty

    f1 = MyFaculty('f1', 's1')
    f1.print_details()


function4()
