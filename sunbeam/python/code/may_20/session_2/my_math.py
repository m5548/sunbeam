# module:
# - any file having an extension as .py
# - every module has a name __name__
# - the running module is called as __main__ module

def add(p1, p2):
    print(f"{p1} + {p2} = {p1 + p2}")


def subtract(p1, p2):
    print(f"{p1} - {p2} = {p1 - p2}")


def divide(p1, p2):
    print(f"{p1} / {p2} = {p1 / p2}")


def multiply(p1, p2):
    print(f"{p1} * {p2} = {p1 * p2}")


# check if the running module is my_math
if __name__ == "__main__":
    print(f"my_math module name is: {__name__}")

    add(10, 20)
    subtract(20, 10)
    multiply(40, 50)
    divide(20, 10)
