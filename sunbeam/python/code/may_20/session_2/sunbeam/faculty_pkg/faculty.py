class Faculty:
    def __init__(self, name, subject):
        self.__name = name
        self.__subject = subject

    def print_details(self):
        print(f"name = {self.__name}")
        print(f"subject = {self.__subject}")

