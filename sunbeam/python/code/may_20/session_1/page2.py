# file IO

def write_to_file():
    data = input("enter your data: ")

    # step 1: open a file
    # param1: file name or path
    # param2:
    # - reason you want to open the file
    #   - w: write
    #   - r: read
    #   - a: append
    # - type of the file
    #   - t: text (default)
    #   - b: binary file
    file = open('myfile.txt', 'w')

    # step 2: perform operation
    file.write(data)

    # step 3: close the file
    file.close()


# write_to_file()


def append_data_to_file():
    data = input("enter your data: ")

    # step 1
    file = open('myfile.txt', 'a')

    # step 2
    file.write(data)

    # step 3
    file.close()


# append_data_to_file()

def append_multiple_lines():
    file = open('myfile.txt', 'a')
    while True:
        answer = input("Do you want to enter data? (y/n): ")
        if answer == 'y':
            data = input("enter your data: ")
            file.write(data)

            # write new line character
            file.write('\n')
        else:
            print("bye bye..")
            break
    file.close()


append_multiple_lines()
