def write_data():
    # file = open('myfile.txt', 'w')
    # file.write('this is my data')
    # file.close()

    # auto closing the file
    with open('myfile.txt', 'w') as file:
        file.write('this is my data')
        file.write('\n')
        file.write('this is my data')
        file.write('\n')
        file.write('this is my data')
        file.write('\n')
        file.write('this is my data')
        file.write('\n')
        file.write('this is my data')
        file.write('\n')
        file.write('this is my data')
        print(file)

    # never access the file after with block
    # print(file)


write_data()


def read_data():
    # auto closing file
    with open('myfile.txt', 'r') as file:
        data = file.read()
        print(f"data = {data}")


# read_data()

# change the contents of the file to
# this is my new data

# read two files and write the contents of them
# into third file
