def read_file_1():
    file = open('myfile.txt', 'r')

    # read entire data at a time from the file
    data = file.read()
    print(data)

    file.close()


# read_file_1()


def read_file_2():
    file = open('myfile.txt', 'r')
    print(f"file = {file}, type = {type(file)}")

    # read only first 4 bytes from the file
    data = file.read(4)
    print(f"data = {data}")

    print(f"file offset: {file.tell()}")

    # set the file offset to 5th character
    file.seek(5)

    print(f"file offset: {file.tell()}")

    data = file.read(2)
    print(f"data = {data}")

    print(f"file offset: {file.tell()}")

    file.seek(11)
    print(f"file offset: {file.tell()}")

    data = file.read(5)
    print(f"data = {data}")

    # read word "second" from the file
    file.seek(34)
    data = file.read(6)
    print(f"data = {data}")

    file.close()


# read_file_2()

def read_file_3():
    file = open('myfile.txt', 'r')

    lines = file.readlines()
    print(lines)

    # search for word "fifth"
    for line in lines:
        # print(line)
        try:
            print(line.index("fifth"))
        except:
            pass

    file.close()


# read_file_3()





