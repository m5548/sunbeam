# conventions
# - no underscore prefix: public
# - single underscore prefix: protected
# - double underscores prefix: private
# - double underscores as prefix and suffix: system


class BaseClass:
    def __init__(self, data):
        print("inside __init__")

        # private: can be accessed only within the same class
        self.__data = data
        # self.__dict__ = {}

    def print_details(self):
        print("-- called BaseClass --")
        print(f"data = {self.__data}")
        self.__dummy_method()
        print()

    def __dummy_method(self):
        print("this is a dummy method")


class DerivedClass(BaseClass):
    def __init__(self, data):
        BaseClass.__init__(self, data)

    def print_details(self):
        print("-- called DerivedClass --")

        # can not access the __data here as
        # it is a private member of BaseClass
        print(f"data = {self.__data}")
        print()


# d1 = DerivedClass('1')
# d1.print_details()
# d1.__dummy_method()

b1 = BaseClass('2')
# b1.__dummy_method()
# b1.print_details()

# print(BaseClass.__dict__)
# num = 100
# print(num)
# print(b1.__dict__)


num = 100