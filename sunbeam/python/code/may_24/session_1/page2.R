# declare a variable
num = 100
print(num)

# print the value along with constant string
# 1: using cat
cat("num =", num)

# 2: using paste
print(paste("num =", num))

# 3: using paste0
print(paste0("num = ", num))
