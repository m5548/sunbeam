# multi-dimensional collection => collection of collections
# - list of lists
# - list of tuples
# - tuple of tuples
# - tuple of lists


def function1():
    # one-dimensional collection
    list1 = [10, 20, 30, 40, 50]
    print(f"list1 = {list1}")

    print(f"list1[2] = {list1[2]}")

    for value in list1:
        print(f"value = {value}")


# function1()


def function2():
    # two-dimensional collection
    # list of lists
    list1 = [
        [10, 20, 30],
        [40, 50, 60],
        [70, 80, 90]
    ]

    print(f"list1[1]     = {list1[1]}")
    print(f"list1[0][1]  = {list1[0][1]}")
    print(f"list1[1][2]  = {list1[1][2]}")
    print(f"list1[2][0]  = {list1[2][0]}")

    print()

    # for rows
    for values in list1:
        print(f"values = {values}")

        # for columns
        for value in values:
            print(f"value = {value}")

        print()


# function2()


def function3():
    # two-dimensional collection
    # list of tuples
    collection = [
        (10, 20, 30),
        (40, 50, 60),
        (70, 80, 90)
    ]

    # get all the values
    for values in collection:
        for value in values:
            print(f"value = {value}")

        print()


# function3()


def function4():
    # two-dimensional collection
    # list of tuples
    collection = [
        (10, 20, 30, 40, 50, 60),
        (70, 80, 90),
        (100, 110, 120, 130)
    ]

    for values in collection:
        print(f"values = {values}")
        for value in values:
            print(f"value = {value}")

        print()


function4()