# dictionary
# - collection of key-value pairs


def function1():
    # name: "person1", address: "pune", phone: "+9123324"
    person = {
        "name": "person1",
        "address": "pune",
        "phone": "+9123324"
    }

    print(f"person = {person}, type = {type(person)}")


# function1()


def print_person_info(person):
    print(f"name = {person['name']}")
    print(f"address = {person['address']}")
    print(f"phone = {person['phone']}")
    print()


def function2():
    person1 = {
        "name": "person1",
        "address": "pune",
        "phone": "+9123234"
    }
    print_person_info(person1)

    person2 = {
        "phone": "+91567765",
        "name": "person2",
        "address": "mumbai"
    }
    print_person_info(person2)

    # .
    # .

    person3 = {
        "address": "satara",
        "phone": "+91234344",
        "name": "person3",
        "email": "person3@test.com"
    }
    print_person_info(person3)


function2()