def function1():
    name1 = "person1"
    address1 = "pune"
    phone1 = "+91234345"
    print(f"name = {name1}, address = {address1}, phone = {phone1}")

    name2 = "person2"
    address2 = "mumbai"
    phone2 = "+9145335"
    print(f"name = {name2}, address = {address2}, phone = {phone2}")


# function1()


def print_person_info(person):
    print(f"name = {person[0]}")
    print(f"address = {person[1]}")
    print(f"phone = {person[2]}")
    print()


def function2():
    person1 = ["person1", "pune", "+9123434"]
    print_person_info(person1)

    person2 = ["person2", "mumbai", "+9153336"]
    print_person_info(person2)

    # .
    # .

    person3 = ["satara", "+912342345", "person3"]
    print_person_info(person3)


function2()
