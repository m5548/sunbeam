def function1():
    # dictionary
    car = {"model": "i20", "company": "hyundai", "price": 7.5}
    print(f"car = {car}, type = {type(car)}")

    print(f"keys = {car.keys()}")
    print(f"values = {car.values()}")

    print(f"model = {car['model']}")
    print(f"company = {car['company']}")
    print(f"price = {car['price']}")


# function1()


def function2():
    mobile = {"model": "iPhone XS Max", "company": "apple", "price": 144000, "ram": "6GB"}

    # print all values dynamically
    # key = value

    # step 1: get all keys
    keys = mobile.keys()
    # keys = ['model', 'company', 'price', 'ram']
    print(f"keys = {keys}")

    # step 2: run a for loop over the keys
    for key in keys:
        # step 3: for every key get the value from mobile object
        print(f"{key} = {mobile[key]}")
        # print(f"key = {key}")


# function2()


def function3():
    book = {"author": "Aurelien", "name": "Hans-On Machine Learning", "price": 2275, "publisher": "O'REILLY"}

    # print all the values for all the keys
    keys = book.keys()
    for key in keys:
        print(f"{key} = {book[key]}")


# function3()


def function4():
    book = {"author": "Aurelien", "name": "Hans-On Machine Learning", "price": 2275, "publisher": "O'REILLY"}

    # get value for a key
    print(f"author = {book.get('author')}")
    print(f"author = {book['author']}")

    print()

    # if key does not exist, get method will return None (application wont crash)
    print(f"ISBN = {book.get('isbn')}")

    # if key does not exist, application will receive an exception KeyError and will crash
    print(f"ISBN = {book['isbn']}")


function4()
