def function1():
    # list of dictionaries
    persons = [
        {"name": "person1", "address": "pune", "email": "person1@test.com"},
        {"name": "person2", "address": "mumbai", "email": "person2@test.com"},
        {"name": "person3", "address": "satara", "email": "person3@test.com"}
    ]

    print(f"persons = {persons}")

    # get every dictionary
    for person in persons:
        print(f"person = {person}")

        # get the information about every person
        print(f"name = {person['name']}")
        print(f"address = {person['address']}")
        print(f"email = {person['email']}")
        print()


# function1()


def function2():
    # list of dictionaries
    persons = [
        {"name": "person1", "address": "pune", "email": "person1@test.com", "phone": "+9145354"},
        {"name": "person2", "address": "mumbai", "email": "person2@test.com"},
        {"name": "person3", "address": "satara", "email": "person3@test.com"}
    ]

    for person in persons:
        keys = person.keys()
        for key in keys:
            print(f"{key} = {person[key]}")

        print()


# function2()


def function3():
    cars = [
        {"model": "i20", "company": "hyundai", "price": 7.5},
        {"model": "nano", "company": "tata", "price": 2.5, "color": "yellow"},
        {"model": "discovery", "company": "range rover", "price": 85}
    ]

    # print all the information about every car
    for car in cars:
        print(f"model = {car.get('model')}")
        print(f"company = {car.get('company')}")
        print(f"price = {car.get('price')}")
        print(f"color = {car.get('color')}")
        print()

    print(f"-" * 80)

    for car in cars:
        keys = car.keys()
        for key in keys:
            print(f"{key} = {car[key]}")

        print()


function3()