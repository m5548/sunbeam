def function1():
    # tuple is immutable object
    # once created, CAN NOT be modified
    t1 = (10, 20, 30, 40, 50)
    print(f"t1 = {t1}, type = {type(t1)}")

    t2 = tuple([10, 20, 30, 40, 50])
    print(f"t2 = {t2}, type = {type(t2)}")


# function1()


def function2():
    # tuple
    t1 = 10, 20, 30
    print(f"t1 = {t1}, type = {type(t1)}")


# function2()


def function3():
    # tuple
    t1 = 10, 20

    # extracting/unpacking values from tuple
    value1, value2 = t1
    # value1, value2 = 10, 20

    print(f"value1 = {value1}, value2 = {value2}")


# function3()


def function4():
    v1, v2, v3, v4, v5, v6 = 10, 20, 30, 40, 50, 60
    print(f"v1 = {v1}, v2 = {v2}, v3 = {v3}")


# function4()


def function5():
    v1 = 10
    v2 = 20
    print(f"v1 = {v1}, v2 = {v2}")

    # swapping two values
    # temp = v1
    # v1 = v2
    # v2 = temp
    # print(f"v1 = {v1}, v2 = {v2}")

    # swapping two values
    v1, v2 = v2, v1
    print(f"v1 = {v1}, v2 = {v2}")


# function5()


def function6():
    v1, v2 = (10, 20, 30, 40, 50), (60, 70)
    print(f"v1 = {v1}, v2 = {v2}")

    v1, v2 = v2, v1
    print(f"v1 = {v1}, v2 = {v2}")

    str1, str2 = "test1", "test2"
    print(f"str1 = {str1}, str2 = {str2}")

    str1, str2 = str2, str1
    print(f"str1 = {str1}, str2 = {str2}")


# function6()


def math_operations_1(p1, p2):
    addition = p1 + p2
    subtraction = p1 - p2
    multiplication = p1 * p2
    division = p1 / p2

    # returning a tuple of 4 values
    return addition, subtraction, multiplication, division


# addition, subtraction, multiplication, division = math_operations_1(20, 10)
# print(f"addition = {addition}, subtraction = {subtraction}, multiplication = {multiplication}, division = {division}")


def math_operations_2(p1, p2):
    # returning a tuple of 4 values
    return p1 + p2, p1 - p2, p1 * p2, p1 / p2


answers = math_operations_1(20, 10)
print(f"answers = {answers}, type = {type(answers)}")

# addition, subtraction, multiplication, division = math_operations_2(20, 10)
# print(f"addition = {addition}, subtraction = {subtraction}, multiplication = {multiplication}, division = {division}")
