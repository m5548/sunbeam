def function1():
    # list
    list_1 = [10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50]
    print(f"list_1 = {list_1}, type = {type(list_1)}")

    # tuple
    tuple_1 = (10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50)
    print(f"tuple_1 = {tuple_1}, type = {type(tuple_1)}")

    # set
    set_1 = {10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50}
    print(f"set_1 = {set_1}, type = {type(set_1)}")


# function1()


def function2():
    # empty list
    list1 = []
    print(f"list1 = {list1}, type = {type(list1)}")

    # empty list
    list2 = list()
    print(f"list2 = {list2}, type = {type(list2)}")

    print()

    # empty tuple
    tuple1 = ()
    print(f"tuple1 = {tuple1}, type = {type(tuple1)}")

    # empty tuple
    tuple2 = tuple()
    print(f"tuple2 = {tuple2}, type = {type(tuple2)}")

    print()

    # empty dictionary
    # set_1 = {}
    # print(f"set_1 = {set_1}, type = {type(set_1)}")

    # empty set
    set_2 = set()
    print(f"set_2 = {set_2}, type = {type(set_2)}")


# function2()


def function3():
    s1 = {10, 20, 30, 40}
    s2 = {30, 40, 50, 60}

    print(f"s1 = {s1}, s2 = {s2}")
    print()

    print(f"s1 intersection s2 = {s1.intersection(s2)}")
    print(f"s2 intersection s1 = {s2.intersection(s1)}")

    print()

    print(f"s1 union s2 = {s1.union(s2)}")
    print(f"s2 union s1 = {s2.union(s1)}")

    print()

    print(f"s1 difference s2 = {s1.difference(s2)}")
    print(f"s2 difference s1 = {s2.difference(s1)}")


# function3()


def function4():
    # get 10 values from user
    # find the unique values from them

    values = set()
    for index in range(10):
        values.add(int(input("enter a number: ")))

    print(f"values = {values}")


# function4()


def function5():
    # get 10 values from user
    # find the unique values from them

    values = []
    for index in range(10):
        values.append(int(input("enter a number: ")))

    print(f"all values = {values}")
    print(f"unique values = {set(values)}")


# function5()


def function6():
    # list
    list_1 = [10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50]
    print(f"list_1 = {list_1}, type = {type(list_1)}")

    # tuple
    tuple1 = tuple(list_1)
    print(f"tuple1 = {tuple1}, type = {type(tuple1)}")

    # mutable set
    set1 = set(list_1)
    set1.add(60)
    print(f"set1 = {set1}, type = {type(set1)}")

    # immutable set
    set2 = frozenset(list_1)
    print(f"set2 = {set2}, type = {type(set2)}")


function6()


