def function1():
    t1 = 10, 20, 30, 40, 50, 60, 70, 80, 90, 100

    # positive indexing
    print(f"t1[0] = {t1[0]}")
    print(f"t1[5] = {t1[5]}")
    print(f"t1[9] = {t1[len(t1) - 1]}")

    print()

    # negative indexing
    print(f"t1[-10] = {t1[-len(t1)]}")
    print(f"t1[-5]  = {t1[-5]}")
    print(f"t1[-1]  = {t1[-1]}")


# function1()


def function2():
    t1 = 10, 20, 30, 40, 50, 60, 70, 80, 90, 100

    # slicing
    # 30, 40, 50, 60, 70
    print(f"t1[2:7] = {t1[2:7]}")

    # 70, 80, 90, 100
    print(f"t1[6:10] = {t1[6:10]}")
    print(f"t1[6:]   = {t1[6:]}")

    # 10, 20, 30, 40, 50
    print(f"t1[0:5] = {t1[0:5]}")
    print(f"t1[:5]  = {t1[:5]}")


function2()
