def function1():
    name = input("what is your name? ")
    address = input("what is your address? ")
    phone = input("what is your phone number? ")

    print(f"name = {name}, address = {address}, phone = {phone}")


# function1()


def function2():
    print("enter 5 values")

    values = []
    for index in range(5):
        value = input(f"enter value {index + 1}: ")
        values.append(value)

    print(values)


# function2()


def function3():
    # convert a string value to int
    print(f"int version of '10' = {int('10')}")

    # convert a string value to float
    print(f"float version of '10.50' = {float('10.50')}")

    # convert a string value to bool
    # 'True'    => True
    # 'False'   => False
    print(f"bool version of 'True' = {bool('True')}")
    print(f"bool version of 'False' = {bool('False')}")

    print()

    # convert any data type to string
    print(f"str version of 10 = {str(10)}")
    print(f"str version of 10.50 = {str(10.50)}")
    print(f"str version of True = {str(True)}")
    print(f"str version of False = {str(False)}")

    print()

    # convert bool to int
    print(f"int value of True = {int(True)}")
    print(f"int value of False = {int(False)}")

    # convert int to bool
    print(f"bool value of 1 = {bool(1)}")
    print(f"bool value of 0 = {bool(0)}")


# function3()


def function4():
    # get number input from user and check if it is an even number
    # number = input("enter a number: ")
    # number = int(number)

    number = int(input("enter a number: "))
    if number % 2 == 0:
        print("this is an even number")
    else:
        print("this is NOT an even number")


function4()
