# load ggplot package
library(ggplot2)

ages = c(25, 28, 30, 33, 35, 37, 40, 45)
salaries = c(20, 30, 35, 40, 40, 50, 80, 85)

# create a data frame
df = data.frame(
  age = ages,
  salary = salaries
)

print(df)

# scatter plot
ggplot(df, aes(x = age, y = salary)) +
  geom_point() +
  labs(title = "Age vs Salary") +
  theme_grey()


# line plot
ggplot(df, aes(x = age, y = salary)) +
  geom_line() +
  labs(title = "Age vs Salary") +
  theme_grey()