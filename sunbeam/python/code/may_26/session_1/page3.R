# read the data from external file
df = read.csv("Data.csv", stringsAsFactors = FALSE)
print(df)

# data.frame
print(class(df))

# list
print(typeof(df))

# get column names
print(names(df))

# get info about the df
str(df)

# get statistical information about the df
print(summary(df))

# get first 5 records
print(head(df, 5))

# get last 5 records
print(tail(df, 5))

# get number of rows
print(nrow(df))

# get number of columns
print(ncol(df))

# get column names
print(colnames(df))

# get row names
print(rownames(df))

