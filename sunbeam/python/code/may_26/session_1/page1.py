import pandas as pd


def function1():
    df = pd.DataFrame([
        {"name": "person1", "age": "20", "email": "person1@test.com", "phone": "+913453455"},
        {"name": "person2", "age": "30", "email": "person2@test.com", "phone": "+923454335"},
        {"name": "person3", "age": "40", "email": "person3@test.com", "phone": "+913453456"},
        {"name": "person4", "age": "50", "email": "person4@test.com", "phone": "+913453457"}
    ])
    print(df)


# function1()


def function2():
    # create a data frame for cars

    df = pd.DataFrame([
        {"model": "i20", "company": "hyundai", "price": 7.5},
        {"model": "X5", "company": "BMW", "price": 35},
        {"model": "nano", "company": "Tata", "price": 2.5}
    ])

    print(df)


# function2()


def function3():
    df = pd.read_csv('Data.csv')
    print(df)
    print(len(df.columns))
    print()
    df.info()
    print()
    print(df.describe())
    print()
    print(df.head())
    print()
    print(df.tail())


function3()