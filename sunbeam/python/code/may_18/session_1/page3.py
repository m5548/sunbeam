# single / simple inheritance


# base class
class Person:
    def __init__(self, name):
        self._name = name


# derived class
class Player(Person):
    def __init__(self, name, team):
        Person.__init__(self, name)
        self._team = team

    def print_details(self):
        print(f"name = {self._name}")
        print(f"team = {self._team}")


player = Player('player 1', 'team1')
player.print_details()
