# multiple inheritance


class Developer:
    def __init__(self, name, technology):
        self._name = name
        self._technology = technology


class Ops:
    def __init__(self, name, department):
        self._name = name
        self._department = department


class DevOps(Developer, Ops):
    def __init__(self, name, technology, department):
        Developer.__init__(self, name, technology)
        Ops.__init__(self, name, department)