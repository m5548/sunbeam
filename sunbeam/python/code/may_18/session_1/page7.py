# hierarchical inheritance

class Person:
    def __init__(self, name, age):
        self._name = name
        self._age = age


class Employee(Person):
    def __init__(self, name, age, id):
        Person.__init__(self, name, age)
        self._id = id


class Player(Person):
    def __init__(self, name, age, team):
        Person.__init__(self, name, age)
        self._team = team


class Student(Person):
    def __init__(self, name, age, school):
        Person.__init__(self, name, age)
        self._school = school
