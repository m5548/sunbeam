# multi-level inheritance

class Person:
    def __init__(self, name):
        self._name = name


class Employee(Person):
    def __init__(self, name, id):
        Person.__init__(self, name)
        self._id = id


class Manager(Employee):
    def __init__(self, name, id, department):
        Employee.__init__(self, name, id)
        self._department = department


