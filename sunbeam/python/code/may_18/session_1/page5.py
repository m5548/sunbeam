# multiple inheritance


class Faculty:
    def __init__(self, name, subject):
        self._name = name
        self._subject = subject


class LabAssistant:
    def __init__(self, name, lab):
        self._name = name
        self._lab = lab


class FacultyLabAssistant(Faculty, LabAssistant):
    def __init__(self, name, subject, lab):
        Faculty.__init__(self, name, subject)
        LabAssistant.__init__(self, name, lab)
