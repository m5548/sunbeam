# Person: name, age
# Employee: name, age, id
# Company: name, facility to add multiple employees

class Person:
    def __init__(self, name, age):
        self._name = name
        self._age = age


class Employee(Person):
    def __init__(self, name, age, id):
        Person.__init__(self, name, age)
        self._id = id

    def print_details(self):
        print(f"name: {self._name}")
        print(f"age: {self._age}")
        print(f"id: {self._id}")
        print()


class Company:
    def __init__(self, name, address):
        self.__name = name
        self.__address = address
        self.__employees = []

    def add_employee(self, name, age, id):
        employee = Employee(name, age, id)
        self.__employees.append(employee)

    def print_details(self):
        print(f"name = {self.__name}")
        print(f"address = {self.__address}")
        print('-' * 50)
        for employee in self.__employees:
            employee.print_details()


company1 = Company("company 1", "pune")
company1.add_employee("employee 1", 29, 1)
company1.add_employee("employee 2", 27, 2)
company1.add_employee("employee 3", 28, 3)
company1.add_employee("employee 4", 29, 4)
company1.add_employee("employee 5", 35, 5)
company1.print_details()

