class Number:
    def __init__(self, number):
        self.__number = number

    def print_details(self):
        print(f"number = {self.__number}")

    def __add__(self, other):
        # str.__add__(self.__number, other.__number)
        result = self.__number + other.__number
        return Number(result)


# n1.__number = test1
n1 = Number('test1')

# n2.__number = test2
n2 = Number('test2')

# result = n1.__add__(n2)
result = n1 + n2
print(f"result = {result}")
result.print_details()
