def function1():
    # int
    num1 = 10

    # int
    num2 = 20

    # internal: int.__add__(num1, num2)
    # internal: num1.__add__(num2)
    print(f"{num1} + {num2} = {num1 + num2}")


function1()


def function2():
    # str
    str1 = "test1"

    # str
    str2 = "test2"

    # internal: str.__add__(str1, str2)
    # internal: str1.__add__(str2)
    print(f"{str1} + {str2} = {str1 + str2}")


function2()

