class Animal:
    def __init__(self, name):
        self._name = name

    def make_sound(self):
        print(f"{self._name} does not make any sound")


class Lion(Animal):
    def __init__(self):
        Animal.__init__(self, 'Lion')

    def make_sound(self):
        print("Lion Roars")


class Cow(Animal):
    def __init__(self):
        Animal.__init__(self, 'Cow')

    def make_sound(self):
        print("Cow says moo moo")


class Dinosaur(Animal):
    pass


a1 = Animal("animal1")
a1.make_sound()

l1 = Lion()
l1.make_sound()

c1 = Cow()
c1.make_sound()

d1 = Dinosaur("Dinosaur")
d1.make_sound()