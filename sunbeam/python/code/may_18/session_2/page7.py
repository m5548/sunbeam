class Person:
    def __init__(self, name, age):
        self.__name = name
        self.__age = age

    def print_details(self):
        print(f"name = {self.__name}")
        print(f"age = {self.__age}")

    def __add__(self, p2):
        # self  => p1
        # other => p2

        # str.__add__(self.__name, other.__name)
        new_name = self.__name + p2.__name

        # int.__add__(self.__age, other.__age)
        new_age = self.__age + p2.__age

        return Person(new_name, new_age)


p1 = Person("person1", 20)
p2 = Person("person2", 25)

# result = p1.__add__(p2)
result = p1 + p2

# Person.print_details(result)
result.print_details()