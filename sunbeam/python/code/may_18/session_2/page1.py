# method overiding

class Person:
    def __init__(self, name, address, age):
        self._name = name
        self._address = address
        self._age = age

    def print_details(self):
        print(f"name = {self._name}")
        print(f"address = {self._address}")
        print(f"age = {self._age}")
        print()


class Student(Person):
    def __init__(self, name, address, age, roll_no):
        Person.__init__(self, name, address, age)
        self._roll_no = roll_no

    # student has overiden the print_details method
    def print_details(self):
        # print(f"name = {self._name}")
        # print(f"address = {self._address}")
        # print(f"age = {self._age}")

        # calling parent/base class's method
        Person.print_details(self)

        print(f"roll no = {self._roll_no}")
        print()


s1 = Student("student 1", "pune", 25, 1)
s1.print_details()
# s1.my_method()

p1 = Person("person1", "mumbai", 30)
p1.print_details()
