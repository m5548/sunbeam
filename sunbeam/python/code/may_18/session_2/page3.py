class Vehicle:
    def __init__(self, wheels, engine):
        self._wheels = wheels
        self._engine = engine

    def print_details(self):
        pass


class Bike(Vehicle):
    def __init__(self, engine, model, company, price):
        Vehicle.__init__(self, 2, engine)
        self._model = model
        self._company = company
        self._price = price

    def print_details(self):
        print("-- Bike details --")
        print(f"model = {self._model}")
        print(f"company = {self._company}")
        print(f"price = {self._price}")
        print(f"no of wheels = {self._wheels}")
        print(f"engine = {self._engine}")
        print()


class Car(Vehicle):
    def __init__(self, engine, model, company, price):
        Vehicle.__init__(self, 4, engine)
        self._model = model
        self._company = company
        self._price = price

    def print_details(self):
        print("-- Car details --")
        print(f"model = {self._model}")
        print(f"company = {self._company}")
        print(f"price = {self._price}")
        print(f"no of wheels = {self._wheels}")
        print(f"engine = {self._engine}")
        print()


class Person:
    def __init__(self, name):
        self.__name = name

        # person will add multiple vehicles
        # one person may own multiple vehicles
        # one to many relationship
        self.__vehicles = []

    def add_bike(self, engine, model, company, price):
        bike = Bike(engine, model, company, price)
        self.__vehicles.append(bike)

    def add_car(self, engine, model, company, price):
        car = Car(engine, model, company, price)
        self.__vehicles.append(car)

    def print_details(self):
        print(f"name = {self.__name}")
        print('-' * 50)
        for vehicle in self.__vehicles:
            print(f"type of vehicle = {type(vehicle)}")
            vehicle.print_details()


person1 = Person('person1')
person1.add_bike("new_engine", "CT100", "Honda", 30000)
person1.add_car("v2", "i20", "hyudai", 750000)
person1.print_details()
