# operator overloading
# - change the meaning of existing operators
#   by overiding method(s)
# - + -> __add__
# - - -> __sub__
# - / -> __truediv__
# - * -> __mul__

class MyNumber:
    def __init__(self, number):
        self.__number = number

    def print_details(self):
        print(f"number = {self.__number}")

    def __add__(self, other):
        print("inside MyNumber.__add__")
        result = self.__number + other.__number
        return MyNumber(result)


num1 = MyNumber(10)
num2 = MyNumber(20)

# internal: MyNumber.__add__(num1, num2)
# internal: num1.__add__(num2)
print(f"addition = {num1 + num2}")
addition = num1 + num2
print(f"addition = {addition}, type = {type(addition)}")
addition.print_details()
