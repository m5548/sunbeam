def function1():
    try:
        # try block
        num1 = int(input("enter number 1: "))
        num2 = int(input("enter number 2: "))
    except:
        # executes when there is an exception
        print(f"inside except block")
        print("You have entered wrong number(s)")
    else:
        # executes when there is no exception
        print(f"inside else block")
        print(f"{num1} + {num2} = {num1 + num2}")


# function1()


def function2():
    try:
        # try block
        num1 = int(input("enter number 1: "))
        num2 = int(input("enter number 2: "))
    except:
        # executes when there is an exception
        print(f"inside except block")
        print("You have entered wrong number(s)")
    else:
        # executes when there is no exception
        print(f"inside else block")
        print(f"{num1} + {num2} = {num1 + num2}")

        try:
            print(f"{num1} / {num2} = {num1 / num2}")
        except ZeroDivisionError:
            print("Denominator can not be Zero")

    finally:
        # close file
        print(f"inside finally block")


function2()