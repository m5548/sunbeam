def function1():
    try:
        # try block
        num1 = int(input("enter number 1: "))
        num2 = int(input("enter number 2: "))

        print(f"{num1} + {num2} = {num1 + num2}")
    except:
        # except block
        print("You have entered wrong number(s)")


# function1()

def handle_exception_value_error():
    print("You have entered wrong number(s)")


def handle_zero_division_error():
    print("denominator can not be zero")


def function2():
    try:
        # try block
        num1 = int(input("enter number 1: "))
        num2 = int(input("enter number 2: "))

        print(f"{num1} / {num2} = {num1 / num2}")
    except ValueError:
        # except block for handling int conversion (ValueError)
        # print("You have entered wrong number(s)")
        handle_exception_value_error()
    except ZeroDivisionError:
        # except block for handling zero division error
        # print("denominator can not be zero")
        handle_zero_division_error()
    except:
        # except block to handle any exception
        # generic except block
        print("error occurred")


# function2()


def function3():

    def get_number_from_user(message):
        # infinite loop
        while True:
            try:
                value = int(input(message))
                return value
            except ValueError:
                print("wrong input")

    try:
        num1 = get_number_from_user("enter number 1: ")
        num2 = get_number_from_user("enter number 2: ")

        print(f"{num1} / {num2} = {num1 / num2}")
    except ZeroDivisionError:
        print("Denominator CAN NOT be zero")


# function3()


def function4():
    try:
        # try block
        num1 = int(input("enter number 1: "))
        num2 = int(input("enter number 2: "))

        print(f"{num1} / {num2} = {num1 / num2}")
    except Exception as ex:
        # except block
        print(f"Exception raised: {ex}")


function4()