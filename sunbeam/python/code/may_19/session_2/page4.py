def is_prime_1(number):
    if number <= 3:
        print(f"{number} is prime")
    else:
        is_prime = True
        for index in range(2, number):
            if number % index == 0:
                is_prime = False
                break

        if is_prime:
            print(f"{number} is prime number")
        else:
            print(f"{number} is NOT prime")


def is_prime_2(number):
    if number <= 3:
        print(f"{number} is prime")
    else:
        for index in range(2, number):
            if number % index == 0:
                print(f"{number} is NOT prime")
                break
        else:
            # will be called only when the for loop executes successfully
            # print("inside else block")
            print(f"{number} is prime")


# is_prime_2(5)

for number in range(1, 20):
    is_prime_2(number)
