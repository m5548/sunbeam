# custom exception class
class InvalidAgeError(Exception):
    def __init__(self):
        Exception.__init__(self, "invalid age")


def function1():
    age = int(input("Enter your age: "))
    if (age < 20) or (age > 60):
        # raising an exception
        # raise Exception("invalid age")
        raise InvalidAgeError()

    print(f"age = {age}")
    return age


try:
    function1()
except Exception as ex:
    print(f"something is wrong: {ex}")
