# comparison operators


class MyClass:
    def __init__(self, number):
        self.__number = number

    def __str__(self):
        str_num = f"{self.__number}"
        return str_num

    def __eq__(self, other):
        return self.__number == other.__number

    def __ne__(self, other):
        return self.__number != other.__number

    def __lt__(self, other):
        return self.__number < other.__number

    def __gt__(self, other):
        return self.__number > other.__number

    def __le__(self, other):
        return self.__number <= other.__number

    def __ge__(self, other):
        return self.__number >= other.__number


m1 = MyClass(10)
m2 = MyClass(20)
m3 = m1

# print(f"{m1} == {m3} = {m1 == m3}")

print(f"{m1} == {m2} = {m1 == m2}")
print(f"{m1} != {m2} = {m1 != m2}")
print(f"{m1} < {m2} = {m1 < m2}")
print(f"{m1} > {m2} = {m1 > m2}")
print(f"{m1} <= {m2} = {m1 <= m2}")
print(f"{m1} >= {m2} = {m1 >= m2}")