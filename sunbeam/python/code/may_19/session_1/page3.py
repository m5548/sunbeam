# mathematical operators


class MyClass:
    def __init__(self, number):
        self.__number = number

    def __str__(self):
        return f"{self.__number}"

    def __add__(self, other):
        return MyClass(self.__number + other.__number)

    def __sub__(self, other):
        return MyClass(self.__number - other.__number)

    def __mul__(self, other):
        return MyClass(self.__number * other.__number)

    def __truediv__(self, other):
        return MyClass(self.__number / other.__number)

    def __floordiv__(self, other):
        return MyClass(self.__number // other.__number)

    def __mod__(self, other):
        return MyClass(self.__number % other.__number)

    def __pow__(self, power, modulo=None):
        return MyClass(self.__number ** power)


m1 = MyClass(10)
m2 = MyClass(20)

# __add__
print(f"{m1} + {m2} = {m1 + m2}")

# __sub__
print(f"{m1} - {m2} = {m1 - m2}")

# __truediv__
print(f"{m1} / {m2} = {m1 / m2}")

# __floordiv__
print(f"{m1} // {m2} = {m1 // m2}")

# __mul__
print(f"{m1} * {m2} = {m1 * m2}")

# __mod__
print(f"{m1} % {m2} = {m1 % m2}")

# __pow__
print(f"{m1} ** {2} = {m1 ** 2}")

