class Person(object):
    def __init__(self, name):
        self._name = name

    def print_details(self):
        print(f"-- Person details --")
        print(f"name = {self._name}")


class Student(Person):
    def __init__(self, name, roll_no):
        Person.__init__(self, name)
        self._roll_no = roll_no

    def print_details(self):
        # call the Parent class method
        Person.print_details(self)

        print(f"-- Student details --")
        # print(f"name = {self._name}")
        print(f"roll no = {self._roll_no}")

    def __str__(self):
        return f"Student [name={self._name}, roll no={self._roll_no}]"


s1 = Student('student 1', 1)

# will be called from Student class
# s1.print_details()

# s1.__str__()
print(f"s1 = {s1}")
print(f"s1 = {s1.__str__()}")

num = 100

# internal: int.__str__(num)
# internal: num.__str__()
print(f"num = {num}")
print(f"num = {num.__str__()}")
