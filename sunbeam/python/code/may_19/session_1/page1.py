num1, num2 = 10, 20

# int => num1 + num2 => num1.__add__(num2)
# print(f"{num1} + {num2} = {num1 + num2}")

print(f"num1 = {num1}")


# class MyClass(object):
class MyClass:
    def __init__(self, number):
        self.__number = number

    def print_details(self):
        print(f"number = {self.__number}")

    def __add__(self, other):
        return MyClass(self.__number + other.__number)


m1 = MyClass(10)
m2 = MyClass(20)

m3 = m1 + m2
print(f"m3 = {m3}")
m3.print_details()

print(f"base class of MyClass = {MyClass.__base__}")

# get all the contents of an object
print(m1.__dict__)

# get all the contents of a class
print(MyClass.__dict__)
