# string
# - immutable
# - collection of characters


def function1():
    # num = 100

    str1 = 'tHis Is My TeSt strinG'

    # sentence: first letter of sentence is always capital
    print(f"capital = {str1.capitalize()}")

    print(f"uppercase = {str1.upper()}")
    print(f"lowercase = {str1.lower()}")


# function1()


def function2():
    str1 = 'tHis Is My TeSt strinG'

    print(f"str1[0:4] = {str1[0:4]}")
    print(f"str1[::-1] = {str1[::-1]}")

    # list of characters
    list_characters = list(str1)
    print(list_characters)

    print(f"characters [0:4] = {list_characters[0:4]}, {''.join(list_characters[0:4])}")
    print(f"reversing a string = {list_characters[::-1]}, {''.join(list_characters[::-1])}")

    # for ch in list_characters:
    #     print(ch)

    # reverse the list
    list_characters.reverse()
    print(list_characters)

    # join all the characters to create a string
    print(f"join with space = {' '.join(list_characters)}")
    print(f"join with dash = {'-'.join(list_characters)}")
    print(f"join with empty string = {''.join(list_characters)}")


# function2()


def function3():
    numbers = ['10', '20', '30', '40', '50']

    print('-*-'.join(numbers))


# function3()


def function4():
    persons = [
        {"id": 1, "name": "person1", "address": "pune", "age": 30, "phone": "+9134435"},
        {"id": 2, "name": "person2", "address": "mumbai", "age": 32, "phone": "+9134434"},
        {"id": 3, "name": "person3", "address": "satara", "age": 34, "phone": "+9134433"},
        {"id": 4, "name": "person4", "address": "nashik", "age": 35, "phone": "+9134432"},
        {"id": 5, "name": "person5", "address": "karad", "age": 37, "phone": "+91344351"},
    ]

    id = "id"
    name = "name"
    address = "address"
    age = "age"
    phone = "phone"

    print(f"-" * 58)
    print(f"| {id:^6} | {name:^10} | {address:^10} | {age:^6} | {phone:^10} |")
    print(f"-" * 58)

    for person in persons:
        print(f"| {person['id']:^6} | {person['name']:<10} | {person['address']:<10} | {person['age']:^6} | {person['phone']:>10} |")

    print(f"-" * 58)


# function4()


def function5():
    num1 = 10000000
    num2 = -10
    num3 = 10
    pi = 3.14842252

    print(f"positive = {num1:+}")
    print(f"negative = {num2:-}")

    print()

    print(f"comma separated number = {num1:,}")
    print(f"underscore separated number = {num1:_}")

    print()

    print(f"decimal number = {num3:n}")
    print(f"decimal number = {num3:d}")
    print(f"hexa decimal number = {num3:x}")
    print(f"hexa decimal number = {num3:X}")
    print(f"octal number = {num3:o}")
    print(f"binary number = {num3:b}")

    # for number in range(10):
    #     print(f"| {number:d} | {number:x} | {number:b} | {number:o} |")

    print()

    print(f"pi = {pi}")
    print(f"pi = {pi:0.3f}")
    print(f"pi = {pi:e}")
    print(f"pi = {pi:E}")


function5()