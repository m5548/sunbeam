package org.sunbeam.dbda.generics.test;

import java.util.Arrays;

public class Program {
	public static void main(String[] args) {
		int[] arr = new int[] { 15, 11, 14, 12, 13 };
		
		Arrays.sort( arr );	//Dual-Pivot Quicksort
		
		System.out.println(Arrays.toString(arr));	//[11, 12, 13, 14, 15]
	}
}
