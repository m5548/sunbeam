package org.sunbeam.dbda.generics.test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class Employee implements Comparable<Employee>{
	private String name;
	private int empid;
	private float salary;
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	public String getName() {
		return name;
	}
	public int getEmpid() {
		return empid;
	}
	public float getSalary() {
		return salary;
	}
	@Override
	public String toString() {
		return String.format("%-15s%-5d%-10.2f", this.name, this.empid, this.salary);
	}
	@Override
	public int compareTo(Employee other) {
		return this.empid - other.empid;
	}
}
class CompareByName implements Comparator<Employee>{
	@Override
	public int compare(Employee emp1, Employee emp2) {
		return emp1.getName().compareTo(emp2.getName());
	}
}
class CompareByEmpid implements Comparator<Employee>{
	@Override
	public int compare(Employee emp1, Employee emp2) {
		return emp1.getEmpid() - emp2.getEmpid();
	}
}
class CompareBySalary implements Comparator<Employee>{
	@Override
	public int compare(Employee emp1, Employee emp2) {
		return (int) (emp1.getSalary() - emp2.getSalary());
	}
}
public class Program {
	private static Scanner sc = new Scanner(System.in); 
	public static Employee[] getEmployees( ) {
		Employee[] arr = new Employee[ 5 ];
		arr[ 0 ] = new Employee("Umesh", 15, 50000.50f);
		arr[ 1 ] = new Employee("Sachin", 14, 45000.50f);
		arr[ 2 ] = new Employee("Girish", 11, 40000.50f);
		arr[ 3 ] = new Employee("Yogesh", 12, 35000.50f);
		arr[ 4 ] = new Employee("Amit", 13, 30000.50f);
		return arr;
	}
	private static void printRecord(Employee[] arr) {
		if( arr != null ) {
			for( Employee emp : arr )
				System.out.println(emp.toString());
			System.out.println();
		}
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Compare By Name");
		System.out.println("2.Compare By Empid");
		System.out.println("3.Compare By Salary");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		Employee[] arr = Program.getEmployees();
		while( ( choice = Program.menuList( ) ) != 0 ) {
			Comparator<Employee> comparator = null;
			switch( choice ) {
			case 1:
				comparator = new CompareByName();
				break;
			case 2:
				comparator = new CompareByEmpid();
				break;
			case 3:
				comparator = new CompareBySalary();
				break;
			}
			Arrays.sort(arr, comparator);
			Program.printRecord(arr);
		}
	}
}
