package org.sunbeam.dbda.generics.test;

import java.util.Iterator;

class ArrayList implements Iterable<Integer>{
	int index = -1;
	private int[] arr;
	public ArrayList(  ) {
		this( 5 );
	}
	public ArrayList( int capacity ) {
		this.arr = new int[ capacity ];
	}
	public void add( int element ) {
		this.arr[ ++ this.index ] = element;
	}
	@Override
	public Iterator<Integer> iterator() {
		Iterator<Integer> itr = new ArrayListIterator( this.arr );	//Upcasting
		return itr;
	}
}
class ArrayListIterator implements Iterator<Integer>{
	private int[] arr;
	public ArrayListIterator(int[] arr) {
		this.arr = arr;
	}
	int index = 0;
	@Override
	public boolean hasNext() {
		return index < this.arr.length;
	}

	@Override
	public Integer next() {
		int element = this.arr[ index ];
		++ index;
		return element;
	}
}
public class Program {
	public static void main(String[] args) {
		ArrayList list = new ArrayList( );
		list.add( 10 );
		list.add( 20 );
		list.add( 30 );
		
		for( int element : list )
			System.out.println(element);
	}
	public static void main1(String[] args) {
		ArrayList list = new ArrayList( );
		list.add( 10 );
		list.add( 20 );
		list.add( 30 );
		
		Integer element = null;
		Iterator<Integer> itr = list.iterator();
		while( itr.hasNext()) {
			element = itr.next();
			System.out.println(element);
		}
	}
}
