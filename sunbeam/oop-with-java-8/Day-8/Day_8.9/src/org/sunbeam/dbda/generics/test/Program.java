package org.sunbeam.dbda.generics.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Program{
	/* public static void print( Object obj ) {
		System.out.println(obj);
	} */
	
	/* public static <T> void print( T obj ) {
		System.out.println(obj);
	} */
	
	public static <T extends Number > void print( T obj ) {
		System.out.println(obj);
	}
	public static void main(String[] args) {
		//Program.print(true);
		
		//Program.print('A');
		
		//Program.print("SunBeam");
		
		Program.print(125);
		
		Program.print(3.142);
		
		//Program.print( new Date());
		
		
		
	}
}
