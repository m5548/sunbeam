package org.sunbeam.dbda.generics.test;

import java.time.LocalDate;

class Date{
	private int day;
	private int month;
	private int year;
	public Date( ) {
		LocalDate ldt = LocalDate.now();
		this.day = ldt.getDayOfMonth();
		this.month = ldt.getMonthValue();
		this.year = ldt.getYear();
	}
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public void setYear(int year) {
		this.year = year;
	}
	@Override
	public String toString() {
		return this.day+" / "+this.month+" / "+this.year;
	}
}
public class Program{
	public static void main(String[] args) {
		Date dt1 = new Date( 27, 4, 2021);
		Date dt2 = dt1;
		
		dt1.setDay(13);
		dt2.setMonth(7);
		
		System.out.println(dt1.toString());
		System.out.println(dt2.toString());
		
	}
}
