package org.sunbeam.dbda.generics.test;

import java.util.Arrays;
import java.util.Scanner;

interface Collection{
	void acceptRecord( );
	void printRecord( );
	int[] toArray( );
}
class Array implements Collection{
	private int[] arr;
	public Array() {
		this( 5 );
	}
	public Array( int size ) {
		this.arr = new int[ size ];
	}
	@Override
	public void acceptRecord() {
		try( Scanner sc = new Scanner(System.in)){
			for( int index = 0; index < this.arr.length; ++ index ) {
				System.out.print("Enter element	:	");
				this.arr[ index ] = sc.nextInt();
			}
		}
	}

	@Override
	public void printRecord() {
		for( int index = 0; index < this.arr.length; ++ index ) {
			System.out.print(this.arr[ index ] +"	");
		}
		System.out.println();
	}
	@Override
	public int[] toArray() {
		return this.arr;
	}
	
	/* @Override
	public int[] toArray() {
		return Arrays.copyOf(this.arr, this.arr.length);	//Defensive Copy
	} */
	
}
public class Program {
	public static void main(String[] args) {
		Collection collection = null;
		
		collection = new Array( );
		
		collection.acceptRecord();
		
		collection.printRecord();
	}
}