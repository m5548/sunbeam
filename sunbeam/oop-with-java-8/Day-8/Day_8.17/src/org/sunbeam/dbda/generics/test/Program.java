package org.sunbeam.dbda.generics.test;

import java.util.Arrays;

class Employee implements Comparable<Employee>{
	private String name;
	private int empid;
	private float salary;
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return String.format("%-15s%-5d%-10.2f", this.name, this.empid, this.salary);
	}
	/* @Override
	public int compareTo(Employee other) {
		if( this.empid < other.empid )
			return -1;
		if( this.empid > other.empid )
			return 1;
		return 0;
	} */
	
	@Override
	public int compareTo(Employee other) {
		return this.name.compareTo(other.name);
	}
	
	/* @Override
	public int compareTo(Employee other) {
		return this.empid - other.empid;
	} */
	/* public int compareTo(Employee other) {
		return (int) (this.salary - other.salary);
	} */
}
public class Program {
	public static Employee[] getEmployees( ) {
		Employee[] arr = new Employee[ 5 ];
		arr[ 0 ] = new Employee("Umesh", 15, 50000.50f);
		arr[ 1 ] = new Employee("Sachin", 14, 45000.50f);
		arr[ 2 ] = new Employee("Girish", 11, 40000.50f);
		arr[ 3 ] = new Employee("Yogesh", 12, 35000.50f);
		arr[ 4 ] = new Employee("Amit", 13, 30000.50f);
		return arr;
	}
	private static void printRecord(Employee[] arr) {
		if( arr != null ) {
			for( Employee emp : arr )
				System.out.println(emp.toString());
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Employee[] arr = Program.getEmployees();
		Program.printRecord( arr );
		
		Arrays.sort(arr);	//mergesort
		Program.printRecord( arr );
	}
}
