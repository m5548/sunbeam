package org.sunbeam.dbda.generics.test;

import java.util.Date;

//Parameterized Type(class)

class Box<T>{	//T -> Type Parameter
	private T object;
	public T getObject() {
		return object;
	}
	public void setObject(T object) {
		this.object = object;
	}
}

public class Program {
	public static void main(String[] args) {
		//Box<int> b = new Box<>( );	//int is not allowed
		
		//int 		-> primitive
		//Integer	-> non primitive
		Box<Integer> b = new Box<>( );	//OK
		b.setObject( 10 );	//b.setObject( Integer.valueOf(10) );
		int number = b.getObject();	//int number = b.getObject().intValue();
		System.out.println("Number	:	"+number);
	}
	public static void main4(String[] args) {
		Box b = new Box();	//class Box -> raw type
		//Box<Object> b = new Box<Object>();
		b.setObject( new Date());
		String str =  (String) b.getObject(); //ClassCastException
		System.out.println(str);
	}
	public static void main3(String[] args) {
		Box<Date> b1 = new Box<Date>();	//OK
		
		Box<Date> b2 = new Box<>();	//OK
		
		//Box<> b3 = new Box<Date>();	//NOT OK
		
		//Box<> b4 = new Box<>();	//NOT OK
		
		Box<Object> b5 = new Box<Object>();	//OK
		
		//Box<Object> b6 = new Box<Date>();	//NOT OK
		
		//Box<Date> b7 = new Box<Object>();	//NOT OK
	}
	public static void main2(String[] args) {
		//Box<Date> box = new Box<Date>();	//Date -> Type Argument
		
		Box<Date> box = new Box< >();	//Type Inference
		//"< >" operator is called diamond operator. 
		
		box.setObject( new Date());
		
		Date date =  box.getObject();
		
		System.out.println(date.toString());
	}
	public static void main1(String[] args) {
		Box<Date> box = new Box<Date>();	//Date -> Type Argument
		
		box.setObject( new Date());
		
		Date date =  box.getObject();
		
		System.out.println(date.toString());
		
		//String str = (String) box.getObject(); //NOT OK
	}
}
