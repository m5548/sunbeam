package org.sunbeam.dbda.generics.test;

import java.time.LocalDate;

class Date implements Cloneable{
	private int day;
	private int month;
	private int year;
	public Date( ) {
		LocalDate ldt = LocalDate.now();
		this.day = ldt.getDayOfMonth();
		this.month = ldt.getMonthValue();
		this.year = ldt.getYear();
	}
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public void setYear(int year) {
		this.year = year;
	}
	/*
	//Date this = dt1
	@Override
	public Date clone( ) {
		Date other = new Date( this.day, this.month, this.year);
		return other;
	}*/
	
	@Override
	public Date clone( ) {
		try {
			Date other = (Date) super.clone();
			return other;
		} catch (CloneNotSupportedException cause) {
			throw new InternalError(cause);
		}
	}
	
	@Override
	public String toString() {
		return this.day+" / "+this.month+" / "+this.year;
	}
}
public class Program extends Object{
	public static void main(String[] args) {
		Date dt1 = new Date( 27, 4, 2021);
		Date dt2 = dt1.clone( );
		
		dt1.setDay(10);
		dt2.setDay(20);
		
		System.out.println(dt1.toString());
		System.out.println(dt2.toString());
		
	}
}
