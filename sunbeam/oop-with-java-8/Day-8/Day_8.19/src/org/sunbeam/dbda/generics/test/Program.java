package org.sunbeam.dbda.generics.test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

 abstract class Person implements Comparable<Person>{
	private String name;
	public Person(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	@Override
	public String toString() {
		return this.name;
	}
	@Override
	public int compareTo(Person other) {
		return this.name.compareTo(other.name);
	}
}
class Student extends Person{
	private int rollNumber;
	public Student(String name, int rollNumber) {
		super(name);
		this.rollNumber = rollNumber;
	}
	public int getRollNumber() {
		return rollNumber;
	}
	@Override
	public String toString() {
		return String.format("%-15s%-5d", super.toString(), this.rollNumber);
	}
}
class Employee extends Person{
	private int empid;
	public Employee(String name, int empid) {
		super(name);
		this.empid = empid;
	}
	public int getEmpid() {
		return empid;
	}
	@Override
	public String toString() {
		return String.format("%-15s%-5d", super.toString(), this.empid);
	}
}
class CompareByName implements Comparator<Person>{
	@Override
	public int compare(Person p1, Person p2) {
		return p1.getName().compareTo(p2.getName());
	}
}
class CompareById implements Comparator<Person>{
	@Override
	public int compare(Person p1, Person p2) {
		if( p1 instanceof Student && p2 instanceof Student ) {
			Student s1 = (Student) p1;
			Student s2 = (Student) p2;
			return s1.getRollNumber() - s2.getRollNumber();
		}else if( p1 instanceof Employee && p2 instanceof Employee ) {
			Employee emp1 = (Employee) p1;
			Employee emp2 = (Employee) p2;
			return emp1.getEmpid() - emp2.getEmpid();
		}else if( p1 instanceof Student && p2 instanceof Employee ) {
			Student s1 = (Student) p1;
			Employee emp2 = (Employee) p2;
			return s1.getRollNumber() - emp2.getEmpid();
		}else {
			Employee emp1 = (Employee) p1;
			Student s2 = (Student) p2;
			return emp1.getEmpid() - s2.getRollNumber();
		}
	}
}
public class Program {
	static Scanner sc = new Scanner(System.in);
	public static Person[] getPersons( ) {
		Person[] arr = new Person[ 5 ];
		arr[ 0 ] = new Student("Digvijay", 15);
		arr[ 1 ] = new Employee("Rahul", 11);
		arr[ 2 ] = new Student("Yogesh", 14);
		arr[ 3 ] = new Employee("Amit", 12);
		arr[ 4 ] = new Student("Girish", 13);
		return arr;
	}
	private static void printRecord(Person[] arr) {
		if( arr != null ) {
			for (Person person : arr) {
				System.out.println(person.toString());
			}
			System.out.println();
		}
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Compare By Name");
		System.out.println("2.Compare By Id");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		Person[] arr = Program.getPersons();
		while( ( choice = Program.menuList( ) ) != 0 ) {
			 Comparator<Person> comparator = null;
			switch( choice ) {
			case 1:
				comparator = new CompareByName();
				break;
			case 2:
				comparator = new CompareById();
				break;
			}
			Arrays.sort(arr, comparator);
			Program.printRecord(arr);
		}
	}
}
