package org.sunbeam.dbda.generics.test;

import java.util.Date;

class Box<T extends Number>{	//T is bounded type parameter	
	private T object;
	public T getObject() {
		return object;
	}
	public void setObject(T object) {
		this.object = object;
	}
}

public class Program {
	public static void main(String[] args) {
		Box<Number> b1 = new Box<>( );
		
		//Box<Boolean> b2 = new Box<>( );		//Not OK
		
		//Box<Character> b3 = new Box<>( );	//Not OK
		
		Box<Integer> b4 = new Box<>( );
		
		Box<Double> b5 = new Box<>( );
		
		//Box<String> b6 = new Box<>( );	//Not OK
		
		//Box<Date> b7 = new Box<>( );	//Not OK
	}
}
