package org.sunbeam.dbda.generics.test;

import java.util.ArrayList;

public class Program {
	public static ArrayList<Integer> getIntegerList( ){
		ArrayList<Integer> list = new ArrayList<>();
		list.add( 10 );
		list.add( 20 );
		list.add( 30 );
		return list;
	}
	public static ArrayList<Double> getDoubleList( ){
		ArrayList<Double> list = new ArrayList<>();
		list.add( 10.1 );
		list.add( 20.2 );
		list.add( 30.3 );
		return list;
	}
	public static ArrayList<String> getStringList( ){
		ArrayList<String> list = new ArrayList<>();
		list.add( "Python" );
		list.add( "Java" );
		list.add( "Hadoop" );
		return list;
	}
	public static void printIntegerList( ArrayList<Integer> list) {
		for( Integer element : list )
			System.out.println(element);
	}
	public static void printDoubleList( ArrayList<Double> list) {
		for( Double element : list )
			System.out.println(element);
	}
	public static void printStringList( ArrayList<String> list) {
		for( String element : list )
			System.out.println(element);
	}
	public static void main(String[] args) {
		ArrayList<Integer> integerList = Program.getIntegerList();
		
		Program.printIntegerList(integerList);
	}
	public static void main2(String[] args) {
		ArrayList<Double> doubleList = Program.getDoubleList();
		
		Program.printDoubleList(doubleList);
	}
	public static void main3(String[] args) {
		ArrayList<String> stringList = Program.getStringList();
		
		Program.printStringList(stringList);
	}
}
