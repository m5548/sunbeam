package org.sunbeam.dbda.generics.test;

import java.util.ArrayList;

public class Program extends Object{
	public static void main(String[] args) {
		ArrayList<Integer> list1 = new ArrayList<Integer>();
		list1.add(10);
		list1.add(20);
		list1.add(30);
		
		ArrayList<Integer> list2 = (ArrayList<Integer>) list1.clone();
		
		list1.clear();
		if( !list1.isEmpty()) {
			for( Integer element : list1 )
				System.out.print( element+"	");
		}else
			System.out.println("List1 is empty");
		
		System.out.println();
		for( Integer element : list2 )
			System.out.print( element+"	");
	}
}
