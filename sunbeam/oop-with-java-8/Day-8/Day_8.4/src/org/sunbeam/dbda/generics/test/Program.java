package org.sunbeam.dbda.generics.test;

import java.util.ArrayList;

public class Program {
	public static void main1(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add( 10 );
		list.add( 20 );
		list.add( 30 );
		
		for( Integer element : list )
			System.out.println(element);
	}
	public static void main2(String[] args) {
		ArrayList<Double> list = new ArrayList<>();
		list.add( 10.1 );
		list.add( 20.2 );
		list.add( 30.3 );
		
		for( Double element : list )
			System.out.println(element);
	}
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add( "Python" );
		list.add( "Java" );
		list.add( "Hadoop" );
		
		for( String element : list )
			System.out.println(element);
	}
}
