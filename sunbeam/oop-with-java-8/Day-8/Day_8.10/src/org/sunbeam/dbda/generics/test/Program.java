package org.sunbeam.dbda.generics.test;

interface Printable{	//ISI
	int number = 10;
	//public static final int number = 10;
	
	//public Printable( ) {	} //Interfaces cannot have constructors
	
	void print( );
	//public abstract void print( );
}

//Interface implementation inheritance
class Test implements Printable{	//Syska
	@Override
	public void print() {
		System.out.println("Number	:	"+Printable.number);
	}
}
public class Program{	//Customer
	public static void main(String[] args) {
		Printable p = null;	//OK
		//p = new Printable( );	//Cannot instantiate the type Printable
		p = new Test( );	//Upcasting
		p.print();//DMD
	}
	public static void main1(String[] args) {
		Test t = new Test( );
		t.print();
	}
}
