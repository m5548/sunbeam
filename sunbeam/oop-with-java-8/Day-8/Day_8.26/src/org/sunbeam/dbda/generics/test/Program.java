package org.sunbeam.dbda.generics.test;

@FunctionalInterface
interface A{	//Functional Interface / SAM Interface
	void f1( );	//Functional Method / Method Descriptor
}

@FunctionalInterface
interface B{	//OK
	int number = 10;
	void f1( );	//abstract
	default void f2( ) {	}
	default void f3( ) {	}
	static void f4( ) {	}
	static void f5( ) {	}
}
@FunctionalInterface
interface C{	///Error : Invalid '@FunctionalInterface' annotation; C is not a functional interface
	void f1( );
	void f2( );
}
public class Program {
	public static void main(String[] args) {
		
	}
}