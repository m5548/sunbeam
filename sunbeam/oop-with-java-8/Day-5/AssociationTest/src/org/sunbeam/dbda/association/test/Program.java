package org.sunbeam.dbda.association.test;

import java.util.Scanner;

import org.sunbeam.dbda.association.lib.Address;
import org.sunbeam.dbda.association.lib.Date;
import org.sunbeam.dbda.association.lib.Person;

public class Program {
	private static Scanner sc = new Scanner( System.in);
	private static void acceptRecord(Date birthDate) {
		System.out.println("Enter birthdate	:	");
		System.out.print("Day	:	");
		birthDate.setDay(sc.nextInt());
		System.out.print("Month	:	");
		birthDate.setMonth(sc.nextInt());
		System.out.print("Year	:	");
		birthDate.setYear(sc.nextInt());
	}
	private static void acceptRecord(Address currentAddress) {
		sc.nextLine();
		System.out.println("Enter current Address	:	");
		System.out.print("City Name	:	");
		currentAddress.setCityName(sc.nextLine());
		System.out.print("State Name	:	");
		currentAddress.setStateName(sc.nextLine());
		System.out.print("Pincode	:	");
		currentAddress.setPinCode(sc.nextInt());
	}
	private static void acceptRecord(Person person) {
		sc.nextLine();
		System.out.print("Name	:	");
		person.setName(sc.nextLine());

		Date birthDate = person.getBirthDate();
		Program.acceptRecord(birthDate);
		
		
		Address currentAddress = person.getCurrentAddress();
		Program.acceptRecord(currentAddress);
	}
	private static void printRecord(Date birthDate) {
		System.out.println("Birth date	:	"+birthDate.getDay()+" / "+birthDate.getMonth()+" / "+birthDate.getYear());
	}
	private static void printRecord(Address currentAddress) {
		System.out.println("Current address	:	"+currentAddress.getCityName()+" "+currentAddress.getStateName()+" "+currentAddress.getPinCode());
	}
	private static void printRecord(Person person) {
		System.out.println("Name		:	"+person.getName());
		
		Date birthDate = person.getBirthDate();
		Program.printRecord(birthDate);
		
		Address currentAddress = person.getCurrentAddress();
		Program.printRecord(currentAddress);
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Date");
		System.out.println("2.Address");
		System.out.println("3.Person");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		while( ( choice = Program.menuList( ) ) != 0 ) {
			switch( choice ) {
			case 1:
				Date birthDate = new Date( );
				Program.acceptRecord(birthDate);
				Program.printRecord(birthDate);
				break;
			case 2:
				Address currentAddress = new Address( );
				Program.acceptRecord(currentAddress);
				Program.printRecord(currentAddress);
				break;
			case 3:
				Person person = new Person();
				Program.acceptRecord(person);
				Program.printRecord(person);
				break;
			}
		}
	}
	public static void main3(String[] args) {
		Person person = new Person();
		Program.acceptRecord( person );
		Program.printRecord( person );
	}
	public static void main2(String[] args) {
		Address currentAddress = new Address();
		Program.acceptRecord( currentAddress );
		Program.printRecord( currentAddress );
	}
	public static void main1(String[] args) {
		Date birthDate = new Date();
		Program.acceptRecord( birthDate );
		Program.printRecord( birthDate );
	}
	
}
