package org.sunbeam.dbda.test;
//Parent class -> Super Class
class Person{
	String name;
	int age;
	public Person( ) {
		this.name = null;
		this.age = 0;
	}
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	public void showRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
	public static void f1( ) {
		System.out.println("Person.f1");
	}
}
//Child Class -> Sub Class
class Employee extends Person{	//Implementation Inheritance
	int empid;			
	float salary;		
	public Employee( ) {
	}
	public Employee(String name, int age, int empid, float salary) {
		this.empid = empid;
		this.salary = salary;
	}
	/*
	 public void showRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	 }
	*/
	public void displayRecord( /*Employee this = emp */) {
		this.showRecord();
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		//Person.f1();
		Employee.f1();
	}
	public static void main3(String[] args) {
		Employee emp = new Employee();
		emp.displayRecord();	//emp.displayRecord();
	}
	public static void main2(String[] args) {
		Employee emp = new Employee();
		emp.showRecord();
		emp.displayRecord();
	}
	public static void main1(String[] args) {
		Person p = new Person();
		p.showRecord();
		//p.displayRecord();	//Not OK
	}
}
