package org.sunbeam.dbda.test;
//Super class
class Person{	//Top level class
	public class Date{	//Nested class
		public void print( ) {
			System.out.println("Date.print");
		}
	}
}
//Sub class
class Employee extends Person{	//Implementation Inheritance
	
}
public class Program {
	public static void main(String[] args) {
		Person.Date dt1 = new Person().new Date();
		dt1.print();
		
		Employee.Date dt2 = new Employee().new Date();
		dt2.print();
	}
}
