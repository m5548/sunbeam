package org.sunbeam.dbda.test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(int[][] arr) {
		for( int row = 0; row < arr.length; ++ row ) {
			for( int col = 0; col < arr[ row ].length; ++ col ) {
				System.out.print("Enter element	:	");
				arr[ row ][ col ] = sc.nextInt( );
			}
		}
	}
	private static void printRecord(int[][] arr) {
		for( int row = 0; row < arr.length; ++ row ) {
			for( int col = 0; col < arr[ row ].length; ++ col ) {
				System.out.print(arr[ row ][ col ]+"	");
			}
			System.out.println();
		}
	}
	private static void showRecord(int[][] arr) {
		//System.out.println(arr.toString());[[I@3d4eac69
		//System.out.println(Arrays.toString(arr)); //[[I@3d4eac69, [I@42a57993, [I@75b84c92, [I@6bc7c054]
		for( int row = 0; row < arr.length; ++ row )
			System.out.println(Arrays.toString(arr[ row ]));
	}
	private static void displayRecord(int[][] arr) {
		System.out.println(Arrays.deepToString(arr));
	}
	public static void main(String[] args) {
		int[][] arr = new int[ 4 ][ 3 ];
		//Program.acceptRecord( arr );
		
		//Program.printRecord( arr );
		
		//Program.showRecord(arr);
		
		Program.displayRecord(arr);
		
	}
	public static void main4(String[] args) {
		int[][] arr = new int[ 4 ][ 3 ];
		for( int row = 0; row < arr.length; ++ row ) {
			for( int col = 0; col < arr[ row ].length; ++ col ) {
				System.out.print("Enter element	:	");
				arr[ row ][ col ] = sc.nextInt( );
			}
		}
		for( int row = 0; row < arr.length; ++ row ) {
			for( int col = 0; col < arr[ row ].length; ++ col ) {
				System.out.print(arr[ row ][ col ]+"	");
			}
			System.out.println();
		}
	}
	public static void main3(String[] args) {
		int[][] arr = new int[ 4 ][ 3 ];
	}
	public static void main2(String[] args) {
		int[][] arr = null;
		arr = new int[ 4 ][ 3 ];
	}
	public static void main1(String[] args) {
		int arr1[ ][ ] = null;	//OK
		
		int[ ] arr2[ ] = null;	//OK
		
		int[ ][ ] arr3 = null;	//OK : Recommended
	}
}
