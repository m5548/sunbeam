package org.sunbeam.dbda.test;
//Parent class -> Super Class
class Person{
	public static int xyz = 100;
	String name;
	int age;
	public Person( ) {
		this.name = null;
		this.age = 0;
	}
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	public void showRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
}
//Child Class -> Sub Class
class Employee extends Person{	//Implementation Inheritance
	//String name;
	//int age;
	int empid;			
	float salary;		
	public Employee( ) {
	}
	public Employee(String name, int age, int empid, float salary) {
		this.empid = empid;
		this.salary = salary;
	}
	public void displayRecord( ) {
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		System.out.println(Person.xyz);
		System.out.println(Employee.xyz);
	}
	public static void main2(String[] args) {
		Employee emp  = new Employee();
		emp.name = "Sandeep";	//OK
		emp.age = 38;	//OK
		emp.empid = 33;	//OK
		emp.salary = 45000.50f;	//OK
		
		System.out.println(emp.name);
		System.out.println(emp.age);
		System.out.println(emp.empid);
		System.out.println(emp.salary);
		
	}
	public static void main1(String[] args) {
		Person p = new Person();
		p.name = "Sandeep";	//OK
		p.age = 38;	//OK
		//p.empid = 33;	//Not OK
		//p.salary = 45000.50f;	//Not OK
		
		
		System.out.println(p.name);
		System.out.println(p.age);
		//System.out.println(p.empid);
		//System.out.println(p.salary);
	}
}
