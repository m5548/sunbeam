package org.sunbeam.dbda.test;
//Parent class -> Super Class
class Person{
	String name;
	int age;
	public Person( ) {
		System.out.println("public Person( )");
		this.name = null;
		this.age = 0;
	}
	public Person(String name, int age) {
		System.out.println("public Person(String name, int age) ");
		this.name = name;
		this.age = age;
	}
	public void showRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
}
//Child Class -> Sub Class
class Employee extends Person{	//Implementation Inheritance
	int empid;			
	float salary;		
	public Employee( ) {
		super( );
		System.out.println("public Employee( )");
	}
	public Employee(String name, int age, int empid, float salary) {
		super( name, age );	//Constructor call must be the first statement in a constructor
		System.out.println("public Employee(String name, int age, int empid, float salary)");
		this.empid = empid;
		this.salary = salary;
	}
	public void displayRecord( /*Employee this = emp */) {
		this.showRecord();
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		Employee emp = new Employee("Sandeep", 38, 33, 45000.50f);
		emp.displayRecord();
	}
	public static void main3(String[] args) {
		Employee emp = new Employee();
		emp.displayRecord();
	}
	public static void main2(String[] args) {
		Person p1 = new Person("Sandeep", 38);	//public Person(String name, int age) 
		p1.showRecord();
	}
	public static void main1(String[] args) {
		Person p1 = new Person();	//public Person( )
		p1.showRecord();
	}
}
