package org.sunbeam.dbda.test;

class Employee{
	String name;		//null
	int age;			//0
	int empid;			//0
	float salary;		//0.0f
	public Employee( ) {
	}
	public Employee(String name, int age, int empid, float salary) {
		this.name = name;
		this.age = age;
		this.empid = empid;
		this.salary = salary;
	}
	public void displayRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		Employee emp1 = new  Employee();
		//emp1.displayRecord();
		
		Employee emp2 = new  Employee("Sandeep", 38, 33, 45000.50f );
		emp2.displayRecord();
	}
}
