package org.sunbeam.dbda.test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(int[][] arr) {
		for( int row = 0; row < arr.length; ++ row ) {
			for( int col = 0; col < arr[ row ].length; ++ col ) {
				System.out.print("Enter element	:	");
				arr[ row ][ col ] = sc.nextInt( );
			}
		}
	}
	private static void printRecord(int[][] arr) {
		for( int[] row : arr ) {
			for( int col : row )
				System.out.print(col+"	");
			System.out.println();
		}
	}
	public static void main(String[] args) {
		int[][] arr = new int[ 4 ][ 3 ];
		
		Program.acceptRecord( arr );
		
		Program.printRecord( arr );
		
	}
}
