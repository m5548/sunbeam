package org.sunbeam.dbda.test;
enum Day{
	MON( 1 ), TUES( "TuesDay" ), WED(3, "WednesDay");
	private int dayNumber;
	private String dayName;
	private Day(int dayNumber) {
		this.dayNumber = dayNumber;
	}
	private Day(String dayName) {
		this.dayName = dayName;
	}
	private Day(int dayNumber, String dayName) {
		this.dayNumber = dayNumber;
		this.dayName = dayName;
	}
	public int getDayNumber() {
		return dayNumber;
	}
	public String getDayName() {
		return dayName;
	}
}
public class Program {
	public static void main(String[] args) {
		Day[] days = Day.values();
		for (Day day : days) {
			System.out.printf("%-10s%-3d%-10s%-3d\n", day.name(), day.ordinal(), day.getDayName(), day.getDayNumber());
		}
	}
	public static void main3(String[] args) {
		Day day = Day.WED;
		System.out.println(day.name());		//WED
		System.out.println(day.ordinal());	//2
		System.out.println(day.getDayName());
		System.out.println(day.getDayNumber());
	}
	public static void main2(String[] args) {
		Day day = Day.TUES;
		System.out.println(day.name());		//TUES
		System.out.println(day.ordinal());	//1
		System.out.println(day.getDayName());
	}
	public static void main1(String[] args) {
		Day day = Day.MON;
		System.out.println(day.name());		//MON
		System.out.println(day.ordinal());	//0
		System.out.println(day.getDayNumber());
	}
}