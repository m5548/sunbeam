package org.sunbeam.dbda.test;

import java.util.Scanner;

enum Color{
	RED, GREEN, BLUE				//RED, GREEN, BLUE : Enum constants : name()
	//RED = 0, GREEN = 1, BLUE = 2	//0,1,2 : Ordinals : ordinal( )
}
public class Program {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.print("Enum constant name(RED/GREEN/BLUE)	:	");
		String name = sc.nextLine();
		Color color = Color.valueOf(name);
		System.out.println(color.name()+"	"+color.ordinal());
	}
	public static void main4(String[] args) {
		Color[] colors = Color.values();
		for (Color color : colors) {
			System.out.println(color.name()+"	"+color.ordinal());	
		}
	}
	public static void main3(String[] args) {
		Color color = Color.RED;
		System.out.println(color.name()+"	"+color.ordinal());
		
		color = Color.GREEN;
		System.out.println(color.name()+"	"+color.ordinal());
		
		color = Color.BLUE;
		System.out.println(color.name()+"	"+color.ordinal());
	}
	public static void main2(String[] args) {
		Color color = Color.RED;
		System.out.println(color.name()+"	"+color.ordinal());
	}
	public static void main1(String[] args) {
		System.out.println(Color.RED.name()+"	"+Color.RED.ordinal());
	}
}