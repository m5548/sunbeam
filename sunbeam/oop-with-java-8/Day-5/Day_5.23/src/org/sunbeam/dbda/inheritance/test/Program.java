package org.sunbeam.dbda.inheritance.test;

import org.sunbeam.sandeep.inheritance.test.Person;

class Employee extends Person{
	private int empid;
	private float salary;
	public Employee( ) {		
	}
	public Employee( String name, int age, int empid, float salary ) {
		super( name, age );
		this.empid = empid;
		this.salary = salary;
	}
	public void printRecord( ) {
		super.printRecord();
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}
public class Program {	//SunBeam
	public static void main(String[] args) {
		Employee emp = new Employee("Sandeep",38,33,45000.50f);
		emp.printRecord();
	}
}
