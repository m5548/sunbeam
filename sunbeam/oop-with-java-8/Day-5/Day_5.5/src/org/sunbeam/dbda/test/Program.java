package org.sunbeam.dbda.test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(int[][] arr) {
		for( int row = 0; row < arr.length; ++ row ) {
			for( int col = 0; col < arr[ row ].length; ++ col ) {
				System.out.print("Enter element	:	");
				arr[ row ][ col ] = sc.nextInt( );
			}
		}
	}
	private static void printRecord(int[][] arr) {
		for( int row = 0; row < arr.length; ++ row ) {
			for( int col = 0; col < arr[ row ].length; ++ col ) {
				System.out.print(arr[ row ][ col ]+"	");
			}
			System.out.println();
		}
	}
	private static void showRecord(int[][] arr) {
		for( int[] row : arr ) {
			for( int col : row ) {
				System.out.print(col+"	");
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {
		int[][] arr = new int[ 4 ][ ];
		arr[ 0 ] = new int[ 3 ];
		arr[ 1 ] = new int[ 2 ];
		arr[ 2 ] = new int[ 4 ];
		arr[ 3 ] = new int[ 3 ];
		
		Program.acceptRecord(arr);
		
		//Program.printRecord(arr);
		
		Program.showRecord(arr);
	}
}
