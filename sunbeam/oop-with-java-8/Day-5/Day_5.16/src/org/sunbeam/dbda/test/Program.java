package org.sunbeam.dbda.test;

class Person{
	String name;
	int age;
	public Person( ) {
		this.name = null;
		this.age = 0;
	}
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	public void showRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
}
public class Program {
	public static void main(String[] args) {
		Person p1 = new Person( );
		//p1.showRecord();
		
		Person p2 = new Person( "Sandeep", 38 );
		p2.showRecord();
	}
}
