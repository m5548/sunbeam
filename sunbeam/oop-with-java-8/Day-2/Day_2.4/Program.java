import java.util.Scanner;
class Program{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner( System.in);

        class Employee{
            String name;     //Field    //null
            int empid;       //Field    //0
            float salary;    //Field    //0.0f

            void acceptRecord( ){
                System.out.print("Name  :   ");
                name = sc.nextLine();
                System.out.print("Empid  :   ");
                empid = sc.nextInt();
                System.out.print("Salary  :   ");
                salary = sc.nextFloat( );
            }

            void printRecord( ){
                System.out.println("Name    : "+name);
                System.out.println("Empid    : "+empid);
                System.out.println("Salary    : "+salary);
            }
        }

       Employee emp  = new Employee();  //OK

       emp.acceptRecord( );
       //acceptRecord() method is called on employee instance

        emp.printRecord( );
        //printRecord() method is called on employee instance

    }
}