import java.util.Scanner;
class Program{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner( System.in);

        class Employee{ //Local Class
            String name;     //Field    //null
            int empid;       //Field    //0
            float salary;    //Field    //0.0f

            void acceptRecord( ){
                System.out.print("Name  :   ");
                sc.nextLine( ); //To clear the buffer
                name = sc.nextLine();
                System.out.print("Empid  :   ");
                empid = sc.nextInt();
                System.out.print("Salary  :   ");
                salary = sc.nextFloat( );
            }

            void printRecord( ){
                System.out.println("Name    : "+name);
                System.out.println("Empid    : "+empid);
                System.out.println("Salary    : "+salary);
            }
        }
        //Instantiation
        Employee emp1  = new Employee();  //OK
        emp1.acceptRecord( );   //message passing

        Employee emp2  = new Employee();  //OK
        emp2.acceptRecord( );

        Employee emp3  = new Employee();  //OK
        emp3.acceptRecord( );

        emp1.printRecord( );
        emp2.printRecord( );
        emp3.printRecord( );
    }
}