import java.util.Scanner;
class Program{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner( System.in);

        class Employee{
            String name;     //Field    //null
            int empid;       //Field    //0
            float salary;    //Field    //0.0f
        }
        
        //new Employee( );    //Anonymous instance of class Employee

       //Employee emp;    //Object reference / reference
       //emp = new Employee( ); //Instance with reference

       Employee emp  = new Employee();  //OK
           
        System.out.print("Name  :   ");
        emp.name = sc.nextLine();
        System.out.print("Empid  :   ");
        emp.empid = sc.nextInt();
        System.out.print("Salary  :   ");
        emp.salary = sc.nextFloat( );

        System.out.println("Name    : "+emp.name);
        System.out.println("Empid    : "+emp.empid);
        System.out.println("Salary    : "+emp.salary);
    }
}