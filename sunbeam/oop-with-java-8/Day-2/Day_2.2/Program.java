import java.util.Scanner;
class Program{
    public static void main(String[] args) {
        String name;
        int empid;
        float salary;
        
        Scanner sc = new Scanner( System.in);
        System.out.print("Name  :   ");
        name = sc.nextLine();
        System.out.print("Empid  :   ");
        empid = sc.nextInt();
        System.out.print("Salary  :   ");
        salary = sc.nextFloat( );

        System.out.println("Name    : "+name);
        System.out.println("Empid    : "+empid);
        System.out.println("Salary    : "+salary);
    }
}