import java.util.Scanner;
class Complex{
    private int real;
    private int imag;
    public Complex( ){ 
        System.out.println("Inside parameterless constructor");
        this.real = 10;
        this.imag = 20;
    }
    public Complex( int real, int imag){ 
        System.out.println("Inside parameterized constructor");
        this.real = real;
        this.imag = imag;
    } 
    public void acceptRecord( ){
        Scanner sc = new Scanner( System.in);
        System.out.print("Real Number   :   ");
        this.real = sc.nextInt();
        System.out.print("Imag Number   :   ");
        this.imag = sc.nextInt();
    }
    public void printRecord(  ){
        System.out.println("Real Number :   "+this.real);
        System.out.println("Imag Number :   "+this.imag);
    }
}
class Program{
    public static void main(String[] args) {
        Complex c1 = new Complex( 10, 20 );    //OK

        Complex c2 = new Complex( );    //OK
 
    }
}