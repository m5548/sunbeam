import java.util.Scanner;
class Employee{
    private String name;        //Field : Class Scope
    private int empid;          //Field : Class Scope
    private float salary;       //Field : Class Scope
    public void acceptRecord(  ){  //Method    :   Class Scope
        Scanner sc = new Scanner( System.in);
        System.out.print("Name   :   ");
        this.name = sc.nextLine( );
        System.out.print("Empid   :   ");
        this.empid = sc.nextInt();
        System.out.print("Salary   :   ");
        this.salary = sc.nextFloat( );
    }
    public void printRecord(  ){  //Method    :   Class Scope
        System.out.println("Name    :   "+this.name);
        System.out.println("Empid    :   "+this.empid);
        System.out.println("Salary    :   "+this.salary);
    }
}
class Program{
    public static void main(String[] args) {
        Employee emp = new Employee();  //OK

        emp.acceptRecord( );

        emp.printRecord( );
    }
}