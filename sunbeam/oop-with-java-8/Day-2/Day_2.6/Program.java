import java.util.Scanner;
//problem Statement : Write a program to store and print information of employee.
//Step 1 : Define Employee class
class Employee{
    //Step 2 : Declare Fields inside class
    String name;
    int empid;
    float salary;
    //Step 5 : Define method inside class
    //Employee this = emp;
    void acceptRecord( /* Employee this */){    //this is implicit reference variable declared by compiler.
        Scanner sc = new Scanner( System.in);
        System.out.print("Name   :   ");
        this.name = sc.nextLine( );
        System.out.print("Empid   :   ");
        this.empid = sc.nextInt();
        System.out.print("Salary   :   ");
        this.salary = sc.nextFloat( );
    }
    //Employee this = emp;
    void printRecord( /* Employee this */ ){
        System.out.println("Name    :   "+this.name);
        System.out.println("Empid    :   "+this.empid);
        System.out.println("Salary    :   "+this.salary);
    }
}

class Program{
    public static void main(String[] args) {
        //Step 3 : Instantiate class / Create instance of Class
        Employee emp;   //emp is reference
        emp = new Employee();   //Instance

        //Step 4 : call acceptRecord( ) method on instance( actually reference )
        emp.acceptRecord( );    //emp.acceptRecord( emp );

        emp.printRecord( ); //emp.printRecord( emp );
    }
}