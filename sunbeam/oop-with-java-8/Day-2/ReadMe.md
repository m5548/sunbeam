# Day 2
## Java Buzzwords
### Java is Simple Programming Language
* Syntax of Java is simple than C/C++. Hence java is considered as simple programming language.
    1. No Need of Header files and macros.
    2. We can not declare or define anything global.
    3. Do not support structure and union.
    4. Do not support delete operator and destructor.
    5. Do not support copy constructor and assignment operator function.
    6. Do not support default argument and constructor member initializer list.
    7. Do not support friend function and friend class.
    8. Do not support operator overloading
    9. Do not support constant member function.
    10. Java do not support multi-class inheritance( Multiple Implementation Inheritance )
    11. No diamond problem, hence do not support virtual base class concept.
    12. Do not support virtual keyword.
    13. Do not support private and protected mode of inheritance.
    14. Do not support sizeof operator.
    15. Do not support pointer and pointer arithmetic.
    16. Do not support RTTI and Advanced casting operators.

* Size of software, that is required to do java development is small. Hence Java is considered as simple.

### Java is Object Oriented Programming Language.
* Java programming language is object oriented as well as functional oriented.
* Object Orineted Programming Structure( OOPS )
    - oops is not a syntax. It is a programming methodology that we can use to solve real world problems.
    - oops is a process / methodology.
    - Alan Kay is inventor of oops.
    - There are 4 major pillars/parts/elements of oops.
        1. Abstraction      ->  To achive simplicity
        2. Encapsulation    ->  To hide and secure data
        3. Modularity       ->  To minimize module dependency
        4. Hierachy         ->  To achive reusability
    - Here word "major" means, langauge without above features can not be considered as object oriented.
    - There are 3 minor pillars/parts/elements of oops.
        1. Typing / Polymorphism    ->  To reduce maintenance of the system
        2. Concurrency              ->  To Utilize H/W resources efficiently
        3. Persistence              ->  To maintain state of the instance on secondry storage
    - Word "minor" means, if language supports to above features then it will be useful but not essential to classify language object oriented.
* Since Java supports all major and minor pillars of oops, It is object oriented.

### Java is Architecture Neutral programming language.
* Architecture means CPU Architecture.
* CPU Architectures : ( X86/X64, ARM, POWER PC, SPARC, ALPHA etc. )
* After compilation, Java compiler, do not generate CPU architecture specific code. Rather it generates "Bytecode". Which is CPU architecture neutral code. Hence Java is considered as Architectire neutral programming language.
* CPU do not execute bytecode. Rather, JVM executes bytecode.
* If we want to see bytecode then we should use following command:
```java
javap -c Program.class 
```
* Bytecode is an object oriented assembly language code, which is designed for JVM.
* Bytecode is also called as virtual code / managed code.

### Java is Portable programming language
* Since Java is architecture neutral, Java is portable programming language.
* JVM, JRE and JDK are platform dependent. Bytecode is architecture neutral.
* In Java, size of data types on all the platforms is constant. Hence Java is considered as truly portable programming language.
    1. boolean  :   Not mentioned
    2. byte     :   1 byte
    3. char     :   2 bytes
    4. short    :   2 bytes
    5. int      :   4 bytes
    6. float    :   4 bytes
    7. double   :   8 bytes
    8. long     :   8 bytes
* Since java is portable programming language, It does not support sizeof operator.

### Java is Robust programming language
* Java is robust programming language due to following reasons:
    1. Architecture Neutral
        - Since Java is architecture neutral, developer need not to do OS or H/W specific code.
    2. Object Orientation
        - Object orientation help Java application developer to reuse exisiting code with less effors.
    3. Memory Management
        - Garbage collector helps developer to deallocte memory of unused objects.
    4. Exception handling
        - Java compiler, help application developer to write try catch block

### Java is multithreaded programming language
* Process / Task
    1. Running instance of a program is called process.
    2. Application in execution is called process.
* Thread
    1. Light weight process / sub process is called thread.
    2. Thread is a separate path of execution, which runs independently.
* Single tasking:
    - Ability of an operating system, to execute single task/process at a time, is called single tasking operating system.
    - Example : MSDOS
* Multi tasking:
    - Ability of an operating system, to execute multiple task/processes at a time, is called multi tasking operating system.
    - Example : Windows 10, Ubunutu 21.04, Mac OS Big Sur etc.
* During execution, if application take help of single thread then it is called single threaded application.
* During execution, if application take help of multiple threads then it is called multi threaded application.
* Java is multithreaded because:
    1. When we start execution of Java application then JVM take help of 2 threads i.e main thread and garbage collector. Due to these threads, every Java application is multithreaded.
    2. Thread is OS / non Java resource. To access OS thread, Java application developer, need not to do native coding. SUN/ORACLE developers has already developed framework to use OS thread. Hence Java is considered as Multithreaded.

### Java is Dynamic programming language
1. In Java, all the methods are by default virtual.
2. Java is designed in such a manner that we can execute our application on any upcomming operating as well as hardware.
3. Java's reflection feature allows developer to get reflective information of any instance/class at runtime.
### Java is a Secure programming language
* To achive security, Java application developer need not to write code from scratch. With the help of security framework privided by SUN/ORACLE, we can achieve security. Hence Java is considered as secure Program language.
### Java application generates high performance
* Performance of Java application is slower than C/C++.
* JIT compiler helps JVM to compile and optimize bytecode. Which help to imporve performance of Application.
* Using JNI we can use C/C++ code into Java application. Which help to imporve performance of Application.
### Java is Distributed Programming Language.
* Since Java supports RMI, java is considered as Distributed Programming Language.

## Object Oriented Programming
* day(int), month(int), year(int)  are related to Date
```java
class Date{
    int day;
    int month;
    int year;
}
```
* name(String), rollNumber(int), marks(float) are related to Student
```java
class Student{
    String name;
    int rollNumber;
    float makrs;
}
```
* If we want to group related data elements together then we should use class.
* class is keyword in Java.
* A variable declared inside method is called local variable / method local variable.
* If we call method then method local variable get space on Java Stack.
* A variable declared inside class / class scope is called field. 
* Field is also called as Property or attribute.
* If create instance of a class then field get space inside it. In other words, only fields get space once per instance according to their order of declaration inside class.
* If we want to create instance of a class then we should use new operator.
```java
new Employee( );    //Instance of Employee class
//More specifically, it is anonymous instance of Employee class.
```
* If create instance using new operator then it gets space on Heap.
* If we want to perform operations on instance then we should create reference of the instance.
* Function implemeted inside class is called method.
* Method is also called as operation, behavior or message.
* In Java, object is also called as instance.
* Process of creating instance from a class is called instantiation.
```java
ClassName refName = new ClassName( );
```
* Process of calling method on instance is called message passing.
```java
Employee emp =  new Employee(); //Instantiation
emp.printRecord( ); //Message passing
//Here printRecord() method is called on emp.
```

## Phases of Software Development Life Cycle
1. Requirement
2. Analysis
3. Design
4. Coding/Implementation
5. Testing
6. Deployment / Installation
7. Maintenance

## Steps to solve real problem using OOPs
1. Understand problem statement / clients requirement.
2. According to requirement, decide and declare class(es).
3. Decide fields and declare it inside  the class.
4. To store value/state, Instantiate the class i.e. create instance of the class.( Now fields will get space inside instance ).
5. To process state of instance, invoke/call method on instance.( Now we should define method inside class ).
## this reference
* If we call method on instance( actually reference ) then Java compiler(javac) implicitly pass, reference of that instance as a argument to that method. To store reference of current/calling instance, Compiler implicitly declared one parameter inside method. It is called this reference.
* this is a keyword in Java. It is used to store reference of current/calling instance.
* this refernce is a method parameter. Hence it gets space once per method call.
* Using this reference, fields and methods can communicate with each other. Hence this reference is considered as a link/connection between them.
* Definition : this refernce is implicit reference variable which is available in every non static method of class which is used to store reference of current/calling instance.

## Access Modifier
* If we want to control visibility of members of the class then we should use access modifier.
* 4 access modifiers are there in Java.
    1. private
    2. default( package level private )
    3. protected
    4. public
* In Java, class members are by default considered as package level private.
* In C++, Parent class is called Base class and Child class is called derived class.
* In Java, Parent class is called Super class and Child class is called sub class.

```java
true    :   boolean
'A'     :   Character
123     :   byte/short/int/long
3.142   :   double
3.14f   :   float
"DBDA"  :   String
null    :   To initialize only reference variable
//int num1 = null;  //Not Ok
//Integer num2 = null;    //OK
```
* Consider following code:
```java
class Program{
    public static void main(String[] args) {
       Employee emp;
       emp.printRecord( );  //error: variable emp might not have been initialized
    }
}
```
* Solution 1
```java
class Program{
    public static void main(String[] args) {
       Employee emp = new Employee();
       emp.printRecord( );  //OK
    }
}
```
* Solution 2
```java
class Program{
    public static void main(String[] args) {
       Employee emp;
       emp = new Employee();    //OK
       emp.printRecord( );  //OK
    }
}
```
* If reference contains null value then such reference variable is called null reference variable / null object.
* Method of a class which is having a body is called concrete method.
* Non static method of a class is called instance method. It is designed to call on instance.
* Non static fields declared inside class is called instance variable. It gets space inside instance.
* A class that we can instantiate is called concrete class. In other words, we can instantiate concrete class.
* Stack Trace
```java
Exception in thread "main" java.lang.NullPointerException
        at Program.main(Program.java:26)
```
* Using null object, if we try to access any member of a class then JVM throws NullPointerException.
```java
class Program{
    public static void main(String[] args) {
        Employee emp = null; //emp : null reference variable / null object
        emp.printRecord( ); //NullPointerException
    }
}
```
* Solution
```java
class Program{
    public static void main(String[] args) {
        Employee emp = null; //emp : null reference variable / null object
        emp = new Employee(); 
        emp.printRecord( ); //OK
    }
}
```

## Intialization
```java
int num1 = 10;      //Intialization
int num2 = num1;    //Intialization
```
* During declaration, process of storing value inside variable is called initialization.
* We can do initialization only once.
## Assignment
```java
int num1 = 10;      //Intialization
int num2;
num2 = num1;        //Assignment
num2 = 20;        //Assignment
```
* After declaration of variable, process of storing value inside variable is called assignment.
* We can do assignment multiple times.

## Constructor
* Constructor is a method like syntax but it is not a method.
* If we want to initialize instance then we should define constructor inside class.
* Due to following reasons constructor is considered as special syntax of a class:
    1. Its name is same as class name.
    2. It doesn't have any return type.
    3. It is desgined to call implicitly.
    4. It is designed to call once per instance.
* We can not call constructor on instance explicitly.
```java
class Program{
    public static void main(String[] args) {
        Complex c1 = new Complex( );    //OK
        c1.Complex( );  //not OK
    }
}
```
* Constructor calling sequence, depends on order of instance declaration.
* Types of constructor
    1. Parameterless constructor
    2. Parameterized constructor
    3. Default constructor
### Parameterless constructor
* A constructor, which do not accept any argument is called parameterless constructor.
```java
class Complex{
    private int real;
    private int imag;
    public Complex( ){  //Parameterless Constructor
        this.real = 10;
        this.imag = 20;
    }
}
```
* Consider following code:
```java
public static void main(String[] args) {
    Complex c1 = new Complex( );    //Here on instance, parameterless ctor will call.
}
```
* If we create instance, without passing arguments then parameterless constructor gets called. Hence parameterless constructor is also called as zero argument constructor.

### Parameterized constructor
* If constructor which accept arguments then such constructor is called parameterized constructor.
```java
class Complex{
    private int real;
    private int imag;
    public Complex( int real, int imag ){  //Parameterized Constructor
        this.real = real;
        this.imag = imag;
    }
}
```
* Consider following code:
```java
public static void main(String[] args) {
    Complex c1 = new Complex( 10, 20 ); //Here, on instance, parameterized constructor will call.
}
```
* If we create instance, by passing arguments then parameterized constructor gets called.

### Default constructor
* If we do not define any constructor inside class then compiler generates one constructor for that class by default. It is called default constructor.
* Compiler do not generate default parameterized constructor for the class. Default constructor is always parameterless.
* We can define Multiple constructors inside class. It is called constructor overloading.

### Constructor Chaining
* If we want to reuse, body of existing constructor then we should call constructor from another constructor. 
* Process of calling constructor from another constructor is called constructor chaining.
* For constructor chaining, we should use this Statement.
* this statement must be first statement inside constructor body.
* Its Java lanaguage feature.
```java
class Complex{
    private int real;
    private int imag;
    public Complex( ){
        this( 10,20 );  //Constructor Chaining
    }
    public Complex( int real, int imag){
        this.real = real;
        this.imag = imag;
    }
}
```