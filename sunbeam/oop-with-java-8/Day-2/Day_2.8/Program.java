import java.util.Scanner;
class Employee{ //Concrete Class
    private String name;    //Non Static Field -> Instance variable
    private int empid;      //Non Static Field -> Instance variable
    private float salary;   //Non Static Field -> Instance variable
    //Concrete Method
    public void acceptRecord(  ){  //Non Static Method ->   Instance Method
        Scanner sc = new Scanner( System.in);
        System.out.print("Name   :   ");
        this.name = sc.nextLine( );
        System.out.print("Empid   :   ");
        this.empid = sc.nextInt();
        System.out.print("Salary   :   ");
        this.salary = sc.nextFloat( );
    }
    //Concrete Method
    public void printRecord(  ){  //Non Static Method ->   Instance Method
        System.out.println("Name    :   "+this.name);
        System.out.println("Empid    :   "+this.empid);
        System.out.println("Salary    :   "+this.salary);
    }
}
class Program{
    public static void main(String[] args) {
        Employee emp = null; //emp : null reference variable / null object
        emp = new Employee(); 
        emp.printRecord( ); //OK
    }
}