package org.sunbeam.dbda.inheritance.test;

import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		try(Program p = new Program();){ //The resource type Program does not implement java.lang.AutoCloseable
			//TODO
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
	public static void main3(String[] args) {
		try(Scanner sc1 = new Scanner(System.in);
				Scanner sc2 = new Scanner(System.in);
					Scanner sc3 = new Scanner(System.in);){
			//TODO
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
	public static void main2(String[] args) {
		try(Scanner sc = new Scanner(System.in);){
			//TODO
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
	public static void main1(String[] args) {
		Scanner sc = null;
		try {
			sc = new Scanner(System.in);
			//TODO
		}catch( Exception ex ) {
			ex.printStackTrace();
		}finally {
			sc.close( );
		}
	}
}
