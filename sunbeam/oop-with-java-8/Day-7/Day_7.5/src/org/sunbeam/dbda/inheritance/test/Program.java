package org.sunbeam.dbda.inheritance.test;

import java.util.Scanner;

class Product{
	private String title;	//null
	private float price;	//0.0
	public Product( ) {
	}
	public void acceptRecord( ) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Title		:	");
		this.title = sc.nextLine();
		System.out.print("Price		:	");
		this.price = sc.nextFloat();
	}
	public void printRecord( ) {
		System.out.println("Title		:	"+this.title);
		System.out.println("Price		:	"+this.price);
	}
}
class Book extends Product{
	private int pageCount;	//0
	public Book( ) {
	}
	public void acceptRecord( ) {
		Scanner sc = new Scanner(System.in);
		super.acceptRecord();
		System.out.print("Page Count	:	");
		this.pageCount = sc.nextInt( );
	}
	public void printRecord( ) {
		super.printRecord();
		System.out.println("Page Count	:	"+this.pageCount);
	}
}
class Tape extends Product{
	private int playTime;	//0
	public Tape( ) {
	}
	public void acceptRecord( ) {
		Scanner sc = new Scanner(System.in);
		super.acceptRecord();
		System.out.print("Play Time	:	");
		this.playTime = sc.nextInt( );
	}
	public void printRecord( ) {
		super.printRecord();
		System.out.println("Play Time	:	"+this.playTime);
	}
}
public class Program {
	public static Scanner sc = new Scanner(System.in);
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Book");
		System.out.println("2.Tape");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		while( ( choice = Program.menuList( ) ) != 0 ) {
			Product product = null;
			switch( choice ) {
			case 1:
				product = new Book( );	//Upcasting
				break;
			case 2:
				product = new Tape( );	//Upcasting
				break;
			}
			if( product != null ) {
				product.acceptRecord();	//DMD
				product.printRecord();	//DMD
			}
		}
	}
	public static void main2(String[] args) {
		int choice;
		while( ( choice = Program.menuList( ) ) != 0 ) {
			Product product = null;
			switch( choice ) {
			case 1:
				product = new Book( );
				product.acceptRecord();
				product.printRecord();
				break;
			case 2:
				product = new Tape( );
				product.acceptRecord();
				product.printRecord();
				break;
			}
		}
	}
	public static void main1(String[] args) {
		int choice;
		while( ( choice = Program.menuList( ) ) != 0 ) {
			Book book = null;
			Tape tape = null;
			switch( choice ) {
			case 1:
				book = new Book( );
				book.acceptRecord();
				book.printRecord();
				break;
			case 2:
				tape = new Tape( );
				tape.acceptRecord();
				tape.printRecord();
				break;
			}
		}
	}
}
