package org.sunbeam.dbda.inheritance.test;

public class Program {
	public static void main(String[] args) {
		//num1 and num2 are variables of primitive type
		int num1  = 10;
		int num2  = 10;
		if( num1.equals( num2 ) )//Cannot invoke equals(int) on the primitive type int
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
		//Equal
	}
	public static void main1(String[] args) {
		//num1 and num2 are variables of primitive type
		int num1  = 10;
		int num2  = 10;
		if( num1 == num2 )
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
		//Equal
	}
}
