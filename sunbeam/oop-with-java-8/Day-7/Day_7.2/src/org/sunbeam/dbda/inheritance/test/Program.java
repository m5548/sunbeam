package org.sunbeam.dbda.inheritance.test;

class Person{
	public void print( int number ) {
		System.out.println("Person.print()");
	}
}
class Employee extends Person{
	@Override	//Annotation
	public void print( int number ) {
		System.out.println("Employee.print()");
	}	
}
public class Program {
	public static void main(String[] args) {
		Employee emp = new Employee();
		emp.print(0);
	}
	public static void main1(String[] args) {
		Person p = new Employee();	//Upcasting
		p.print( 0 );
	}
}
