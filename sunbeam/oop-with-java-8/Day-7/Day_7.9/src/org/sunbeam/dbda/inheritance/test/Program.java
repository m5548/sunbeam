package org.sunbeam.dbda.inheritance.test;

abstract class Test{
	public abstract void f1( );
	public abstract void f2( );
	public abstract void f3( );
}
abstract class AbstractTest extends Test{	//Helper class
	@Override
	public void f1() {	}
	@Override
	public void f2() {	}
	@Override
	public void f3() {	}
}
class A extends AbstractTest{
	@Override
	public void f1() {
		System.out.println("A.f1");
	}
}
class B extends AbstractTest{
	@Override
	public void f2() {
		System.out.println("B.f2");
	}
}
class C extends AbstractTest{
	@Override
	public void f3() {
		System.out.println("C.f3");
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = null;
		
		t  = new A( );
		t.f1();//DMD
		
		t  = new B( );
		t.f2();//DMD
		
		t  = new C( );
		t.f3();//DMD
	}
}
