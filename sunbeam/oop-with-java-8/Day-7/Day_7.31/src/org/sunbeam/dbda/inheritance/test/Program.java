package org.sunbeam.dbda.inheritance.test;
public class Program {
	public static void main(String[] args) {
		int num1 = 10;
		Object obj = num1;	//Auto-Boxing -> Upcasting 
		//Object obj =  Integer.valueOf(num1);
		System.out.println(obj);
	}
	public static void main1(String[] args) {
		int num1 = 10;
		Integer integer = Integer.valueOf(num1);	//Boxing
		Object obj = integer;	//Upcasting
	}
}
