package org.sunbeam.dbda.inheritance.test;

class Person{
	public void print( ) {
		System.out.println("Person.print()");
	}
}
class Employee extends Person{
	public void print( ) {
		System.out.println("Employee.print()");
	}	
}
public class Program {
	public static void main(String[] args) {
		//Employee emp = new Person();	//Compiler Error
	}
	public static void main7(String[] args) {
		Person p = new Person();	
		Employee emp = (Employee) p;	//Downcasting --> ClassCastException
		emp.print();//Employee.print()
	}
	public static void main6(String[] args) {
		Person p = new Employee();	//Upcasting
		Employee emp = (Employee) p;	//Downcasting
		emp.print();//Employee.print()
	}
	public static void main5(String[] args) {
		Person p = null;
		Employee emp = (Employee) p;	//Downcasting
		System.out.println(p+"	"+emp);	//null	null
	}
	public static void main4(String[] args) {
		Person p = new Employee();	//Upcasting
		p.print();//Employee.print()	--> Dynamic Method Dispatch
	}
	public static void main3(String[] args) {
		Employee emp = new Employee();
		Person p = emp;	//Upcasting
		p.print();//Employee.print()
	}
	public static void main2(String[] args) {
		Employee emp1 = new Employee();
		emp1.print();//Employee.print()
	}
	public static void main1(String[] args) {
		Person p1 = new Person( );
		p1.print();	//Person.print()
	}
}
