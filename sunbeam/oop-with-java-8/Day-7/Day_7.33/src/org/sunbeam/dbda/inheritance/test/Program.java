package org.sunbeam.dbda.inheritance.test;

import java.util.Date;

public class Program {
	public static void main(String[] args) {
		Object obj = new Date() ;	//Upcasting
		
		String str = (String) obj;	//Downcasting : ClassCastException
		
		Date dt = (Date) obj;	//Downcasting : OK
			
	}
	public static void main4(String[] args) {
		Object obj = new String() ;	//Upcasting
		
		String str = (String) obj;	//Downcasting : OK
		
		Date dt = (Date) obj;	//Downcasting : ClassCastException
			
	}
	public static void main3(String[] args) {
		Object obj = null;
		
		String str = (String) obj;	//Downcasting
		
		Date dt = (Date) obj;	//Downcasting
		
		System.out.println(obj+"	"+str+"	"+dt);	//null	null	null
	}
	public static void main2(String[] args) {
		Date dt1 = new Date( );
		System.out.println(dt1.toString());
		
		Object obj = new Date( );	//Upcasting
		Date dt2 = (Date) obj;	//Downcasting
	}
	public static void main1(String[] args) {
		//String str1 = new String("DBDA");	//OK
		
		Object obj = new String( "DBDA" );	//Upcasting
		String str2 = (String) obj;	//Downcasting
	}
}
