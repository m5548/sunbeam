package org.sunbeam.dbda.inheritance.test;
public class Program {
	public static void main(String[] args) {
		Integer integer = new Integer("125");
		//int number = integer.intValue(); //Unboxing
		int number = integer; //Aut-Unboxing
		System.out.println("Number	:	"+number);
	}
	public static void main1(String[] args) {
		String str = "123";
		int number =  Integer.parseInt(str); //UnBoxing
		System.out.println("Number	:	"+number);
	}
}
