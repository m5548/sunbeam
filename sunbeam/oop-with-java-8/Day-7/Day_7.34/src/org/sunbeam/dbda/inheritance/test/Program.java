package org.sunbeam.dbda.inheritance.test;

import java.util.Date;

class Box{
	private Object object;
	public Object getObject() {
		return this.object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
}
public class Program {
	public static void main(String[] args) {
		Box b4 = new Box( );
		
		b4.setObject(new Date( ));
		//Date date = (Date) b4.getObject();	//Downcasting
		
		String str = (String) b4.getObject();	//Downcasting : ClassCastException
		System.out.println(str);
	}
	public static void main3(String[] args) {
		Date dt1 = new Date();
		Box b3 = new Box( );
		b3.setObject(dt1);
		
		Date dt2 = (Date) b3.getObject();
		System.out.println(dt2.toString());
	}
	public static void main2(String[] args) {
		int num1 = 125;
		Box b2  = new Box( );
		b2.setObject(num1);	//b2.setObject(Integer.valueOf(num1));
		
		Integer integer = (Integer) b2.getObject();	//Downcasting
		int num2 = integer.intValue();
	}
	public static void main1(String[] args) {
		Box b1  = new Box( );
	}
}
