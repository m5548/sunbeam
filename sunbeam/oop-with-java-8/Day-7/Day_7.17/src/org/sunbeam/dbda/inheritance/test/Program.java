package org.sunbeam.dbda.inheritance.test;
class Employee extends Object{
	private String name;
	private int empid;
	private String department;
	private String designation;
	private String joinDate;
	private float salary;
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	public Employee(String name, int empid, String department, String designation, String joinDate, float salary) {
		this.name = name;
		this.empid = empid;
		this.department = department;
		this.designation = designation;
		this.joinDate = joinDate;
		this.salary = salary;
	}
	public void printRecord( ) {
		System.out.println(this.name+"	"+this.empid+"	"+this.department+"	"+this.designation+"	"+this.joinDate+"	"+this.salary);
	}
	/* @Override
	public String toString() {
		return this.name+"	"+this.empid+"	"+this.salary;
	} */
	@Override
	public String toString() {
		return "Employee [name=" + name + ", empid=" + empid + ", salary=" + salary + "]";
	}	
}

public class Program {
	public static void main(String[] args) {
		Employee emp = new Employee("Sandeep", 33, "TCT", "Head Technical", "26/12/2006", 45000.50f);		
		System.out.println(emp);
		
		//System.out.println(String.valueOf(emp));
		
		/*
		 	public static String valueOf(Object obj) {
        		return (obj == null) ? "null" : obj.toString();
    		}
		*/
	}
	public static void main2(String[] args) {
		Employee emp = new Employee("Sandeep", 33, "TCT", "Head Technical", "26/12/2006", 45000.50f);		
		System.out.println(emp.toString());
	}
	public static void main1(String[] args) {
		Employee emp = new Employee("Sandeep", 33, "TCT", "Head Technical", "26/12/2006", 45000.50f);
		String str = emp.toString();
		System.out.println(str);
	}
}
