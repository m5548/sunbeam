package org.sunbeam.dbda.inheritance.test;
public class Program {
	public static void main(String[] args) {
		int num1 = 10;
		String str = String.valueOf(num1);	//Boxing
	}
	public static void main3(String[] args) {
		int num1 = 10;
		String str = Integer.toString( num1 );	//Boxing
	}
	public static void main2(String[] args) {
		int num1 = 10;
		Integer i = new Integer(num1);	//Boxing
	}
	public static void main1(String[] args) {
		int num1 = 10;
		Integer i = Integer.valueOf(num1); //Boxing
	}
}
