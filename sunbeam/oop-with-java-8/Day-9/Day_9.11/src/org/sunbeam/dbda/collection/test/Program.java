package org.sunbeam.dbda.collection.test;

import java.util.Set;
import java.util.TreeSet;

public class Program {
	public static void main(String[] args) {
		Set<Integer> set = new TreeSet<>( );
		
		set.add(50);
		set.add(10);
		set.add(40);
		set.add(20);
		set.add(30);
		
		set.add(50);
		set.add(10);
		set.add(40);
		set.add(20);
		set.add(30);
		
		//set.add(null); //NullPointerException
		
		set.forEach(System.out::println);
	}
}
