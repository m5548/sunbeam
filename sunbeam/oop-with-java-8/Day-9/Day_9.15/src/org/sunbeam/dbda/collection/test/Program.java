package org.sunbeam.dbda.collection.test;

import java.util.HashSet;
import java.util.Set;

public class Program {
	public static void main(String[] args) {
		Set<Integer> set = new HashSet<>();
		set.add(24);
		set.add(12);
		set.add(189);
		set.add(973);
		set.add(5);
		set.add(null);
		
		set.add(24);
		set.add(12);
		set.add(189);
		set.add(973);
		set.add(5);
		set.add(null);
		set.forEach(System.out::println);
	}
}
