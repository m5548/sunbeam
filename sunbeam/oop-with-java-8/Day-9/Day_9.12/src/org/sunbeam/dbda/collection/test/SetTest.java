package org.sunbeam.dbda.collection.test;
import java.util.Set;

import org.sunbeam.dbda.collection.model.Employee;

public class SetTest {
	private Set<Employee> empList;	//null
	public void setEmpList(Set<Employee> empList) {
		this.empList = empList;
	}
	
	public void addRecord(Employee[] arr) {
		if( this.empList != null ) {
			if( arr != null ) {
				for( Employee emp : arr )
					this.empList.add(emp);
			}
		}
	}
	
	public Employee findRecord(int empid ) {
		if( this.empList != null ) {
			for( Employee emp : this.empList ) {
				if( emp.getEmpid() == empid )
					return emp;
			}
		}
		return null;
	}
	
	public boolean removeRecord(int empid) {
		Employee key = new Employee();
		key.setEmpid(empid);
		if( this.empList.contains(key)) {
			this.empList.remove(key);
			return true;
		}
		return false;
	}
	
	public void printRecord( ) {
		if( this.empList != null ) 
			this.empList.forEach(System.out::println);
	}
}
