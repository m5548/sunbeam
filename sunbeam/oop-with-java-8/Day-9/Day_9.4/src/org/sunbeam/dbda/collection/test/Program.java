package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
public class Program {
	private static List<Integer> getList() {
		List<Integer> list = new ArrayList<Integer>();
		list.add( 50 );
		list.add( 10 );
		list.add( 40 );
		list.add( 20 );
		list.add( 30 );
		return list;
	}
	public static void main(String[] args) {
		List<Integer> list = Program.getList();
		
		//Collections.sort(list);
		
		list.sort(null);
		
		list.forEach(System.out::println);
	}
}
