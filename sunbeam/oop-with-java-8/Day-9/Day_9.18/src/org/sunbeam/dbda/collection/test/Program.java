package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Program {
	private static Map<Integer, String> getMap() {
		Map<Integer, String> map = new Hashtable<>();
		map.put(34, "DAC");
		map.put(92, "DMC");
		map.put(107, "DESD");
		map.put(65, "DBDA");
		map.put(6, "PREDAC");
		return map;
	}
	private static void printKeys(Map<Integer, String> map) {
		if( map != null ) {
			Set<Integer> keys = map.keySet();
			for (Integer key : keys) 
				System.out.println(key);
		}
	}	
	private static void printValues(Map<Integer, String> map) {
		if( map != null ) {
			Collection<String> values = map.values();
			for (String value : values) 
				System.out.println(value);
		}
	}
	private static void printEntries(Map<Integer, String> map) {
		if( map != null ) {
			Set<Entry<Integer, String>> entries = map.entrySet();
			for (Entry<Integer, String> entry : entries) 
				System.out.println(entry.getKey()+"	"+entry.getValue());
		}
	}
	private static void findAndPrint(Map<Integer, String> map, int id) {
		if( map != null ) {
			Integer key = new Integer( id );
			if( map.containsKey(key)) {
				String value = map.get(key);
				System.out.println(key+"	"+value);
			}else
				System.out.println(id+" not found");
		}
	}
	private static void findAndRemove(Map<Integer, String> map, int id) {
		if( map != null ) {
			Integer key = new Integer( id );
			if( map.containsKey(key)) {
				String value = map.remove(key);
				System.out.println(key+"	"+value+" is removed");
			}else
				System.out.println(id+" not found");
		}
	}
	private static List<String> getCourses(Map<Integer, String> map) {
		return new ArrayList<String>( map.values());
		/* Collection<String> values = map.values();
		List<String> list = new ArrayList<String>(values);
		return list; */
	}	
	public static void main(String[] args) {
		Map<Integer, String> map = Program.getMap( );
		
		//Program.printKeys( map );
		
		//Program.printValues( map );
		
		//Program.printEntries( map );
		
		//Program.findAndPrint( map, 65 );
		
		//Program.findAndRemove( map, 6 );
		
		List<String> courses = Program.getCourses( map );
		courses.forEach(System.out::println);
	}
}
