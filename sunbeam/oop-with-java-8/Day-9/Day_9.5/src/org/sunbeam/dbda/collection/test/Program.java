package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
public class Program {
	private static List<Integer> getList() {
		List<Integer> list = new ArrayList<Integer>();
		list.add( 10 );
		list.add( 20 );
		list.add( 60 );
		list.add( 70 );
		list.add( 80 );
		return list;
	}
	public static void main(String[] args) {
		List<Integer> elements = Arrays.asList(30, 40, 50 );
		
		List<Integer> list = Program.getList();
		//list.addAll(elements);
		list.addAll(2, elements);
		list.forEach(System.out::println);
	}
}
