package org.sunbeam.dbda.collection.test;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.TreeMap;

import org.sunbeam.dbda.collection.model.Account;
import org.sunbeam.dbda.collection.model.Customer;
public class Program {
	private static Scanner sc = new Scanner(System.in);
	public static Account[] getKeys( ) {
		Account[] arr = new Account[ 3 ];
		arr[ 0 ] = new Account(1562,"Saving",50000);
		arr[ 1 ] = new Account(8271,"Loan",60000);
		arr[ 2 ] = new Account(3617,"Current",70000);
		return arr;
	}
	public static Customer[] getValues( ) {
		Customer[] arr = new Customer[ 3 ];
		arr[ 0 ] = new Customer("Amol","amol@gmail.com","111");
		arr[ 1 ] = new Customer("Rupesh","rupesh@gmail.com","222");
		arr[ 2 ] = new Customer("Nilesh","nilesh@gmail.com","333");
		return arr;
	}
	public static void acceptRecord( int[] accountNumber ) {
		System.out.print("Enter account number	:	");
		accountNumber[ 0 ] = sc.nextInt();
	}
	private static void printRecord(Customer value) {
		if( value != null )
			System.out.println(value.toString());
		else
			System.out.println("Account not found.");
	}
	private static void printRecord(boolean removedStatus) {
		if( removedStatus )
			System.out.println("Entry is removed.");
		else
			System.out.println("Account not found.");
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Add Record");
		System.out.println("2.Find Record");
		System.out.println("3.Remove Record");
		System.out.println("4.Print Records");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		int[] accountNumber = new int[ 1 ];
		
		MapTest test = new MapTest();
		//test.setMap(new Hashtable<Account, Customer>());
		//test.setMap(new HashMap<Account, Customer>());
		//test.setMap(new LinkedHashMap<Account, Customer>());
		test.setMap(new TreeMap<Account, Customer>());
		while( ( choice = Program.menuList( ) ) != 0 ) {
			switch( choice ) {
			case 1:
				Account[] keys = Program.getKeys();
				Customer[] values = Program.getValues();
				test.addRecord( keys, values );
				break;
			case 2:
				Program.acceptRecord(accountNumber);
				Customer value = test.findRecord( accountNumber[ 0 ] );
				Program.printRecord( value );
				break;
			case 3:
				Program.acceptRecord(accountNumber);
				boolean removedStatus = test.removeRecord( accountNumber[ 0 ] );
				Program.printRecord(removedStatus);
				break;
			case 4:
				test.printRecord( );
				break;
			}
		}
	}
}
