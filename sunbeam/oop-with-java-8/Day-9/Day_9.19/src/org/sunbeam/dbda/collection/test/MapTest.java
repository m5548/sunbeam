package org.sunbeam.dbda.collection.test;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.sunbeam.dbda.collection.model.Account;
import org.sunbeam.dbda.collection.model.Customer;

public class MapTest {
	private Map<Account, Customer> map;
	public void setMap(Map<Account, Customer> map) {
		this.map = map;
	}
	public void addRecord(Account[] keys, Customer[] values) {
		if( this.map != null ) {
			if( keys != null && values != null ) {
				for( int index = 0; index < keys.length; ++ index )
					this.map.put(keys[ index ], values[ index ] );
			}
		}
	}
	public Customer findRecord(int accountNumber) {
		if( this.map != null ) {
			Account key = new Account();
			key.setNumber(accountNumber);
			if( this.map.containsKey(key)) {
				Customer value = this.map.get(key);
				return value;
			}
		}
		return null;
	}
	public boolean removeRecord(int accountNumber) {
		if( this.map != null ) {
			Account key = new Account();
			key.setNumber(accountNumber);
			if( this.map.containsKey(key)) {
				this.map.remove(key);
				return true;
			}
		}
		return false;
	}
	public void printRecord() {
		if( this.map != null ) {
			Set<Entry<Account, Customer>> entries = this.map.entrySet();
			for (Entry<Account, Customer> entry : entries) {
				Account key = entry.getKey();
				Customer value = entry.getValue();
				System.out.println(key+"	"+value);
			}
		}
	}
}
