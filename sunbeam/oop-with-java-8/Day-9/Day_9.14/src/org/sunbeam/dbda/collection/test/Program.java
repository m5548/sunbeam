package org.sunbeam.dbda.collection.test;

class Test{
	private int number;
	public Test() {
		// TODO Auto-generated constructor stub
	}
	public Test(int number) {
		this.number = number;
	}
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = result * this.number + PRIME;
		return result;
	}
}

public class Program {
	public static void main(String[] args) {
		Test t1 = new Test( 123 );	//154
		Test t2 = new Test( 123 );	//154
		
		System.out.println( t1.hashCode( ) );	//1829164700
		System.out.println( t2.hashCode( ) );	//2018699554
	}
	public static void mai2(String[] args) {
		String s1 = new String( "DAC" );
		String s2 = new String( "DBDA" );
		
		System.out.println(s1.hashCode());	//67430
		System.out.println(s2.hashCode());	//2091387
	}
	public static void main1(String[] args) {
		String s1 = new String( "DBDA" );
		String s2 = new String( "DBDA" );
		
		System.out.println(s1.hashCode());	//2091387
		System.out.println(s2.hashCode());	//2091387
	}
}
