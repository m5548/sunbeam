package org.sunbeam.dbda.collection.test;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

class Pair<K,V> implements Entry<K, V>{
	private K key;
	private V value;
	public Pair() {
	}
	public Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}
	@Override
	public K getKey() {
		return this.key;
	}
	@Override
	public V getValue() {
		return this.value;
	}
	@Override
	public V setValue(V value) {
		this.value = value;
		return this.value;
	}
}
public class Program {
	public static void main(String[] args) {
		Entry<Integer, String> e1 = new Pair<>( 1, "DBDA");
		Entry<Integer, String> e2 = new Pair<>( 2, "DESD");
		Entry<Integer, String> e3 = new Pair<>( 3, "DMC");
		Entry<Integer, String> e4 = new Pair<>( 4, "DAC");
		Entry<Integer, String> e5 = new Pair<>( 5, "PREDAC");
		
		Set< Entry<Integer,String> > map = new HashSet< >( );
		map.add(e1);
		map.add(e2);
		map.add(e3);
		map.add(e4);
		map.add(e5);
	}
	public static void main1(String[] args) {
		Entry<Integer, String> entry = new Pair<>( 1, "DBDA");
		System.out.println(entry.getKey());
		System.out.println(entry.getValue());
	}
}
