package org.sunbeam.dbda.collection.test;
public class Program {
	public static final int SIZE = 7;
	private static int getHashCode(int num1) {
		final int PRIME = 31;
		int result = 1;
		result = result * num1 + PRIME * num1;
		return result;
	}
	public static void main(String[] args) {
		
		for( int count = 1; count <= 15; ++ count ) {
			int number = count;
			int hashCode = Program.getHashCode( number  );
			int slot = hashCode % SIZE;
			System.out.println(number+"	"+hashCode+"	"+slot);
		}
	}
	public static void main2(String[] args) {
		int num1 = 289;
		int hashCode1 = Program.getHashCode( num1  );
		int slot1 = hashCode1 % SIZE;
		System.out.println(num1+"	"+hashCode1+"	"+slot1);
		
		int num2 = 389;
		int hashCode2 = Program.getHashCode( num2  );
		int slot2 = hashCode2 % SIZE;
		System.out.println(num2+"	"+hashCode2+"	"+slot2);
	}
	public static void main1(String[] args) {
		int num1 = 289;
		int hashCode1 = Program.getHashCode( num1  );
		System.out.println(num1+"	"+hashCode1);
		
		int num2 = 289;
		int hashCode2 = Program.getHashCode( num2  );
		System.out.println(num2+"	"+hashCode2);
	}
}
