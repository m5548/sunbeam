package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Program {
	public static void main(String[] args) {
		Collection<Integer> collection = Arrays.asList(10, 20, 30, 40, 50 ); //Ok
		
		Collection<Integer> list = new ArrayList<>( collection );
		System.out.println("Size	:	"+list.size());
		System.out.println(list);//[10, 20, 30, 40, 50]
	}
	public static void main5(String[] args) {
		Collection<Integer> collection = Arrays.asList(10, 20, 30, 40, 50 ); //Ok
		
		List<Integer> list = new ArrayList<>( collection );
		System.out.println("Size	:	"+list.size());
		System.out.println(list);//[10, 20, 30, 40, 50]
	}
	public static void main4(String[] args) {
		Collection<Integer> collection = Arrays.asList(10, 20, 30, 40, 50 ); //Ok
		
		ArrayList<Integer> list = new ArrayList<>( collection );
		System.out.println("Size	:	"+list.size());
		System.out.println(list);//[10, 20, 30, 40, 50]
	}
	public static void main3(String[] args) {
		//List<Integer> list = Arrays.asList(10, 20, 30, 40, 50 ); //OK
		Collection<Integer> list = Arrays.asList(10, 20, 30, 40, 50 ); //Ok
		System.out.println( list.getClass().getName());//java.util.Arrays$ArrayList
		System.out.println( list );//[10, 20, 30, 40, 50]
	}
	public static void main2(String[] args) {
		ArrayList<Integer> list1 = new ArrayList<>( 15 );
		
		List<Integer> list2 = new ArrayList< >( 15 );	//Upcasting
		
		Collection<Integer> list3 = new ArrayList<>( 15 );	//Upcasting
	}
	public static void main1(String[] args) {
		ArrayList<Integer> list1 = new ArrayList<>( );
		
		List<Integer> list2 = new ArrayList< >();	//Upcasting
		
		Collection<Integer> list3 = new ArrayList<>();	//Upcasting
	}
}
