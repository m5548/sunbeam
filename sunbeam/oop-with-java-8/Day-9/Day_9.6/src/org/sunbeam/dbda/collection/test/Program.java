package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Vector;

import org.sunbeam.dbda.collection.model.CompareByEmpid;
import org.sunbeam.dbda.collection.model.CompareByName;
import org.sunbeam.dbda.collection.model.CompareBySalary;
import org.sunbeam.dbda.collection.model.Employee;

public class Program {
	private static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(int[] empid) {
		if( empid != null ) {
			System.out.print("Enter empid	:	");
			empid[ 0 ] = sc.nextInt();
		}
	}
	private static void printRecord(Employee value) {
		if( value != null )
			System.out.println(value.toString());
		else
			System.out.println("Employee not found");
	}
	private static void printRecord(boolean removedStatus) {
		if( removedStatus )
			System.out.println("Employee is removed.");
		else
			System.out.println("Employee not found");
	}
	public static Employee[] getEmployees( ) {
		Employee[] arr = new Employee[ 5 ];
		arr[ 0 ] = new Employee("Nitin",13, 50000);
		arr[ 1 ] = new Employee("Amit", 11, 45000);
		arr[ 2 ] = new Employee("Sarang", 15, 40000);
		arr[ 3 ] = new Employee("Yogesh", 14, 25000);
		arr[ 4 ] = new Employee("Digvijay", 12, 30000);
		return arr;
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Add Record");
		System.out.println("2.Find Record");
		System.out.println("3.Remove Record");
		System.out.println("4.Print Records[ Sorted ]");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static int subMenuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Compare By Name");
		System.out.println("2.Compare By Empid");
		System.out.println("3.Compare By Salary");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		int[] empid = new int[ 1 ];
		
		ListTest test = new ListTest();
		test.setEmpList( new ArrayList<Employee>( ) );
		//test.setEmpList( new Vector<Employee>( ) );
		//test.setEmpList( new LinkedList<Employee>( ) );
		
		while( ( choice = Program.menuList( ) ) != 0 ) {
			switch( choice ) {
			case 1:
				Employee[] arr = Program.getEmployees();
				test.addRecord( arr );
				break;
			case 2:
				Program.acceptRecord(empid);
				Employee value = test.findRecord( empid[ 0 ] );
				Program.printRecord(value);
				break;
			case 3:
				Program.acceptRecord(empid);
				boolean removedStatus = test.removeRecord( empid[ 0 ] );
				Program.printRecord(removedStatus);
				break;
			case 4:
				while( ( choice = Program.subMenuList( ) ) != 0 ) {
					Comparator<Employee> comparator = null;
					switch( choice ) {
					case 1:
						comparator = new CompareByName();
						break;
					case 2:
						comparator = new CompareByEmpid();
						break;
					case 3:
						comparator = new CompareBySalary();
						break;
					}
					test.printRecord( comparator );
				}
				break;
			}
		}
	}
}
