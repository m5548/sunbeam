package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.sunbeam.dbda.collection.model.Employee;

public class ListTest {
	private List<Employee> empList;	//null
	public void setEmpList(List<Employee> empList) {
		this.empList = empList;
	}
	public void addRecord(Employee[] arr) {
		if( this.empList != null ) {
			if( arr != null ) {
				for( Employee emp : arr )
					this.empList.add(emp);
			}
		}
	}
	public Employee findRecord(int empid ) {
		Employee key = new Employee();
		key.setEmpid(empid);
		if( this.empList.contains(key)) {
			int index = this.empList.indexOf(key);
			return this.empList.get(index);
		}
		return null;
	}
	public boolean removeRecord(int empid) {
		Employee key = new Employee();
		key.setEmpid(empid);
		if( this.empList.contains(key)) {
			int index = this.empList.indexOf(key);
			this.empList.remove(index);
			return true;
		}
		return false;
	}
	public void printRecord(Comparator<Employee> comparator) {
		if( this.empList != null ) {
			this.empList.sort(comparator);
			this.empList.forEach(System.out::println);
		}
	}
}
