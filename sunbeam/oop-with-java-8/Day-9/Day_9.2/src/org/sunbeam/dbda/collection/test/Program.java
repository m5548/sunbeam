package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
public class Program {
	private static List<Integer> getList() {
		List<Integer> list = new ArrayList<Integer>();
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		return list;
	}
	public static void main(String[] args) {
		List<Integer> list = Program.getList( );
		
		Integer element = null;
		ListIterator<Integer> itr = list.listIterator( list.size() );
		while( itr.hasPrevious()) {
			element = itr.previous();
			System.out.print(element+"	");
		}
	}
	public static void main10(String[] args) {
		List<Integer> list = Program.getList( );
		
		Integer element = null;
		ListIterator<Integer> itr = list.listIterator();
		while( itr.hasNext()) {
			element = itr.next();
			System.out.print(element+"	");
		}
		System.out.println();
		while( itr.hasPrevious()) {
			element = itr.previous();
			System.out.print(element+"	");
		}
		
	}
	public static void main9(String[] args) {
		Program.getList().forEach(System.out::println);
	}
	public static void main8(String[] args) {
		List<Integer> list = Program.getList( );
		list.forEach(System.out::println);//Method Reference
	}
	public static void main7(String[] args) {
		List<Integer> list = Program.getList( );
		list.forEach( element -> System.out.println(element)  );	//Lambda Expression
	}
	public static void main6(String[] args) {
		List<Integer> list = Program.getList( );
		for( Integer element : list )
			System.out.println(element);
	}
	public static void main5(String[] args) {
		List<Integer> list = Program.getList( );
		
		Integer element = null;
		Iterator<Integer> itr = list.iterator();
		while( itr.hasNext()) {
			element = itr.next();
			System.out.println(element);
		}
	}
	public static void main4(String[] args) {
		List<Integer> list = Program.getList( );
		Integer element = list.get( list.size() ); //IndexOutOfBoundsException
		System.out.println(element);	
	}
	public static void main3(String[] args) {
		Integer element = null;
		List<Integer> list = Program.getList( );
		for( int index = 0; index < list.size(); ++ index ) {
			element = list.get(index);
			System.out.println( element );
		}
	}
	public static void main2(String[] args) {
		List<Integer> list = Program.getList( );
		Integer element = list.get(0);
		System.out.println(element);	//10
	}
	public static void main1(String[] args) {
		List<Integer> list = Program.getList( );
		System.out.println("Size	:	"+list.size());//Size	:	5
	}
}
