package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
public class Program {
	private static List<Integer> getList() {
		List<Integer> list = new ArrayList<Integer>();
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(40);
		list.add(50);
		list.add(60);
		list.add(70);
		list.add(80);
		list.add(90);
		list.add(100);
		return list;
	}
	public static void main(String[] args) {
		List<Integer> list = Program.getList( );
		Integer key = new Integer(50);
		if( list.contains(key)) {
			int index = list.indexOf(key);
			Integer element = list.get(index);
			System.out.println("Element	:	"+element);
		}else
			System.out.println(key+" does not exist");
	}
	public static void main1(String[] args) {
		List<Integer> list = Program.getList( );
		Integer key = new Integer(50);
		if( list.contains(key)) {
			int index = list.indexOf(key);
			list.remove(index);
			System.out.println(key +" is removed.");
		}else
			System.out.println(key+" does not exist");
	}
}
