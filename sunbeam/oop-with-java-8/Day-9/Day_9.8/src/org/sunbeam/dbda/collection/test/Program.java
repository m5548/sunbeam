package org.sunbeam.dbda.collection.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;
public class Program {
	public static Vector<Integer> getVector( ){
		Vector<Integer> vector = new Vector<>();
		vector.add( 10 );
		vector.add( 20 );
		vector.add( 30 );
		vector.add( 40 );
		vector.add( 50 );
		return vector;
	}
	public static void main(String[] args) {
		Vector<Integer> v = Program.getVector();
		
		Integer element = null;
		Enumeration<Integer> e = v.elements();
		while( e.hasMoreElements()) {
			element = e.nextElement();
			System.out.println(element);
			if( element == 50 )
				v.add(60); //Ok
		}
	}
	public static void main1(String[] args) {
		Vector<Integer> v = Program.getVector();
		
		Integer element = null;
		Iterator<Integer> itr = v.iterator();
		while( itr.hasNext()) {
			element = itr.next();
			System.out.println(element);
			if( element == 50 )
				v.add(60); //ConcurrentModificationException
		}
	}
}
