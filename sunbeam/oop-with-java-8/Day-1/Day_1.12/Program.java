class Program{
    public static void main(String[] args) {
        double num1 = 10.5; //OK
        int num2 = ( int )num1; //OK : Narrowing
        
        //int num2 = num1; //NOT OK : Narrowing
        //possible lossy conversion from double to int

        System.out.println("Num2    :   "+num2);    //10
    }
    public static void main1(String[] args) {
        double num1 = 10.5; //OK
        double num2 = num1; //OK
        System.out.println("Num2    :   "+num2);    //10.5
    }
}