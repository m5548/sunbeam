# Day 1
## Language, Technology, Platform, Framework
### Language
    * Language can contain:
        1. Syntax
        2. Data types
        3. Tokens( Indentifier, keywords, Constants, Operators, Punctuators )
        4. In built fatures
    * If we want to implement business logic then we should use language.
    * Using language we can create, following types of application:
        1. Console User Interface( CUI ) application.
        2. Graphical User Interface( GUI ) application.
        3. Library( .jar ) application.
    * Example: C, C++, Python , Java, C#, R, Scala, GO etc.
### Technology
    * If we want to develop application then we should use technology.
    * ASP.NET, Java 
    * Every language is a technology but every technology is not a language.
### Platform:
    * It can be hardware of software environment in which we can execute our application.
    * In short, platform privides services and environment to run our application.
    * There are 2 types of platform:
        1. Hardware based platform
            - Operating system( Microsoft Windows, Unix/Linux, Mac OS )
        2. Software only paltform
            - A software, that we install on operating system and which act as a virtual computer is called software only platform.
            - Microsoft .NET, Java
### Framework
    * It is a library of reusable types( interface/class/enum) that we can use to develop application. In other words it is incomplete software.
    * Example
        1. AWT, SWING, Java FX : GUI Framework
        2. JUNIT : Unit Testing Framework
        3. struts : MVC Based readymade web application framework
        4. hibernate : Automatic persistence framework
## Java History
* Java is object oriented programming language developed by James Gosling and his team in 1995 at Sun Microsystems.
* Java language is a both technology as well as platfrom.
* Java language is derived/extended from C and C++.
* Now Java is a product of Oracle.
* Latest version of Java is java 16.
* Slogan of Java is "Write Once Run Anywhere" (WORA).

## Java Buzzwords( Sun Micro System Marketing Words )
    1. Simple
    2. Object Oriented
        * 4 major pillars/parts/elements of OOPS
            1. Abstraction
            2. Encapsulation
            3. Modularity
            4. Hierachy
                * Types of hierarchies
                    1. Has-a        :   Association( It can be Composition / Aggegation )
                    2. Is-a         :   Inheritance / Generalization
                    3. use-a        :   Dependency
                    4. Creates-a    :   Instantiation
        * 3 minor pillars/parts/elements of OOPS
            1. Typing / Polymorphism
            2. Concurrency
            3. Persistence
    3. Architecture Neutral
    4. Portable
    5. Robust
    6. Dynamic
    7. Multithreaded
    8. Secure
    9. High Performance
    10. Distributed.

## Java platform
* Using Java, we can deveop different types of application and to use services and to run applicaiton we can use different types of platform provides by SUN/ORACLE.
    1. Java SE
    2. Java EE
    3. Java ME
    4. Java Card
### Java SE
    * Java Standard Edition( Java SE )
    * It is also called as Core Java.
    * If we want to develop Standalone application and to implement business logic in web application then we should use Java SE platform.
    * Components of Java SE Platform:
        1. Java Application Programming Interaface( Java API )
        2. Java Virtual Machine( JVM )

### Java EE
    * Java Enterprise Edition( Java EE / JEE )( Now it is Jakarta EE )
    * It is also called as Advaned Java / Enterprise Java / Web Java.
    * If we want to develop client server application then we should Java EE.
### Java ME
    * Java  Micro Edition( Java ME )
    * If we want to develp applications for embedded devices then we should use Java ME
    * Example : application for Set top box, mobile phone, printer, Washing machine etc.
### Java Card
    * If we want to develop application for smart card then we should use Java Card.

## Java Language Features
    1. Wrapper class
    2. PATH, Classpath
    3. Package, import, static import
    4. Array( Single dimensional, Multi dimensional, Ragged )
    5. Enumeration
    7. Inheritance( super, final, abstract, instanceof )
    8. Exception handling
    9. Generics
    10. Interface, functional interface, abstract method, default method, static method.
    11. String, StringBuffer,StringBuilder, StringTokenizer, Regular Expression
    12. Nested class, Local class, Lambda Expresion, Method reference, constructor reference
    15. File IO/NIO, Serialization, Deserialization
    16. Scoket Programming( TCP/UDP)
    17. Multithreading( Thread basics, synchronized, synchronization )
    18. Reflection and annotation
    19. Java database connectivity.
### Java 8 Features
    1. Functional Interface
    2. Default method, interface static method
    3. Lambda expresison, Method reference, constructor reference
    4. Stream and Parallel stream
    5. JODA data time API.    

## Editor
    * An application, which help us to create/edit/delete contents.
    * Example : Notepad, Edit Plus, MSVS Code, vi/vim, sublime, emacs, gedit, TextEdit etc.
## Integrated Development Environment( IDE )
* It is a tool  which is used to simplify software development process.
* IDE implicitly use editor, compiler, debugger etc.
* Python IDE:
    1. eclipse
    2. PyCharm
    3. Spider
    4. Jupyter
    5. notebook
* C/C++:
    1. Turbo C/C++
    2. Microsoft Visual Studio
    3. Code Blocks
    4. NetBeans
    5. Eclipse
* Java:
    1. eclipse
    2. IntelliJ IDEA
    3. NetBeans
    4. JCreator

## JDK, JRE, JVM
* Software development kit( SDK )
* SDK =  Developement tools + Documentation + Supporting libraries + Execution environment

* Java Development Kit( JDK )
* JDK =  Java Developement tools + Java API Docs + rt.jar + JVM
    - JDK = Java language SDK
    - It is a software that we can download fromoracle/adoptopenjdk website.
    - We must install JDK on developers machine.

* Java runtime environment( JRE )
* JDK =  Java Developement tools + Java API Docs + JRE[ rt.jar + JVM ] 
    - JRE is a software that we must install on clients machine
    - rt.jar and JVM are integral part of JRE.
* rt.jar file contains all the API's of core java( i.e JAVA SE )
*  Java virtual machine i.e JVM is abstract computer which is used to run java application.

## JDK's installation directory
* Example : 
    - MAC OS : "/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home".
    - Windows : "C:\Program Files\AdoptOpenJDK\jdk-8.0.292.10-hotspot
* Contents of installation directory:
    1. bin
        - It contains Java language development tools.
        - Example : javac, java, javap, javadoc, jdb jar, etc.
    2. include
        - In context of Java, C/C++ code is called native code.
        - Java Native Interface(JNI).
        - Java <---> JNI <---> C/C++ Code
        - JNI is a Framework that is used to access native code into Java language.
        - include directory contains header files which are required to access native code.
    3. lib
        - It contains supporting libraries which are required to execute java development tools.
    4. jre
        - It contains JVM and rt.jar
    5. src
        - Java is open source technology. In other words we can use Java software and Java source code without any cost.
        - src directory contains source code of Java API( Interace/class/enum/annotation/error/exception ).
    6. docs
        - It contains documentation of Java API
    7. man
        - it contains Documentation of Java development tools.
    8. db
        - It contains sample database files, JDBC driver etc.
### src.zip : It contains Source code(.java) of Java API
### rt.jar  : It contains compiled code(.class) of Java API
### docs    : It contains Documentation of Java API
## Simple Hello Application
```java
//File Name : Program.java
class Program{
    public static void main( String[] args ){
        System.out.println("Hello World");
        //System : It is a class declared in java.lang package
        //out : It is public static final field of System class
            //Declaration : public static final PrinStream out;
        //println : It is a non static method of java.io.PrintStream class
    }
}
//Compilation
javac Program.java  (Output : Program.class)
//Executation
java Program
//java Program.class  //Not OK
```
## Comments
* If we want to maintain documentation of source code then we should use comments.
* Single line comment 
    //Date : July 5, 2021
    //Program : Simple Hello Application
* Block / Multiline comment 
    /* Date : July 5, 2021
    Program : Simple Hello Application */
* Java documentation comment / java doc comment
    /**
        This is entry point class which contains method.
    */

## Data types
* Data type of any variable describes:
    1. Memory : How much memory is required to store the data.
    2. Nature : Which kind of data is allowed to store inside memory.
    3. Operation : Which functions/operations are allowed to perform on data.
    4. Range  : Set of values, which are allowed to store inside memory.

* Types of data type:
    1. Primitive data type( also called as value type)
    2. Non primitive data type( also called as reference type )
### Primitive data type
    1. boolean 
    2. byte    
    3. char    
    4. short   
    5. int     
    6. float   
    7. double  
    8. long    
#### Consider size and default value
    1. boolean  :   Non mentioned   =>  false   
    2. byte     :   1 byte          =>  0
    3. char     :   2 bytes         =>  \u0000
    4. short    :   2 bytes         =>  0
    5. int      :   4 bytes         =>  0
    6. float    :   4 bytes         =>  0.0f
    7. double   :   8 bytes         =>  0.0d
    8. long     :   8 bytes         =>  0L
### Non primitive data type( also called as reference type )
    1. Interface
    2. Class
    3. Array
    4. Type variable

* In Java, if we want to use any local variable(primitive/non primitive) then it is mandatory to store value inside it.
```java
class Program{
    public static void main(String[] args) { 
        //Method Local Variable
        int number; //Ok
        System.out.println(number); //error: variable number might not have been initialized
    }
}
```
* Consider following code
```java
class Program{
    public static void main(String[] args) { 
        int number = 0; 
        System.out.println(number);     //Ok
    }
}
```
## Literal
* In Java, constant value is also called as literal
```java
        true    :   boolean literal
        123     :   byte literal
        'A'     :   Character Literal
        2012    :   short literal
        45000   :   int literal
        0.0f    :   float literal
        0.0d    :   double literal
```
* We can not change value of literal.
* Java programming language is a statically type checked programming language. It means that, we can not use any variable without its declaration.
```java
public static void main(String[] args) {
    //number = 123;    // error: cannot find symbol
    int number = 123;    //OK
}
``` 
## Variable
* An entity, whose value, we can change is called variable.
```java
public static void main(String[] args) {
    String name;    //name is a variable
    int age;        //age is a variable
    float salary;   //salary is a variable
}
```
* During declaration of variable, process of storing value inside variable is called initialization.
```java
public static void main(String[] args) {
    String name = "Sandeep";    //Initialization
    int age = 38;               //Initialization
    float salary = 35000.50f;   //Initialization
}
```
* After declaration of variable, process of storing value inside variable is called assignment.
```java
public static void main(String[] args) {
    String name;
    int age;      
    float salary; 

    name = "Sandeep";       //Assignment
    age = 38;               //Assignment
    salary = 25000.50f;    //Assignment
}
```
```java
public static void main(String[] args) {
    String name = "Sandeep";    //Initialization
    int age = 38;               //Initialization
    float salary = 35000.50f;   //Initialization

    name = "Sandeep Kulange";   //Assignment
    age = 37;                   //Assignment
    salary = 32000.50f;         //Assignment
}
```
## Widening
* We can convert, value of variable of narrower type into wider type. It is called as widening.
```java
    public static void main(String[] args) {
        int num1 = 10;      //OK : Initialization
        //double num2 = ( double )num1;   //Widening : OK
        double num2 = num1;   //Widening : OK
        System.out.println("Num2    :   "+num2);    //10.0
    }
```
* In case of widening, explicit type casting is oprional.

## Narrowing
* We can convert value of variable of wider type into narrower type. It is called as narrowing conversion.
```java
public static void main(String[] args) {
    double num1 = 10.5; //OK
    int num2 = ( int )num1; //OK : Narrowing
    
    //int num2 = num1; //NOT OK:Narrowing //Eror : possible lossy conversion from double to int

    System.out.println("Num2    :   "+num2);    //10
}
```
* In case of narrowing, explicit type conversion is mandatory.

## Wrapper Class
* In Java, primitive types are not classes. But for every primitive type SUN/ORACLE  has supplied classes. It is called Wrapper class.
* All the Wrapper classes are final and it is declared in java.lang package.
1. boolean      :   java.lang.Boolean
2. byte         :   java.lang.Byte
3. char         :   java.lang.Character
4. short        :   java.lang.Short
5. int          :   java.lang.Integer
6. float        :   java.lang.Float
7. double       :   java.lang.Double
8. long         :   java.lang.Long
* Application of wrapper class:
    1. If we want to parse string then we should use Wrapper class.
        - int num1 = Integer.parseInt( "125" ); //Parsing
        - float num2 = Float.parseFloat( "3.14f " ); //Parsing
        - double num3 = Double.parseDouble( "3.142d " ); //Parsing
        - int num4 = Integer.parseInt( "1a2b5" ); //NumberFormatException
    2. If we want to store primitive values inside instance of parameterized type then type argument must be wrapper class.
        - LinkedList<int> list = new LinkedList< >( );  //Error
        - LinkedList<Integer> list = new LinkedList< >( );  //OK
* During parsing, if string does not contain parceable numeric value then parseXXX( ) method throws NumberFormatException.
```java
 public static void main(String[] args) {
        String str = "sandeep";
        int number = Integer.parseInt( str );   //NumberFormatException
        System.out.println("Number  :   "+number);
    }
```

## Boxing
* Process of converting, value of variable primitive type into non primitive type is called as boxing.
* Example 1
```java
int num1 = 10;
Integer num2 =  Integer.valueOf( num1 );    //Boxing
```
* Example 2
```java
int num1 = 10;  
String strNumber = String.valueOf(num1);    //Boxing
```
## UnBoxing
* Process of converting, value of variable of non primitive type into primitive type is called unboxing.
* Example 1
```java
String str = "123"; 
int number = Integer.parseInt( str );   //UnBoxing
```
* Example 2
```java
Integer num1  = new Integer( 10 );
int num2 = nummer.intValue( num1 ); //UnBoxing
```
* Consider String code
```java
String s1 = new String("123");  //OK

String s2 = "123" //OK
/* char[] data = new char[ ]{ '1','2','3'};
String s2 = new String( data ); */
```
## Parameter versus argument
```C
int sum( int x, int y ){    //x, y <= Function parameters / Parameters
    int result = x + y;
    return result;
}
int main( void ){
    int a = 10, b = 20, result;
    
    //Function Call
    //result = sum( 10, 20 ); //10, 20  <= function arguments / Arguments

    //Function Call
    result = sum( a, b ); //a, b   <= function arguments / Arguments
    return 0;
}
```
## Command Line argument.
* Let us consider code in C
```c
Main.exe 10 20
//argv[ 0 ] => Main.exe
//argv[ 1 ] => 10
//argv[ 2 ] => 20 
```
* Let us consider code in java
```java
javac Program.java
java Program 10, 20 [Every argument, passed from command line is considered as a string ]
//args[ 0 ] => 10
//args[ 1 ] => 20
```
## Entry Point Method
* Standardizing Java is a job of Java Community Process(JCP).
* Java Language Specification is a document which contains latested definition of Java Language.
* According to JLS main must be entry point method.
* Syntax : 
```java
1. public static void main( String args[ ] );  //Ok
2. public static void main( String[ ] args );  //OK
3. public static void main( String... args );  //OK
```
* Java compiler do not check, whether, main method is exist or not. Its responsibility of JVM.
* When we start exection of Java application then JVM starts exection of main thread and grabage collector.
    1. Main Thread
        - It is a User Thread / Non Daemon Thread
        - It is responsible for invoking main method.
    2. Garbage Collector / Finalizer
        - It is a Daemon Thread / Background Thread
        - It is responsible for releasing/deallocating memory of unused objects.
* With the help of main thread, JVM invoke main method.
* We can define main method per class. But JVM will consider only one main method as a entry point method.
* We can overload main method in Java
```java
class Program{
    public static void main( int number ){
        System.out.println("Number  :   "+number);
    }
    public static void main( String[] args ){
       System.out.println("Program.main");
       Program.main( 123 );
    } 
}
```

## Naming / Coding Conventions
### Pascal Case Coding Convention
    * Example:
        1. System
        2. ThreadGroup
        3. NumberFormatException
        4. ArrayIndexOutOfBoundsException
    * In this case, including first word, first character of each word should be in uppercase.
    * We should use this convention for
        1. Type Names( Interface, class, enum )
        2. File Name
### Camel Case Coding Convention
    * Example
        1. main
        2. parseInt
        3. showInputDialog
    * In this case, excluding first word, first character of each word should be in uppercase.
    * We should use this convention for:
        1. Method Local Variable
        2. Method parameter
        3. Field
        4. Method
        5. Object reference/reference
### Coding convention for package
    * Example
        1. java.lang.String
        2. com.mysql.cj.jdbc.Driver
        3. org.sunbeam.dbda.test.Program
    * Generally package name should be in lowercase.
### Coding convention for final and enum constant
    * Name of final variable and enum constant must be in upper case.

## Stream
* Stream is an object/instance which either produce(write) or consume(read) information from source to destination.
* Stream is always associated with physical device.
### Standard stream objects of C language associated with console.
1. stdin
2. stdout
3. stderr
### Standard stream objects of C++ language associated with console.
1. cin
2. cout
3. cerr
4. clog

### Standard stream objects of Java language associated with console.
1. System.in    -> associated with keyboard
2. System.out   -> associated with monitor
3. System.err   -> Error stream. Associated with monitor

## Scanner
* If we want to access any type(interface, class, enum ) outside package then we should use:
    1. F.Q. Type name
    2. import statement.
* Example 1: 
```java
class Program{
    public static void main(String[] args) {
        java.util.Scanner s = new java.util.Scanner( System.in );
    }
}
```
* Example 2: 
```java
import java.util.Scanner;
class Program{
    public static void main(String[] args) {
        Scanner s = new Scanner( System.in );
    }
}
```
* Methods of Scanner class:
    1. int nextInt( );
    2. float nextFloat( );
    3. double nextDouble( );
    4. String nextLine( );