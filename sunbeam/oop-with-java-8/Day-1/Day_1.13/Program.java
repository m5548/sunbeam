class Program{
    public static void main(String[] args) {
        String str = "sandeep";

        int number = Integer.parseInt( str );   //NumberFormatException

        System.out.println("Number  :   "+number);
    }
    public static void main3(String[] args) {
        String str = "3.142d";

        double number = Double.parseDouble(str);    //OK

        System.out.println("Number  :   "+number);
    }
    public static void main2(String[] args) {
        String str = "3.14f";

        float number = Float.parseFloat(str);   //OK

        System.out.println("Number  :   "+number);  
    }
    public static void main1(String[] args) {
        String str = "125";

        int number = Integer.parseInt( str );   //OK

        System.out.println("Number  :   "+number);
    }
}