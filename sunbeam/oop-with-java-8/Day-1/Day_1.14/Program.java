class Program{
    public static void main(String[] args) {
        String str = "123"; 

        int number = Integer.parseInt( str );   //UnBoxing

        System.out.println("Number  :   "+number);
    }
    public static void main2(String[] args) {
        int num1 = 10;  

        String strNumber = String.valueOf(num1);    //Boxing

        System.out.println("Number  :   "+strNumber);
    }
    public static void main1(String[] args) {
        int num1 = 10;

        Integer num2 =  Integer.valueOf( num1 );

        System.out.println( num2 ); //10
    }
}