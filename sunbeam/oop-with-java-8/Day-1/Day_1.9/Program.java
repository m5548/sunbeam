class Program{
    public static void main(String[] args) {
        String name1 = "Amit Pol";
        int empid1 = 123;
        float salary1 = 125000.50f;
        

        String name2 = "Sandeep Kulange";
        int empid2 = 45678;
        float salary2 = 85000.50f;

        System.out.printf("%-20s%-8d%-10.2f\n",name1, empid1, salary1);
        System.out.printf("%-20s%-8d%-10.2f\n",name2, empid2, salary2);
    }
}