class Program{
    public static void main(String[] args) {
        int num1 = 10;      //OK : Initialization
        //double num2 = ( double )num1;   //Widening : OK
        double num2 = num1;   //Widening : OK
        System.out.println("Num2    :   "+num2);    //10.0
    }
    public static void main1(String[] args) {
        int num1 = 10;      //OK : Initialization
        int num2 = num1;    //OK : Initialization
        System.out.println("Num2    :   "+num2);
    }
}