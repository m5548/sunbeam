class Program{
    public static void main(String[] args) {
        int num1 = Integer.parseInt( args[ 0 ] );   //UnBoxing
        float num2 = Float.parseFloat( args[ 1 ] );   //UnBoxing
        double num3 = Double.parseDouble( args[ 2 ] );   //UnBoxing

        double result = num1 + num2 + num3;
        System.out.println("Result  :   "+result);
    }
    public static void main2(String[] args) {
        int num1 = Integer.parseInt( args[ 0 ] );   //UnBoxing
        int num2 = Integer.parseInt( args[ 1 ] );   //UnBoxing
        int result = num1 + num2;
        System.out.println("Result  :   "+result);
    }
    public static void main1(String[] args) {
        String name = args[ 0 ];
        System.out.println("Hello,"+name);
    }
}