package org.sunbeam.dbda.test;

import java.util.Arrays;

public class Program {
	public static void main(String[] args) {
		int[] arr = new int[  ] { 70, 10, 60, 20, 50, 40, 30 };
		System.out.println( Arrays.toString(arr));
		
		//Arrays.sort(arr);
		Arrays.parallelSort(arr);
		System.out.println( Arrays.toString(arr));	
	}
}
