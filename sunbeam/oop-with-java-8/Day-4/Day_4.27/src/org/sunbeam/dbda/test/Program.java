package org.sunbeam.dbda.test;

import java.util.Scanner;

public class Program {
	private static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(int[] arr) {
		for( int index = 0; index < arr.length; ++ index ) {
			System.out.print("Enter element	:	");
			arr[ index ] = sc.nextInt();
		}
	}
	private static void printRecord(int[] arr) {
		for( int index = 0; index < arr.length; ++ index )
			System.out.println(arr[ index ]);
		System.out.println();
	}
	public static void main(String[] args) {
		int[] arr1 = new int[ 3 ];
		Program.acceptRecord( arr1 );
		
		int[] arr2 = new int[ 5 ];
		Program.acceptRecord( arr2 );
		
		int[] arr3 = new int[ 7 ];
		Program.acceptRecord( arr3 );
		
		Program.printRecord( arr1 );
		Program.printRecord( arr2 );
		Program.printRecord( arr3 );
	}
}
