package org.sunbeam.dbda.test;
class Test{
	public static final int number = 10;
	
	public void showRecord( ) {
		System.out.println("Number	:	"+Test.number);
	}
	public void printRecord( ) {
		System.out.println("Number	:	"+Test.number);
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = new Test( );
		
		t.showRecord();
		
		t.printRecord();
	}
}
