class Program{
    //Local variable get space once per method call
    public static void print( ){
        int count = 0;  //Method Local Variable
        count = count + 1;
        System.out.println("Count   :   "+count);   //1
    }
    public static void main(String[] args) {
        Program.print( );   //1
        Program.print( );   //1
        Program.print( );   //1
    }
}