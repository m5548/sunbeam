package org.sunbeam.dbda.test;
public class Program {
	private static void showRecord(int num1) {
		System.out.println("int	:	"+num1);
	}
	private static void displayRecord(double num2) {
		System.out.println("double	:	"+num2);
	}
	public static void main(String[] args) {
		int num1 = 10;
		Program.showRecord( num1 );
		
		double num2 = 20.5d;
		Program.displayRecord( num2 );
		
	}
}
