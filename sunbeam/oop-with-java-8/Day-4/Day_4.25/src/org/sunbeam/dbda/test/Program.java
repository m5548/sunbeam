package org.sunbeam.dbda.test;
public class Program {
	public static void main(String[] args) {
		System.out.println( 3 < 3 );	//false
		System.out.println( 3 > 3 );	//false
		
		System.out.println( 3 <= 3 );	//true
		System.out.println( 3 >= 3 );	//true
	}
	public static void main4(String[] args) {
		int[] arr = new int[ 3 ];
		for( int index = 0; index < 3;++ index) {
			System.out.println(arr[ index ] );
		}
	}
	public static void main3(String[] args) {
		int[] arr = new int[ 3 ];
		int index = 0; //Initialization
		while( index < 3 ) {	//Entry controlled loop
			System.out.println( arr[ index ] );
			//index = index + 1; //Increment Statement
			//index += 1;//Increment Statement
			//index ++;//Increment Statement
			++ index;//Increment Statement
		}
	}
	public static void main2(String[] args) {
		int[] arr = new int[ 3 ];

		//Exit controlled loop
		int index = 0;	//Initialization
		do {
			System.out.println( arr[ index ] );
			index = index + 1; //Increment Statement
		}while( index < 3 );	//Condition
	}
	public static void main1(String[] args) {
		int[] arr = new int[ 3 ];
		System.out.println( arr[ 0 ] );
		System.out.println( arr[ 1 ] );
		System.out.println( arr[ 2 ] );
	}
}
