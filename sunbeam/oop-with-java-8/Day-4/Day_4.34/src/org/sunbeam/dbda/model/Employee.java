package org.sunbeam.dbda.model;

public class Employee {
	private String name;
	private int empid;
	private float salary;
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	public String getName() {
		return this.name;
	}
	public int getEmpid() {
		return this.empid;
	}
	public float getSalary() {
		return this.salary;
	}
}
