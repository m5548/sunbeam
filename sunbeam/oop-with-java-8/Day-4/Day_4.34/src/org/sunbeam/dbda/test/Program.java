package org.sunbeam.dbda.test;
import java.util.Arrays;
import java.util.Date;

import org.sunbeam.dbda.model.Employee;

public class Program {
	private static Employee[] getEmployees() {
		Employee[] arr = new Employee[ 5 ];
		arr[ 0 ] = new Employee("Rahul Kale", 11, 45000);
		arr[ 1 ] = new Employee("Sandeep Kulange", 12, 47000);
		arr[ 2 ] = new Employee("Nilesh Ghule", 13, 48000);
		arr[ 3 ] = new Employee("Amit Kulkarni", 14, 50000);
		arr[ 4 ] = new Employee("Sachin Pawar", 15, 46000);
		return arr;
	}
	private static void printRecord(Employee[] arr) {
		for( int index = 0; index < arr.length; ++ index ) {
			System.out.println(arr[ index ].getName()+" "+arr[ index ].getEmpid()+" "+arr[ index ].getSalary());
		}
	}
	public static void main(String[] args) {
		Employee[] arr =  Program.getEmployees( );
		
		Program.printRecord( arr );
	}
	
	
	public static void main1(String[] args) {
		Employee[] arr = new Employee[ 5 ];
		arr[ 0 ] = new Employee("Rahul Kale", 11, 45000);
		arr[ 1 ] = new Employee("Sandeep Kulange", 12, 47000);
		arr[ 2 ] = new Employee("Nilesh Ghule", 13, 48000);
		arr[ 3 ] = new Employee("Amit Kulkarni", 14, 50000);
		arr[ 4 ] = new Employee("Sachin Pawar", 15, 46000);
		
		for( int index = 0; index < arr.length; ++ index ) {
			System.out.println(arr[ index ].getName()+" "+arr[ index ].getEmpid()+" "+arr[ index ].getSalary());
		}
	}
}
