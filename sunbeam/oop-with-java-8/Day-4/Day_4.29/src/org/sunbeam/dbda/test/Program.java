package org.sunbeam.dbda.test;

import java.util.Arrays;

public class Program {
	public static void main(String[] args) {
		int[] arr = new int[ 3 ];
		System.out.println( Arrays.toString(arr));	//[0, 0, 0]
	}
	public static void main2(String[] args) {
		int[] arr = new int[ 3 ];
		String str = Arrays.toString(arr);
		System.out.println(str);
	}
	public static void main1(String[] args) {
		boolean[] arr1 = new boolean[ 3 ];
		
		byte[] arr2 = new byte[ 3 ];
		
		char[] arr3 = new char[ 3 ];
	
		int[] arr4 = new int[ 3 ];
		
		float[] arr5 = new float[ 3 ];
		
		double[] arr6 = new double[ 3 ];
		
		System.out.println(arr1.toString());	//[Z@6d06d69c
		System.out.println(arr2.toString());	//[B@7852e922
		System.out.println(arr3.toString());	//[C@4e25154f
		System.out.println(arr4.toString());	//[I@70dea4e
		System.out.println(arr5.toString());	//[F@5c647e05
		System.out.println(arr6.toString());	//[D@33909752
	}
}
