package org.sunbeam.dbda.test;
public class Program {
	private static int sum(int num1, int num2) {	//no of parameters : 2
		return num1 + num2;
	}
	private static double sum(int num1, double num2) {	//no of parameters : 2
		return num1 + num2 ;
	}
	public static void main(String[] args) {
		
		
		int result1 = Program.sum( 10, 20 );
		System.out.println("Result	:	"+result1);
		
		double result2 = Program.sum( 10, 20.5d  );
		System.out.println("Result	:	"+result2);
	}
}
