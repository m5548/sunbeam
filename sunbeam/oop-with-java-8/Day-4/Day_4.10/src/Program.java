class Program {
    public static double PI = 3.142d;   //OK
    public static double pow( double base, int index ){
        double result = 1;
        for( int count = 1; count <= index; count++ )
            result = result * base;
        return result;
    }
    public static void main(String[] args) {
        double radius = 10;   
        double area = PI * pow(radius, 2);
        System.out.println("Area    :   "+area);
    }
    public static void main2(String[] args) {
        double radius = 10;   
        double area = Program.PI * Program.pow(radius, 2);
        System.out.println("Area    :   "+area);
    }
    public static void main1(String[] args) {
        double radius = 10;   
        double area = 3.142d * radius * radius;
        System.out.println("Area    :   "+area);
    }    
}
