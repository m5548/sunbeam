package org.sunbeam.dbda.test;

import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.print("Enter size	:	");
		int size = sc.nextInt();
		int[] arr = new int[ size ];
	}
	public static void main4(String[] args) {
		int[] arr = new int[ -3 ]; //NegativeArraySizeException
	}
	public static void main3(String[] args) {
		//int arr[ ] = new int[ 3 ];
		int[] arr = new int[ 3 ];
	}
	public static void main2(String[] args) {
		int[] arr = null;	//arr is array reference
		arr = new int[ 3 ];	//array instance
	}
	public static void main1(String[] args) {
		//new int[ 3 ];	//Integer array
		
		int arr1[ ] = null;	//OK
		//int [ arr2 ] = null;	//Not OK
		int[ ] arr3 = null;	//OK
	}
}
