package org.sunbeam.dbda.test;
public class Program {
	public static void main(String[] args) {
		int[] arr = new int[  ] { 10, 20, 30 };	//OK
		
		//for each element in arr
		for( int element : arr ) {	//For Each loop / Iterator( Forward & read only )
			System.out.println(element);
		}
		
		/* for( int index = 0; index < arr.length; ++ index )
			System.out.print(arr[ index ]+"	");
		System.out.println(); */
		
	}
	public static void main1(String[] args) {
		//int[] arr = new int[  ];	//NOT OK
		//int[] arr = new int[ 3 ];	//OK
		//int[] arr = new int[ 3 ] { 10, 20, 30 };	//Not OK
		//int[] arr = new int[  ] { 10, 20, 30 };	//OK
		int[] arr = { 10, 20, 30 };	//OK
	}
}
