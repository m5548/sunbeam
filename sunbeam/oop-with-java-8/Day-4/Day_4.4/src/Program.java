
//Creational Design Pattern : Singleton
class Singleton{//Step 1
    //Step 2
    private Singleton( ){
        System.out.println("Inside constructor");
    }
    //Step 5
    private static Singleton reference = null;

    //Step 3
    public static Singleton getReference( ){
        //Step 6
        if( reference == null )
            reference = new Singleton( );
        return reference;
    }
}

class Program{
    public static void main(String[] args) {
        //Step 4
        Singleton s1 = Singleton.getReference( );

        Singleton s2 = Singleton.getReference( );

        Singleton s3 = Singleton.getReference( );
    }
}