package org.sunbeam.dbda.test;

import java.util.Arrays;

public class Program {
	public static void main(String[] args) {
		int[] arr = new int[  ] { 10, 20, 30, 40, 50 };
		//int element = arr[ -1 ]; //ArrayIndexOutOfBoundsException
		//int element = arr[ arr.length ]; //ArrayIndexOutOfBoundsException
		int element = arr[ arr.length - 1 ]; //50
		System.out.println("Element	:	"+element);
		
	}
}
