package org.sunbeam.dbda.test;
public class Program {
	private static int sum(int num1, int num2) {	
		return num1 + num2;
	}
	public static void sum( int num1, int num2 ) {
		int result = num1 + num2;
		System.out.println("Result	:	"+result);
	}
	public static void main(String[] args) {
		
		int result = 0;
		
		result = Program.sum( 10, 20 );
		System.out.println("Result	:	"+result);
		
		Program.sum( 50, 60 );
		
	}
}
