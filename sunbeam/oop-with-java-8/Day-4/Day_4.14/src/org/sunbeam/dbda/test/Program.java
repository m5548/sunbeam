package org.sunbeam.dbda.test;

import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		final int number = 10;	//Initialization : OK
		//number = 20;	//Assignment : NOT OK
		System.out.println("Number	:	"+number);		//OK : 10
	}
	public static void main6(String[] args) {
		final int number;
		number = 10;	//Assignment : OK
		//number = number + 5;	//Not OK :  The final local variable number cannot be assigned.
		System.out.println("Number	:	"+number);		//OK : 10
	}
	public static void main5(String[] args) {
		System.out.print("Number	:	");
		final int number = sc.nextInt();	
		//number = number + 5;	//Not OK :  The final local variable number cannot be assigned.
		System.out.println("Number	:	"+number);		//OK : 10
	}
	public static void main4(String[] args) {
		final int number = 10;	//Initialization
		//number = number + 5;	//Not OK :  The final local variable number cannot be assigned.
		System.out.println("Number	:	"+number);		//OK : 10
	}
	public static void main3(String[] args) {
		int number = 10;	//Initialization
		number = number + 5;	//OK
		System.out.println("Number	:	"+number);		//OK : 15
	}
	public static void main2(String[] args) {
		int number = 10;
		System.out.println("Number	:	"+number);		//OK : 10
	}
	public static void main1(String[] args) {
		int number;	//OK
		//System.out.println("Number	:	"+number);	//The local variable number may not have been initialized
	}
}
