class Program{
    //Local variable get space once per method call
    public static void print( ){
        static int count = 0;  //error: illegal start of expression
        count = count + 1;
        System.out.println("Count   :   "+count);   //1
    }
    public static void main(String[] args) {
        Program.print( );   //1
        Program.print( );   //1
        Program.print( );   //1
    }
}