class Program{
    public static void main(String[] args) {
        Test t1 = new Test( 10, 20 );
        int num1 = t1.getNum1( );
        System.out.println("Num1    :   "+num1);
        int num2 = t1.getNum2( );
        System.out.println("Num2    :   "+num2);
        int num3 = t1.getNum3( );
        System.out.println("Num3    :   "+num3);
    }
    public static void main2(String[] args) {
        Test t1 = new Test( 10, 20 );
        //t1.num1 = 50;   //Not OK
        t1.setNum1( 50 ) ;  //setNum1 method is called on t1 to set value 50.
        t1.setNum2( 60 ) ;  //setNum2 method is called on t1 to set value 60.
        t1.printRecord(  );
    }
    public static void main1(String[] args) {
        Test t1 = new Test( 10, 20 );

        Test t2 = new Test( 30, 40 );

        Test t3 = new Test( 50, 60 );

        t1.printRecord(  );
        t2.printRecord(  );
        t3.printRecord(  );
    }
}