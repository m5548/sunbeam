class Test {
    private int num1;
    private int num2;
    private int num3;
    public Test( ) {
        this.num3 = 500;
    }
    public Test( int num1, int num2 ) {
        this.num1 = num1;
        this.num2 = num2; 
        this.num3 = 500;
    }
    //Inspector / Selector / Getter
    public int getNum1( ){
        return this.num1;
    }
    public int getNum2( ){
        return this.num2;
    }
    public int getNum3( ){
        return this.num3;
    }
    //Mutator / Modifier / Setter methods
    public void setNum1( int num1 ){
        this.num1 = num1;
    }
    public void setNum2( int num2 ){
        this.num2 = num2;
    }
    public void setNum3( int num3 ){
        this.num3 = num3;
    }
    public void printRecord( ){
        System.out.println("Num1    :   "+this.num1);
        System.out.println("Num2    :   "+this.num2);
        System.out.println("Num3    :   "+this.num3);
        System.out.println();
    }
}
