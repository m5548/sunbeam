import java.time.LocalDate;
class Date{
    private int day;
    private int month;
    private int year;
    public Date( ){
        LocalDate date = LocalDate.now();
        this.day = date.getDayOfMonth( );
        this.month = date.getMonthValue();
        this.year = date.getYear(); 
    }
    public Date( int day, int month, int year ){
        this.day = day;
        this.month = month;
        this.year = year;
    }
    //Date this = joinDate
    public void printRecord( /* Date this */){
        System.out.println(this.day+" / "+this.month+" / "+this.year);
    }
}
class Employee{
    private int empid;      //Field -> get space inside employee instance -> on heap
    private Date joinDate;  //Field -> get space inside employee instance -> on heap
}
class Program{
    public static void main(String[] args) {
       int empid = 33;  //Method local variable -> get space on Java Stack.
        Employee emp = null;    //Local reference variable -> get space on Java Stack
        emp = new Employee();   //Instance -> gets space on heap;
    }
    public static void main2(String[] args) {
        Date joinDate;   //Method Local Object reference / reference -> gets space on Java Stack
        joinDate = new Date( 26,12,2006);  //Instance -> gets space on Heap
        joinDate.printRecord( ); //joinDate.printRecord( joinDate );
     }
    public static void main1(String[] args) {
       Date joinDate;   //Method Local Object reference / reference -> gets space on Java Stack
       joinDate = new Date( );  //Instance -> gets space on Heap
       joinDate.printRecord( ); //joinDate.printRecord( joinDate );
    }
}