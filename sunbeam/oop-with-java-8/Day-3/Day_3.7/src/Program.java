class Program{
    public static void main(String[] args) {
        Employee emp1 = new Employee("Rahul", 11, 25000.50f);  //OK

        Employee emp2 = new Employee("Akash", 12, 17000.00f);  //OK

        Employee emp3 = new Employee("Ketan", 13, 20000.50f);  //OK

        emp1.printRecord( );    //emp1.printRecord( emp1 );
        emp2.printRecord( );    //emp2.printRecord( emp2 );
        emp3.printRecord( );    //emp3.printRecord( emp3 );
    }
}