# Day 3
## Value Type Versus Referece Type
### Value Type
1. Primitive type is also called as value type.
2. There are 8 value types in Java(boolean, byte, char, short, int, float, double, long).
3. Variable of value type contains value.
```java
int number = 10;
```
4. In case of copy operation, value gets copied into another variable.
```java
int num1 = 10;
int num2 = num1;    //Value copy operation
```
5. In case of field, default value of variable value type is 0.
```java
class Test{
    boolean status; //false
    int num1;       //0
    double num2;    //0.0
}
```
6. We can not use new operator to create object of value type.
```java
int num1 = new int( 10 );   //Not Ok
int num1 = 10;  //OK
```
7. Local variable of value type get space on Java Stack and Field of value type get space inside instance on heap.
8. We can not store null value inside variable of value type.
```java
int number = null;  //NOT OK
```
### Referece Type
1. Non primitive type is also called as reference type.
2. There are 4 reference types in Java(interface, class, array, type variable )
3. Variable of reference type contains reference.
```java
Integer number = new Integer( 10 );
```
4. In case of copy operation, reference gets  copied into another reference variable
```java
Integer n1 = new Integer( 10 );
Integer n2 = n1;    //reference copy operation
```
5. In case of field, default value of variable of reference type is null.
```java
class Date{ }
class Employee{ 
    private Date joinDate;  //null
}
```
6.To create instance of reference type, it is mandatory to use new operator.
```java
//Employee emp("ABC",23,15000);   //Not Ok
Employee emp = new Employee("ABC",23,15000);    //OK
```
7. Instance of reference type always get space on heap.
8. We can store null value inside variable of reference type.
```java
Employee emp = null;    //OK
```

## System Date
### Using java.util.Date class
```java
 public static void main(String[] args) {
    Date date = new Date( );
    int day = date.getDate( );
    int month = date.getMonth() + 1;
    int year = date.getYear() + 1900;
    System.out.println(day+" / "+month+" / "+year);
}
```
### Using java.util.Calendar
```java
public static void main(String[] args) {
    //Calendar  c = new Calendar( );  //Not OK
    Calendar  c = Calendar.getInstance();
    int day = c.get( Calendar.DAY_OF_MONTH );
    int month = c.get( Calendar.MONTH ) + 1;
    int year = c.get( Calendar.YEAR );
    System.out.println(day+" / "+month+" / "+year);
}
```
### Using java.time.LocalDate
```java
public static void main(String[] args) {
    LocalDate ldt =  LocalDate.now();
    int day = ldt.getDayOfMonth();
    int month = ldt.getMonthValue();
    int year = ldt.getYear( );
    System.out.println(day+" / "+month+" / "+year);
}
```

## Object oriented concepts
* If we declare fields inside class then it gets space once per instance according to their order of declaration inside class.
* Method do not get space inside instance. Rather all the instances of same class share single copy of the method. By passing reference of instance, Instances can share method.
### Characteristics of instance
1. State:
    * Value stored inside instance is called state of the instance.
    * Value of the field represent state of the instance.
2. Behavior
    * Set of operations which is allowed to perform on instance is called behavior of the instance.
    * Methods defined inside class represents behavior of the instance.
3. Identity
    * It is proprty of an instance which is used to indentfy instance uniquely.
    * Value of field can be used to indentfy instance uniquely. e.g empid.

* Variable declared inside class is called field(also called as property/attribute).
* Function implemeted inside class is called method( also called operation/behavior/message).
### class
* Definition
    1. Class is collection of fields and methods.
    2. Structure and behavior of instance is depends on class( i.e fields and methods ) hence class is considered as as a template/model/blueprint for an instance.
    3. Class represents set/group of objects which is having common structure and common behavior.
* Class is logical/imaginary/virtual entity.
* Example : Book, Car, Mobile Phone etc.
* Class implementation represents encapsulation.
### Instance
* Definition
    1. An enity which is having physical existance is called instance.
    2. In Java, object is also called as instance.
    3. An enity which is having state,behavior and identity is called instance.
* Instance is physical/real time entity.
* Example : "Core Java Vol-I", "Tata nano", "iPhone12" etc.
* Process of creating instance from class is called instantiation. It represents abstraction.

## Absolute Path
* A path of file/directory from root directory is called as absolute path. 
* Example: D:\Cdac\SunBeam\Java\Day3\Day_3.6\src\Program.java
## Relative Path
* A path of a file or directory from current directory is called as relative path.
* Example: .\src\Program.java
## Path
* It is environment variable of OS platform which is used to locate development tools.
* How to set path?
```java
set path="C:\Program Files\AdoptOpenJDK\jdk-8.0.292.10-hotspot\bin";
javac Program.java
java Program
```
## path separator character
* Windows( \ )
* Linux and Mac OS( / )
## Classpath
* It is environment variable of Java platform which is used to locate .class file and .jar file.
* By default classpath is set to current working directory. Hence application classloader by default check current working dircetoy.
```java
set classpath=.\bin;
```

## Static Field
* A field of a class which get space inside instance is called instance variable. In short, non static field declared inside class is called instance variable.
* Method of a class which is designed to call in instance( actually objec reference ) is called instance method. In short, non static method declared inside class is called instance method.
* Instance members = { instance variable + instance method };
* We can not access, instance members using class name. To access instance members, we must use object reference.

* If we want to share, value of any field, in all the instances of same class then we should declare that field static.
* static is modifier in Java.
* If we declare any field static then it doesn't get space inside instance. Rather all the instances of same class share single copy it. Hence size of instance depends on size of all the non static fields declared inside class.
* Field of a class which do not get space inside instance is called class level variable. In short, static field delcared inside class is called class level-variable.
* We can access, class level variable using instance but it is designed to access using classname and dot operator.
* Inside non static method( instance method ), we can access static as well as non static members.
* Non static field / instance variable get space once per instance on heap section.
* Static field get space once per class during class loading on method area.
```java
class A{
    int num1;
    int num2;
    static int temp;
}
A a1 = new A( );
A a2 = new A( );
A a3 = new A( );
```
```java
class B{
    int num3;
    int num4;
    static int temp;
}
B b1 = new B( );
B b2 = new B( );
B b3 = new B( );
```
```java
class C{
    int num5;
    int num6;
    static int temp;
}
C c1 = new C( );
C c2 = new C( );
C c3 = new C( );
```
## Static initialization block
* If we want to initialize static fields of the class then we should define static block inside class.
* JVM execute static initialization block during class loading.
* We can define multiple static blocks inside class. In this case, JVM executes it sequentially.
* We can not invoke, static initialization block from another static initialization block.
* Syntax:
```java
class Test{
    private int num1;           //Instance Variable
    private int num2;           //Instance Variable
    private static int num3;    //Class Level Variable

    static{//Static Initialization Block
        Test.num3 = 500;
    }
}
```
## Static method
* To access non static members of the class, method should be non static and to access static members of the class, method should static.
* Non static method is designed to call on instance. Hence it is also called as instance method.
* Static method is designed to call on class name. Hence it is also called as class level method.{
* Instance Members = { Instance Variable + Instance Method }
* Class Level Members = { Class Level Variable + Class Level Method }
* Inside non static method, we can use/access static as well as non static members.
* Inside static method, we can not use non static members. In other words, inside static method we can use only static members of the class.
### Why static method do not get this reference?
    1. If we call non static method on instance then method get this reference.
    2. Static method is designed to call on class name.
    3. Since static method is not designed to call on instance hence it doesnt get this reference.
* Since static method, do not get this reference, we can not access non static members inside static method directly.
* Using instance, we can use non static members inside static method.
```java
class Program {
    public int num1 = 10;
    public static int num2 = 20;
    public static void main(String[] args) {
        //System.out.println("Num1    :   "+num1);    //Not OK
        Program p = new Program( );
        System.out.println("Num1    :   "+p.num1);    //OK
        System.out.println("Num2    :   "+num2);    //OK
    }    
}
```
* Inside a method, if there is need to use this reference, then method should be non static otherwise it should be static.
### Instance Counter
```java
class InstanceCounter{
    private static int count;
    public InstanceCounter( ){
        ++ InstanceCounter.count;
    }
    public static int getCount( ){
        return InstanceCounter.count;
    }
}
class Program {
    public static void main(String[] args) {
        InstanceCounter c1 = new InstanceCounter( );
        InstanceCounter c2 = new InstanceCounter( );
        InstanceCounter c3 = new InstanceCounter( );

        System.out.println("Count   :   "+InstanceCounter.getCount());
    }    
}
```
* If constructor is public then we can create instance of a class inside method of same class 
as well as method of different class.
* If constructor is private then we can create instance of a class inside method of same class only.
### What is singleton class
* A class from which we can create only once instance is called singleton class.
