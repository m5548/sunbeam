import java.util.Scanner;

class Employee{
    private String name;
    private int empid;
    private float salary;
    public Employee( ){
    }
    public void acceptRecord( ){
        Scanner sc = new Scanner( System.in);
        System.out.print("Name  :   ");
        this.name = sc.nextLine();
        System.out.print("Empid  :   ");
        this.empid = sc.nextInt();
        System.out.print("Salary  :   ");
        this.salary = sc.nextFloat();
    }
    public void printRecord( ){
        System.out.println(this.name+"  "+this.empid+"  "+this.salary);
    }
    public static int menuList( ){
        Scanner sc = new Scanner( System.in);
        System.out.println("0.Exit");
        System.out.println("1.Accept Record");
        System.out.println("2.Print Record");
        System.out.print("Enter choice  :   ");
        return sc.nextInt();
        /* int choice = sc.nextInt();
        return choice; */
    }
}
class Program {
    public static void main(String[] args) {
        int choice;
        Employee emp = new Employee();  //OK;
        while ( ( choice = Employee.menuList( ) ) != 0 ){
            switch( choice ){
            case 1:
                emp.acceptRecord( );
                break;
            case 2:
                emp.printRecord( );
                break;
            }
        }
    }    
}