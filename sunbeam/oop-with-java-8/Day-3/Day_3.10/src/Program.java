class Test{
    private int num1;           //Instance Variable
    private int num2;           //Instance Variable
    private static int num3;    //Class Level Variable
    public void setNum1(/* Test this,*/ int num1 ){
        this.num1 = num1;
    }
    public void setNum2(/* Test this,*/  int num2 ){
        this.num2 = num2;
    }
    public static void setNum3( int num3 ){
        Test.num3 = num3;
    }
    public void printRecord( ){
        System.out.println("Num1    :   "+this.num1);
        System.out.println("Num2    :   "+this.num2);
        System.out.println("Num3    :   "+Test.num3);  
    }
}
class Program {
    public static void main(String[] args) {
        Test t1 = new Test( );
        t1.setNum1( 50 );
        t1.setNum2( 60 );
        Test.setNum3(70);
        t1.printRecord(  );
    }    
}