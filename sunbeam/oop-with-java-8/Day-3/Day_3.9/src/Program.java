class Test{
    private int num1;           //Instance Variable
    private int num2;           //Instance Variable
    private static int num3;    //Class Level Variable

    static{//Static Initialization Block
        System.out.println("Inside static block of Test class");
        Test.num3 = 500;
    }
    public Test( int num1, int num2 ){
        this.num1 = num1;
        this.num2 = num2;
    }
    
    public void printRecord( ){
        System.out.println("Num1    :   "+this.num1);
        System.out.println("Num2    :   "+this.num2);
        System.out.println("Num3    :   "+Test.num3);   //OK : recommended
        //System.out.println("Num3    :   "+this.num3);   //OK : Not recommended
        System.out.println();
    }
   
}
class Program {
    static{
        System.out.println("Inside static block of Program class");
    }
    public static void main(String[] args) throws Exception{
        System.out.println("Inside main method");

        System.out.print("Press any key to continue....");
        System.in.read();

        Test t1 = new Test( 10, 20);
        Test t2 = new Test( 30, 40);
        Test t3 = new Test( 50, 60);

        t1.printRecord(  );
        t2.printRecord(  );
        t3.printRecord(  );
    }    
}