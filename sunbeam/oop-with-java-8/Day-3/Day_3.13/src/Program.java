
class B{
    public static void f2( ){
        System.out.println("Inside B.f2()");
    }
    public static void f3( ){
        f2(  ); //OK
        B.f2(  );//OK
    }   
}
class Program {
    public static void main(String[] args) {
        //f3( );  //Not OK
        B.f3( ); //OK
    }    
}