import java.util.Scanner;

class Employee{
    private String name;
    private int empid;
    private float salary;
    public Employee( ){
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getEmpid() {
        return empid;
    }
    public void setEmpid(int empid) {
        this.empid = empid;
    }
    public float getSalary() {
        return salary;
    }
    public void setSalary(float salary) {
        this.salary = salary;
    }
}
class Program {
    static Scanner sc = new Scanner( System.in);
    public static void acceptRecord( Employee emp ){
       System.out.print("Name   :   ");
       sc.nextLine( );
       emp.setName(sc.nextLine());
       //String name = sc.nextLine( );
       //emp.setName(name);

       System.out.print("Empid   :   ");
       emp.setEmpid(sc.nextInt());
       //int empid = sc.nextInt();
       //emp.setEmpid(empid);

       System.out.print("Salary   :   ");
       emp.setSalary(sc.nextFloat());
       //float salary = sc.nextFloat();
       //emp.setSalary(salary);
    }
    public static void printRecord( Employee emp ){
        System.out.println(emp.getName()+"  "+emp.getEmpid()+"  "+emp.getSalary());
    }
    public static int menuList( ){
        System.out.println("0.Exit");
        System.out.println("1.Accept Record");
        System.out.println("2.Print Record");
        System.out.print("Enter choice  :   ");
        return sc.nextInt();
    }
    public static void main(String[] args) {
        int choice;
        Employee emp = new Employee();  //OK;
        while ( ( choice = Program.menuList( ) ) != 0 ){
            switch( choice ){
            case 1:
                Program.acceptRecord( emp );
                break;
            case 2:
                Program.printRecord( emp );
                break;
            }
        }
    }    
}