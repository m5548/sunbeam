package org.sunbeam.dbda.thread.test;

import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Stream;

public class Program {
	public static void main(String[] args) {
		Stream.of(11,18,35,83,110, 45, 23,78 )
		.sorted()
		.forEach(System.out::println);
		
	}
	public static void main2(String[] args) {
		Stream<Integer> stream = Stream.of(11,18,35,83,110, 45, 23,78 );
		Optional<Integer> max = stream.reduce(Integer::min);
		if( max.isPresent())
			System.out.println(max.get());
	}
	public static void main1(String[] args) {
		Stream<Integer> stream = Stream.of(11,18,35,83,110, 45, 23,78 );
		Integer max = stream.reduce(0, Integer::max);
		System.out.println(max);
	}
}
