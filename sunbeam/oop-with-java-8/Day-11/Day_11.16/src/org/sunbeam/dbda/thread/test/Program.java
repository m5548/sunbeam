package org.sunbeam.dbda.thread.test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Program {
	public static void main(String[] args) {
		List<Double> list = Arrays.asList(10.1, 20.2, 30.3, 40.4, 50.5 );
		Stream<Double> stream = list.stream();
	}
	public static void main2(String[] args) {
		int[] arr = new int[ ] { 10, 20, 30, 40, 50 };
		IntStream stream = Arrays.stream(arr);
	}
	public static void main1(String[] args)throws Exception{
		//Build Stream from strings
		Stream<String> strStream = Stream.of("DAC", "DMC","DESD","DBDA");
		
		Stream<Integer> intStream = Stream.of(10, 20, 30, 40, 50 );
	}
}
