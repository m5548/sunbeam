package org.sunbeam.dbda.thread.test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Program {
	public static void main(String[] args) {
		List<Integer> list = Stream.of(11, 12, 13, 14, 15, 13, 11, 15, 16, 17, 18, 19, 20, 19, 17)
							.filter( e -> e % 2 != 0 )
							.distinct()
							.skip(2)
							.limit(2)
							.collect(Collectors.toList());
		list.forEach(System.out::println);
	}
	public static void main9(String[] args) {
		List<Integer> list = Stream.of(11, 12, 13, 14, 15, 13, 11, 15, 16, 17, 18, 19, 20, 19, 17)
							.filter( e -> e % 2 != 0 )
							.distinct()
							.skip(3)
							.collect(Collectors.toList());
		list.forEach(System.out::println);
	}
	public static void main8(String[] args) {

		List<Integer> list = Stream.of(11, 12, 13, 14, 15, 13, 11, 15, 16, 17, 18, 19, 20, 19, 17)
							.filter( e -> e % 2 != 0 )
							.distinct()
							.limit(3)
							.collect(Collectors.toList());
		list.forEach(System.out::println);
	}
	public static void main7(String[] args) {

		List<Integer> list = Stream.of(11, 12, 13, 14, 15, 13, 11, 15, 16, 17, 18, 19, 20, 19, 17)
							.filter( e -> e % 2 != 0 )
							.distinct()
							.collect(Collectors.toList());
		list.forEach(System.out::println);
	}
	public static void main6(String[] args) {
		//Stream<Integer> stream = Stream.of(11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		// Predicate<T> - >boolean test(T t)	:	Predicate<Integer> p =	n -> n % 2 != 0
		List<Integer> list = Stream.of(11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
							.filter( e -> e % 2 != 0 )
							.collect(Collectors.toList());
		list.forEach(System.out::println);
	}
	public static void main5(String[] args) {
		Stream<Integer> stream = Stream.of(11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		stream.collect(Collectors.toList()).forEach(System.out::println);;		
	}
	public static void main4(String[] args) {
		Stream<Integer> stream = Stream.of(11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		List<Integer> integerList = stream.collect(Collectors.toList());
		integerList.forEach(System.out::println);
	}
	public static void main3(String[] args) {
		Stream<Integer> stream = Stream.of(11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		stream.forEach(System.out::println);
	}
	public static void main2(String[] args) {
		Stream<Integer> stream = Stream.of(11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		System.out.println("Count	:	"+stream.count());
	}
	public static void main1(String[] args) {
		Stream<Integer> stream = Stream.of(11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		long count = stream.count();	//Returns the count of elements in this stream.
		System.out.println("Count	:	"+count);
		System.out.println("Count	:	"+stream.count());//Exception in thread "main" java.lang.IllegalStateException: stream has already been operated upon or closed
	}
}
