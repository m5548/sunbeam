package org.sunbeam.dbda.thread.test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class Dish {
	public enum Type {//Nested Type( enum )
		MEAT, FISH, OTHER 
	}	
    private final String name;
    private final boolean vegetarian;
    private final int calories;
    private final Type type;

    public Dish(String name, boolean vegetarian, int calories, Type type) {
        this.name = name;
        this.vegetarian = vegetarian;
        this.calories = calories;
        this.type = type;
    }
    public String getName() {
        return this.name;
    }
    public boolean isVegetarian() {
        return this.vegetarian;
    }
    public int getCalories() {
        return this.calories;
    }
    public Type getType() {
        return this.type;
    }
	@Override
	public String toString() {
		return "Dish [name=" + name + ", vegetarian=" + vegetarian + ", calories=" + calories + ", type=" + type + "]";
	}
    
}
public class Program {
	public static List<Dish> getMenuList( ){
		 List<Dish> menu = Arrays.asList( new Dish("pork", false, 800, Dish.Type.MEAT),
                           new Dish("beef", false, 700, Dish.Type.MEAT),
                           new Dish("chicken", false, 400, Dish.Type.MEAT),
                           new Dish("french fries", true, 530, Dish.Type.OTHER),
                           new Dish("rice", true, 350, Dish.Type.OTHER),
                           new Dish("season fruit", true, 120, Dish.Type.OTHER),
                           new Dish("pizza", true, 550, Dish.Type.OTHER),
                           new Dish("prawns", false, 400, Dish.Type.FISH),
                           new Dish("salmon", false, 450, Dish.Type.FISH));
		 return menu;
	}
	public static void main7(String[] args) {
		List<Dish> menu = Program.getMenuList();
		Integer sum = menu.stream()
			.filter(d-> !d.isVegetarian())
			.filter(d->d.getCalories() > 500 )
			.map(Dish::getCalories)
			.reduce(0, Integer::sum);
		System.out.println("Sum	:	"+sum);
	}
	public static void main6(String[] args) {
		List<Dish> menu = Program.getMenuList();
		Optional<Integer> sum = menu.stream()
			.filter(d-> !d.isVegetarian())
			.filter(d->d.getCalories() > 500 )
			.map(Dish::getCalories)
			.reduce(( n1, n2 )-> n1 + n2 );
		
		if( sum.isPresent())
			System.out.println(sum.get());
	}
	public static void main5(String[] args) {
		//R apply(T t)

		List<Dish> menuList = Program.getMenuList();
		List<String> dishNames = menuList.stream()	
			.filter( Dish::isVegetarian )
			.map(Dish::getName)
			.collect(Collectors.toList());
		
		dishNames.forEach(System.out::println);
	}
	public static void main4(String[] args) {
		//R apply(T t)

		List<Dish> menuList = Program.getMenuList();
		menuList.stream()	
			.filter( Dish::isVegetarian )
			.map(Dish::getName)
			.forEach(System.out::println);
	}
	public static void main3(String[] args) {
		List<Dish> menuList = Program.getMenuList();
		menuList.stream()	
		.filter( Dish::isVegetarian )
		.collect(Collectors.toList())
		.forEach(System.out::println);
	}
	public static void main(String[] args) {
		List<Dish> menuList = Program.getMenuList();
		menuList.stream()	
		//.sorted( Comparator.comparing(Dish::getName))
		.sorted( Comparator.comparing(Dish::getCalories))
		.collect(Collectors.toList())
		.forEach(System.out::println);
	}
	public static void main2(String[] args) {
		List<Dish> menuList = Program.getMenuList();
		
		/* long count = menuList.stream()	
					.filter(d->d.isVegetarian())
					.count(); */
		
		long count = menuList.stream()	
				.filter( Dish::isVegetarian )
				.count();
		System.out.println("Dish Count	:	"+count);
		
	}
	public static void main1(String[] args) {
		List<Dish> menuList = Program.getMenuList();
		
		long count = menuList.stream()	
					.count();
		System.out.println("Dish Count	:	"+count);
		
	}
}
