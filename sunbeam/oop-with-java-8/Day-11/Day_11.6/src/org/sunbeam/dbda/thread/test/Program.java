package org.sunbeam.dbda.thread.test;
class Task extends Thread{
	public Task( String name ) {
		//super( name );
		this.setName(name);
		this.start();
	}
	@Override
	public void run() {
		System.out.println("Inside run method");
	}
}
public class Program {
	public static void main(String[] args) {
		Task th1 = new Task("User Thread-1");
	}
}
