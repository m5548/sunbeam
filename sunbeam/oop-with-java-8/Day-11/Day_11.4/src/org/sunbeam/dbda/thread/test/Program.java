package org.sunbeam.dbda.thread.test;

public class Program {
	public static void main(String[] args) {
		Thread th1 = new Thread( ( )->System.out.println("Inside run method") );
		th1.start();
	}
	public static void main8(String[] args) {
		Runnable target = ( )->System.out.println("Inside run method");
		Thread th1 = new Thread( target );
		th1.start();
	}
	public static void main7(String[] args) {
		Thread th1 = new Thread(new Runnable() {	
			@Override
			public void run() {
				System.out.println("Inside run");
			}
		});
		th1.start();
	}
	public static void main6(String[] args) {
		//Method Local Inner class
		class Task implements Runnable{
			@Override
			public void run() {
				//This is business logic method
				System.out.println("Inside run method");
			}
		}
		
		Runnable target = new Task( );	//Upcasting
		Thread th1 = new Thread(target, "User Thread-1" );	
		th1.start();	
	}
	public static void main5(String[] args) {
		Thread th1 = new Thread( "User Thread-1" );	//NEW State	
		th1.start();	//RUNNABLE
		th1.start();	//IllegalThreadStateException
	}
	public static void main4(String[] args) {
		Thread th1 = new Thread( "User Thread-1" );	//NEW State	
		th1.start();	//RUNNABLE
	}
	public static void main3(String[] args) {
		//Thread th1 = new Thread( "User Thread-1" );
		Thread th1 = new Thread( );
		th1.setName("User Thread-1");		
		System.out.println(th1.getName());	//User Thread-1
	}
	public static void main2(String[] args) {
		Thread th1 = new Thread();
		String state = th1.getState().name();
		System.out.println(state);	//NEW
	}
	public static void main1(String[] args) {
		Thread th1 = new Thread();
		System.out.println(th1.toString());//Thread[Thread-0,5,main]
	}
}
