package org.sunbeam.dbda.thread.test;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class TickTock{
	public static ReentrantLock lock = new ReentrantLock();
	final static Condition condition  = lock.newCondition(); 
	public void tick( boolean running ) throws InterruptedException {
		try{
			lock.lock();
			if( !running ) {
				condition.signal();
				return;
			}
			System.out.print("Tick	");
			condition.signal();
			condition.await();
			
		}finally {
			lock.unlock();
		}
	}
	public void tock( boolean running ) throws InterruptedException {
		try {
			lock.lock();
			if( !running ) {
				condition.signal();
				return;
			}
			System.out.println("	Tock");
			condition.signal();
			condition.await();
		}finally {
			lock.unlock();
		}
	}
}
class Task implements Runnable{
	private Thread thread;
	public Task( String name ) {
		this.thread = new Thread( this, name );
		this.thread.start();
	}
	private static TickTock tt = new TickTock();
	@Override
	public void run() {
		try {
			if( Thread.currentThread().getName().equals("tickThread")) {
				for( int count = 1; count <= 5; ++ count ) {
					tt.tick( true );
					Thread.sleep(250 );
				}
				tt.tick( false );
			}else {
				for( int count = 1; count <= 5; ++ count ) {
					tt.tock( true );
					Thread.sleep(250 );
				}
				tt.tock( false );
			}
		} catch (InterruptedException cause) {
			throw new RuntimeException( cause );
		}
	}
}
public class Program {
	public static void main(String[] args)throws Exception{
		new Task("tickThread");
		new Task("tockThread");
	}
}
