package org.sunbeam.dbda.thread.test;
class Task extends Thread{
	public Task( String name ) {
		super(name);
		this.start();
	}
	@Override
	public void run()throws RuntimeException {
		System.out.println( this.getName()+"	"+this.getPriority());
	}
}
public class Program {
	public static void main(String[] args){
		Thread th1 = Thread.currentThread();
		th1.setPriority(th1.getPriority() + 6 ); //IllegalArgumentException
		System.out.println( th1.getName()+"	"+th1.getPriority());
	}
	public static void main1(String[] args){
		Thread th1 = Thread.currentThread();
		//th1.setPriority(th1.getPriority() + 3 );
		th1.setPriority(Thread.NORM_PRIORITY + 3 );
		System.out.println( th1.getName()+"	"+th1.getPriority());
		
		Task th2 = new Task( "User Thread" );
	}
}
