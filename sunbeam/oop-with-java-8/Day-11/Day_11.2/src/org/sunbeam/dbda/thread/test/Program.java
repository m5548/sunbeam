package org.sunbeam.dbda.thread.test;

import java.lang.Thread.State;
import java.util.Scanner;

class Test{
	//private Scanner sc;
	public Test() {
		System.out.println("Inside constructor of class Test");
		//sc = new Scanner(System.in);
	}
	public void print( ) {
		System.out.println("Inside print method of class Test");
	}
	@Override
	protected void finalize() throws Throwable {
		System.out.println("Inside finalize method of class Test");
		//sc.close();
		
		Thread thread = Thread.currentThread();
		
		String name = thread.getName();
		System.out.println("Name		:	"+name);
		
		int priority = thread.getPriority();
		System.out.println("Priority	:	"+priority);
		
		ThreadGroup group = thread.getThreadGroup();
		System.out.println("Group		:	"+group.getName());
		
		State state = thread.getState();
		System.out.println("State		:	"+state.name());
		
		boolean type = thread.isDaemon();
		System.out.println("Type		:	"+( type ? "Deamon Thread" : "User Thread"));
		
		boolean status = thread.isAlive();
		System.out.println("Status		:	"+(status ? "Alive" : "Dead"));
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = null;
		t = new Test( );
		t.print();
		t = null;	
		System.gc();//Request to GC to run here
	}
}
