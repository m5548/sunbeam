package org.sunbeam.dbda.thread.test;
class Calculator{
	private int[] arr;
	public Calculator( ) {
		this.arr = new int[ ] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
	}
	
	public int sum( int value ) throws InterruptedException {
		int result = 0;
		for( int index = 0; index < this.arr.length; ++ index ) {
			this.arr[ index ] = this.arr[ index ] + value;
			result = result + this.arr[ index ];
			System.out.println("Running total for "+Thread.currentThread().getName()+" is "+result);
			Thread.sleep(300);
		}
		return result;
	}
	
	public int sub( int value ) throws InterruptedException {
		int result = 0;
		for( int index = 0; index < this.arr.length; ++ index ) {
			this.arr[ index ] = this.arr[ index ] - value;
			result = result + this.arr[ index ];
			System.out.println("Running total for "+Thread.currentThread().getName()+" is "+result);
			Thread.sleep(300);
		}
		return result;
	}
	
	public int multiplication( int value ) throws InterruptedException {
		int result = 0;
		for( int index = 0; index < this.arr.length; ++ index ) {
			this.arr[ index ] = this.arr[ index ] * value;
			result = result + this.arr[ index ];
			System.out.println("Running total for "+Thread.currentThread().getName()+" is "+result);
			Thread.sleep(300);
		}
		return result;
	}
}
class Task implements Runnable{
	private Thread thread;
	public Task( String name ) {
		this.thread = new Thread( this, name );
		this.thread.start();
	}
	private static Calculator cal = new Calculator();
	@Override
	public void run() {
		try {
			synchronized( cal ) {
				int result = 0;
				//Critical section
				if( Thread.currentThread().getName().equals("SumThread")) {
					result = cal.sum(5);
				}else if( Thread.currentThread().getName().equals("SubThread")) {
					result = cal.sub( 3 );
				}else {
					result = cal.multiplication(2);
				}
				System.out.println("Result	:	"+result);
			}
			
		} catch (InterruptedException cause) {
			throw new RuntimeException( cause );
		}
	}
}
public class Program {
	public static void main(String[] args)throws Exception{
		new Task("SumThread");

		new Task("SubThread");
		
		new Task("MultiplicationThread");
	}
}
