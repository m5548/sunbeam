package org.sunbeam.dbda.thread.test;
class Task extends Thread{
	public Task( String name ) {
		this.setName(name);
		System.out.println(this.getName()+"	"+this.getState());
		this.start();
	}
	@Override
	public void run()throws RuntimeException {
		try {
			for( int count = 1; count <= 10; ++ count ) {
				System.out.println("Count	:	"+count);
				if(  count == 5 )
					return;
					//throw new InterruptedException( );
				Thread.sleep(500);	//Blocking Call : TIMED_WAITING
			}
		} catch (InterruptedException cause) {
			throw new RuntimeException( cause );
		}
	}
}
public class Program {
	public static void main(String[] args) throws InterruptedException {
		Task th1 = new Task("User Thread-1");

		while( true ) {
			System.out.println(th1.getName()+"	"+th1.getState());
			Thread.sleep(100);
		}
	}
}
