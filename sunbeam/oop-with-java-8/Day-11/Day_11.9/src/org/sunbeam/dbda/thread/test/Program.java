package org.sunbeam.dbda.thread.test;
class Task extends Thread{
	public Task( String name ) {
		this.setName(name);
		this.start();
	}
	@Override
	public void run()throws RuntimeException {
		System.out.println("Starting "+this.getName());
		try {
			for( int count = 1; count <= 10; ++ count ) {
				System.out.println(this.getName()+"	:	"+count);
				Thread.sleep(500);
			}
		} catch (InterruptedException cause) {
			throw new RuntimeException( cause );
		}
		System.out.println("Ending "+this.getName());
	}
}
public class Program {
	public static void main(String[] args)throws Exception{
		Task t1 = new Task( "First" );
		t1.join();
		Task t2 = new Task( "Second" );
		t2.join();
		Task t3 = new Task( "Third" );
		t3.join();
		Task t4 = new Task( "Fourth" );
		t4.join();
		
	}
}
