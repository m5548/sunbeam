package org.sunbeam.dbda.thread.test;
class TickTock{
	public void tick( ) throws InterruptedException {
		synchronized( this ) {
			System.out.print("Tick	");
			this.notify();
			this.wait(1000);
		}
	}
	public void tock( ) throws InterruptedException {
		synchronized( this ) {
			System.out.println("	Tock");
			this.notify();
			this.wait(1000);
		}
	}
}
class Task implements Runnable{
	private Thread thread;
	public Task( String name ) {
		this.thread = new Thread( this, name );
		this.thread.start();
	}
	private static TickTock tt = new TickTock();
	@Override
	public void run() {
		try {
			if( Thread.currentThread().getName().equals("tickThread")) {
				for( int count = 1; count <= 5; ++ count ) {
					tt.tick();
					Thread.sleep(250 );
				}
			}else {
				for( int count = 1; count <= 5; ++ count ) {
					tt.tock();
					Thread.sleep(250 );
				}
			}
		} catch (InterruptedException cause) {
			throw new RuntimeException( cause );
		}
	}
}
public class Program {
	public static void main(String[] args)throws Exception{
		new Task("tickThread");
		new Task("tockThread");
	}
}
