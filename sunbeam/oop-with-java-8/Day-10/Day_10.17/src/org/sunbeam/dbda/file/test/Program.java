package org.sunbeam.dbda.file.test;
//Top Level class
class Outer{	//Outer.class
	//Non static nested class / Inner class
	class Inner{	//Outer$Inner.class
		
	}
}
public class Program {
	public static void main(String[] args) {
		//Create instance of non static nested class / Inner class
		//Hint : For simplicity, consider, non static nested class, as a non static method of class.
		
		Outer out = new Outer( );
		Outer.Inner in1 = out.new Inner( );	//OK
		
		Outer.Inner in2 = new Outer( ).new Inner( );	//Ok
	}
	public static void main1(String[] args) {
		//Create instance of class Outer
		Outer out = null;
		out = new Outer( );
		
		//Outer out = new Outer( );
	}
}
