package org.sunbeam.dbda.file.test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Program {
	public static String toString( File file ) {
		long lastModified = file.lastModified();
		Date date = new Date(lastModified);
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
		String str = sdf.format(date);
		return str;
 	}
	private static void printFileInfo(File file) {
		System.out.println("Name		:	"+file.getName());
		System.out.println("Parent		:	"+file.getParent());
		System.out.println("Length		:	"+file.length());
		System.out.println("Last Modified	:	"+Program.toString(file));
	}

	private static void printDirectoyInfo(File file) {
		File[] files = file.listFiles();
		for (File f : files) {
			if( !f.isHidden())
				System.out.println(f.getName());
		}
	}
	public static void main(String[] args) {
		///Users/sandeepkulange/Desktop/URL.txt
		try( Scanner sc = new Scanner(System.in)){
			System.out.print("Enter path : ");
			String pathname = sc.nextLine();
			File file = new File(pathname);
			if( file.exists()) {
				if( file.isDirectory()) {
					Program.printDirectoyInfo( file );
				}else if( file.isFile()){
					Program.printFileInfo( file );
				}else
					System.out.println("Different path");
			}else 
				System.out.println(pathname+" does not exist.");
			
		}
	}
}
