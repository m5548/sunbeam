package org.sunbeam.dbda.file.test;
@FunctionalInterface
interface Function{
	String changeCase( String str );
}
public class Program {
	public static void main(String[] args) {
		Function function = String::toUpperCase;
		String str = function.changeCase("SunBeam");
		System.out.println(str);
	}
	public static void main1(String[] args) {
		Function function = str-> str.toUpperCase();
		String str = function.changeCase("SunBeam");
		System.out.println(str);
	}
}
