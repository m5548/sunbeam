package org.sunbeam.dbda.collection.model;

import java.util.Comparator;

public class CompareByEmpid implements Comparator<Employee>{
	@Override
	public int compare(Employee emp1, Employee emp2) {
		return emp1.getEmpid() - emp2.getEmpid();
	}
}
