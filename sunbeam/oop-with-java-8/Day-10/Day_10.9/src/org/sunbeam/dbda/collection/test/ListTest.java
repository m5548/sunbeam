package org.sunbeam.dbda.collection.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.sunbeam.dbda.collection.model.Employee;

public class ListTest {
	private List<Employee> empList;	//null
	public void setEmpList(List<Employee> empList) {
		this.empList = empList;
	}
	public void addRecord(Employee[] arr) {
		if( this.empList != null ) {
			if( arr != null ) {
				for( Employee emp : arr )
					this.empList.add(emp);
			}
		}
	}
	public Employee findRecord(int empid ) {
		Employee key = new Employee();
		key.setEmpid(empid);
		if( this.empList.contains(key)) {
			int index = this.empList.indexOf(key);
			return this.empList.get(index);
		}
		return null;
	}
	public boolean removeRecord(int empid) {
		Employee key = new Employee();
		key.setEmpid(empid);
		if( this.empList.contains(key)) {
			int index = this.empList.indexOf(key);
			this.empList.remove(index);
			return true;
		}
		return false;
	}
	public void printRecord(Comparator<Employee> comparator) {
		if( this.empList != null ) {
			this.empList.sort(comparator);
			this.empList.forEach(System.out::println);
		}
	}
	public void save(String pathame) throws Exception{
		try( ObjectOutputStream outputStream = new ObjectOutputStream( new BufferedOutputStream(new FileOutputStream(new File( pathame))))){
			outputStream.writeObject(this.empList);
		}
	}
	@SuppressWarnings("unchecked")
	public void load(String pathname) throws Exception{
		try( ObjectInputStream inputStream = new ObjectInputStream( new BufferedInputStream(new FileInputStream(new File( pathname ))))){
			this.empList = (List<Employee>) inputStream.readObject();
		}
	}
}
