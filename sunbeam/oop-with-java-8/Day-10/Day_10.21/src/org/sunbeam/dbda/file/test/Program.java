package org.sunbeam.dbda.file.test;
//Top Level class
class Outer{	//Outer.class
	//Static Nested class
	static class Inner{	//Outer$Inner.class
	}
}
/*
 If implementation of nested class do not depend on implementation of top level class 
 then we should declare nested class static.
 Hint : For the simplicity, consider static nested class as a static method of a class.  
 */
public class Program {
	public static void main(String[] args) {
		//Create instance of top level class
		Outer out = new Outer( );
		
		//Create instance of static nested class
		Outer.Inner in = new Outer.Inner( );
	}
}
