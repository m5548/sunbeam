package org.sunbeam.dbda.file.test;

@FunctionalInterface
interface Function{
	String get( char[] data );
}
public class Program {
	public static void main(String[] args) {
		Function function = String::new;	//Constructor reference
		char[] data = { 'D','B', 'D', 'A'};
		String str = function.get(data);
		System.out.println(str);
	}
	public static void main3(String[] args) {
		Function function = ( data )->new String(data);
		char[] data = { 'D','B', 'D', 'A'};
		String str = function.get(data);
		System.out.println(str);
	}
	public static void main2(String[] args) {
		Function function = ( data )->{
			return new String(data);
		};
		char[] data = { 'D','B', 'D', 'A'};
		String str = function.get(data);
		System.out.println(str);
	}
	public static void main1(String[] args) {
		Function function = ( data )->{
			String str = new String(data);
			return str;
		};
		char[] data = { 'D','B', 'D', 'A'};
		String str = function.get(data);
		System.out.println(str);
	}
}
