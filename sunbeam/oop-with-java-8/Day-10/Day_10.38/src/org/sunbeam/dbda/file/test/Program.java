package org.sunbeam.dbda.file.test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Program {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(10, 20, 30, 40, 50);
		
		//void accept(T t) -> Functional method of Consumer
		
		/* Consumer< Integer > action = System.out::println;
		list.forEach(action); */
		
		list.forEach(System.out::println);
	
	}
	public static void main2(String[] args) {
		List<Integer> list = Arrays.asList(10, 20, 30, 40, 50);
		//void accept(T t) -> Functional method of Consumer
		//Consumer< Integer > action = ( Integer element)-> System.out.println(element);
		//Consumer< Integer > action = ( element)-> System.out.println(element);
		
		/* Consumer< Integer > action = element -> System.out.println(element);
		list.forEach(action); */
		
		list.forEach( e->System.out.println(e) );	//Behavior Parameterization
	}
	public static void main1(String[] args) {
		List<Integer> list = Arrays.asList(10, 20, 30, 40, 50);
		for( Integer element  : list )
			System.out.println(element);
	}
}
