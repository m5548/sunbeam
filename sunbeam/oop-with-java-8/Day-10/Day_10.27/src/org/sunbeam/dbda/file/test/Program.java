package org.sunbeam.dbda.file.test;

public class Program {	//Program.class
	public static void main(String[] args) {
		Object obj = new Object( ) {	//Program$1.class
			private String message = "Hello";
			@Override
			public String toString() {
				return message;
			}
		};
		
		String str = obj.toString();
		System.out.println(str);	//Hello
	}
	public static void main1(String[] args) {
		//Object obj = null;	//Reference
		
		//new Object( );	//Anonymous Instance
		
		//Object obj = new Object( );	//Instance with reference.
	}
}
