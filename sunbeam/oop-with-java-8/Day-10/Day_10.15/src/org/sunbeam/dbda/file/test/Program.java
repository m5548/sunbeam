package org.sunbeam.dbda.file.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class Program {
	public static Map<String, Integer> getWords(String pathname) throws Exception {
		Map<String, Integer> map = new TreeMap<String, Integer>( );
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(pathname)))) {
			String line = null;
			String delim = " {};<>!=().,/[]++\"";
			while ((line = reader.readLine()) != null) {
				StringTokenizer stk = new StringTokenizer( line, delim);
				while( stk.hasMoreTokens()) {
					String token = stk.nextToken().trim();
					if( !token.isEmpty()) {
						if( map.containsKey(token)) {
							Integer value =  map.get(token);
							map.put(token, ( value.intValue() + 1 ) );
						}else {
							map.put(token, 1);
						}
					}
				}
			}
		}
		return map;
	}
	public static void main(String[] args) {
		String pathname = "../Day_10.15/src/org/sunbeam/dbda/file/test/Program.java";
		try {
			Map<String,Integer> map = Program.getWords(pathname);
			Set<Entry<String, Integer>> entries = map.entrySet();
			for (Entry<String, Integer> entry : entries) {
				System.out.printf("%-15s%-5d\n", entry.getKey(), entry.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
