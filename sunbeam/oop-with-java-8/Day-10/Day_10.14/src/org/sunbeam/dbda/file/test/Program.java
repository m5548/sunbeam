package org.sunbeam.dbda.file.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Program {
	public static void readRecord(String pathname, String keyword) throws Exception {
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(pathname)))) {
			String line = null;
			int lineNumber = 0;
			while ((line = reader.readLine()) != null) {
				++ lineNumber;
				if( line.contains(keyword)) {
					System.out.println(lineNumber+". "+line.trim());
				}
			}
		}
	}
	public static void main(String[] args) {
		String pathname = "../Day_10.14/src/org/sunbeam/dbda/file/test/Program.java";
		String keyword = "public";
		try {
			Program.readRecord(pathname, keyword);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
