package org.sunbeam.dbda.file.test;

@FunctionalInterface
interface Printable{	//Functional Interface
	void print( );	//Functional method / Method descriptor
}

public class Program {
	public static void main(String[] args) {
		// In Java "->" operator is called lambda operator.
		//If expression contains -> operator then it is called lambda expression.
		/*
		  Consider following syntax:
		  
		  FunctionalInterface refName = //Lambda Expression / anonymous method
		  
		  FunctionalInterface refName =  ( Input Parameters )-> { Lambda Body; }
		  
		  FunctionalInterface refName =  ( Input Parameters ) goes to { Lambda Body; }
		  
		*/
		
		/* Printable p = (  )-> {
			String str = "Welcome to functional programming";
			System.out.println(str);
		}; */
		
		/* Printable p = (  )-> {
			System.out.println("Welcome to functional programming");
		}; */
		
		Printable p = (  )-> System.out.println("Welcome to functional programming");
		p.print( );
	}
	public static void main2(String[] args) {
		Printable p = new Printable() {	
			@Override
			public void print() {
				System.out.println("Inside print");
			}
		};
		p.print();
	}
	public static void main1(String[] args) {
		class Test implements Printable{
			@Override
			public void print() {
				System.out.println("Inside print");
			}
		}
		
		Printable p = new Test( );	//Upcasting
		p.print();
	}
}
