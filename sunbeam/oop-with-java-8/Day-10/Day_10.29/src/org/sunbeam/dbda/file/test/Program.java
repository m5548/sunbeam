package org.sunbeam.dbda.file.test;
interface Printable{
	public void print( );
}
public class Program {	//Program.class
	public static void main(String[] args) {
		Printable p = new Printable() {	
			@Override
			public void print() {
				System.out.println("Inside print.");
			}
		};
		p.print();
	}
}
