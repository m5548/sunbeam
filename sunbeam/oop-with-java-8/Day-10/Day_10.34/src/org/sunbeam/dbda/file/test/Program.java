package org.sunbeam.dbda.file.test;

@FunctionalInterface
interface IMath{
	int factorial( int number );
}
public class Program {
	public int fact( int number ) {
		int result = 1;
		for( int count = 1; count <= number; ++ count )
			result = result * count;
		return result;
	}
	
	public static void main(String[] args) {
		/* Program p = new Program( );
		int res = p.fact(5);	//method call */
		
		/* IMath math = number -> {
			int result = 1;
			for( int count = 1; count <= number; ++ count )
				result = result * count;
			return result;
		}; */
		
		Program p = new Program( );
		IMath math = p::fact;	//Method reference
		int result = math.factorial(5);
		System.out.println("Result	:	"+result);
	}
}
