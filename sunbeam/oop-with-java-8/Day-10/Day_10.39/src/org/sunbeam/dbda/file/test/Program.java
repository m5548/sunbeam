package org.sunbeam.dbda.file.test;
class Employee{
	private String name;
	private int empid;
	private float salary;
	public Employee() {
	}
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", empid=" + empid + ", salary=" + salary + "]";
	}
	
}

@FunctionalInterface
interface Function{
	Employee get( String name, int empid, float salary );
}
public class Program {
	public static void main(String[] args) {
		Function function = Employee::new;	//Constructor reference
		
		Employee emp = function.get("Sandeep", 33, 45000.50f );
		
		System.out.println(emp.toString());
	}
	public static void main3(String[] args) {
		Function function = (  name,  empid,  salary)->  new Employee(name, empid, salary);
		
		Employee emp = function.get("Sandeep", 33, 45000.50f );
		System.out.println(emp.toString());
	}
	public static void main2(String[] args) {
		Function function = (  name,  empid,  salary)-> {
			return  new Employee(name, empid, salary);
		};
		Employee emp = function.get("Sandeep", 33, 45000.50f );
		System.out.println(emp.toString());
	}
	public static void main1(String[] args) {
		Function function = (  name,  empid,  salary)-> {
			Employee emp = new Employee(name, empid, salary);
			return emp;
		};
		Employee emp = function.get("Sandeep", 33, 45000.50f );
		System.out.println(emp.toString());
	}
}
