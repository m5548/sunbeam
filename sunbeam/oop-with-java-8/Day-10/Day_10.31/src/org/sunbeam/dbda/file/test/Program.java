package org.sunbeam.dbda.file.test;

@FunctionalInterface
interface Printable{
	void print( String message );	//Functional method / Method descriptor
}

public class Program {
	public static void printMessage( Printable p, String message ) {
		p.print( message );
	}
	public static void main(String[] args) {
		//Behavior Parameterization
		Program.printMessage(message -> System.out.println( message ), "Good Evening!!");
		Program.printMessage(message -> System.out.println( message ), "Hello FP.");
	}
	public static void main1(String[] args) {
		//Printable p = ( String str )-> System.out.println( str );
		//Printable p = ( String message )-> System.out.println( message );
		//Printable p = ( message )-> System.out.println( message );
		Printable p =  message -> System.out.println( message );
		p.print("Good Evening!!");
	}
}
