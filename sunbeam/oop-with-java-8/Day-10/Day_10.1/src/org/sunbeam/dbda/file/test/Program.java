package org.sunbeam.dbda.file.test;

import java.io.File;
import java.io.IOException;

public class Program {
	public static void main(String[] args) {
		String pathname = "Dbda";
		File file = new File( pathname );
		boolean removedStatus = file.delete();
		if( removedStatus )
			System.out.println(pathname+" is deleted.");
	}
	public static void main3(String[] args) {
		String pathname = "Dbda";
		File file = new File( pathname );
		boolean creationStatus = file.mkdir();
		if( creationStatus )
			System.out.println("Directory is created.");
	}
	public static void main2(String[] args) {
		String pathname = "File.txt";
		File file = new File( pathname );
		boolean removedStatus = file.delete();
		if( removedStatus )
			System.out.println(pathname+" is deleted.");
	}
	public static void main1(String[] args) {
		try {
			String pathname = "File.txt";
			File file = new File( pathname );
			boolean creationStatus = file.createNewFile();
			if( creationStatus )
				System.out.println("File is created.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
