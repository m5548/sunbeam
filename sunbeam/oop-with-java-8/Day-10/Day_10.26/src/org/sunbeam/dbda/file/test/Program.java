package org.sunbeam.dbda.file.test;
public class Program {	//Program.class
	public static void main(String[] args) {
		//Method Local Inner Class
		class Complex{	//Program$1Complex.class	
			private int real;
			private int imag;
			public Complex() {
				// TODO Auto-generated constructor stub
			}
			public Complex(int real, int imag) {
				this.real = real;
				this.imag = imag;
			}
			public int getReal() {
				return real;
			}
			public void setReal(int real) {
				this.real = real;
			}
			public int getImag() {
				return imag;
			}
			public void setImag(int imag) {
				this.imag = imag;
			}
			@Override
			public String toString() {
				return "Complex [real=" + real + ", imag=" + imag + "]";
			}
		}
		
		Complex c1 = new Complex( );
		c1.setReal(10);
		c1.setImag(20);
		System.out.println(c1.toString());
	}
}
