package org.sunbeam.dbda.file.test;

//Access modifier of top level class can be public / default(package level private)
class Outer{	//Top Level class	=>	Outer.class
	//We can use any access modifier on nested class
	class Inner{	//Nested class	=>	Outer$Inner.class
		
	}
}
/*
Types of nested class:
 1. Non static nested class / Inner class
 	- If implementation of nested class depends on implementation of top level class then
 		we should declare nested class non static.
 2. Static nested class
 - If implementation of nested class do not depend on implementation of top level class then
 		we should declare nested class static.
*/
public class Program {
	public static void main(String[] args) {
	
	}
}
