package org.sunbeam.dbda.file.test;
class Employee{
	private String name;
	private int empid;
	private float salary;
	public Employee() {
	}
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
}

@FunctionalInterface
interface Function{
	String get( Employee emp );
}
public class Program {
	public static void main(String[] args) {
		Function function = Employee::getName;
		String name = function.get( new Employee("Sandeep", 33, 45000.50f ) );
		System.out.println(name);
		
	}
	public static void main1(String[] args) {
		Function function = emp -> emp.getName();
		String name = function.get( new Employee("Sandeep", 33, 45000.50f ) );
		System.out.println(name);
		
	}
}
