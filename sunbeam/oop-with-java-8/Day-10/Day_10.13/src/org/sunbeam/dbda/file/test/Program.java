package org.sunbeam.dbda.file.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Program {
	public static void readRecord(String pathname) throws Exception {
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(pathname)))) {
			String line = null;
			while ((line = reader.readLine()) != null)
				System.out.println(line);
		}
	}

	public static void main(String[] args) {
		String pathname = "../Day_10.13/src/org/sunbeam/dbda/file/test/Program.java";
		try {
			Program.readRecord(pathname);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
