package org.sunbeam.dbda.inheritance.test;

class Person{
	 String name;
	 int age;
	public Person( ) {
	}
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	public void showRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
}
class Employee extends Person{
	 int empid;
	 float salary;
	public Employee( ) {
	}
	public Employee(String name, int age, int empid, float salary) {
		super( name, age );
		this.empid = empid;
		this.salary = salary;
	}
	public void displayRecord( ) {
		super.showRecord();
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		Person p = new Employee( "Sandeep", 38, 33, 45000.50f); //Upcasting : OK
		System.out.println("Name	:	"+p.name); //OK
		System.out.println("Age	:	"+p.age); //OK
		Employee emp = (Employee) p;	//Downcasting
		System.out.println("Empid	:	"+emp.empid); //Not OK
		System.out.println("Salary	:	"+emp.salary); //OK
	}
	public static void main3(String[] args) {
		/* Employee emp = new Employee( "Sandeep", 38, 33, 45000.50f);
		Person p = emp;	//Upcasting // OK */
		
		Person p = new Employee( "Sandeep", 38, 33, 45000.50f); //Upcasting : OK
		
		System.out.println("Name	:	"+p.name); //OK
		System.out.println("Age	:	"+p.age); //OK
		//System.out.println("Empid	:	"+p.empid); //Not OK
		//System.out.println("Salary	:	"+p.salary); //OK
	}
	public static void main2(String[] args) {
		Employee emp = new Employee();
		emp.name = "Sandeep";
		emp.age = 38;
		emp.empid = 33;
		emp.salary = 45000.50f;
		
		//Person p = ( Person)emp;	//Upcasting //OK
		Person p = emp;	//Upcasting // OK
		
		System.out.println("Name	:	"+p.name); //OK
		System.out.println("Age	:	"+p.age); //OK
		//System.out.println("Empid	:	"+p.empid); //Not OK
		//System.out.println("Salary	:	"+p.salary); //OK
	}
	public static void main1(String[] args) {
		Employee emp = new Employee();
		emp.name = "Sandeep";
		emp.age = 38;
		emp.empid = 33;
		emp.salary = 45000.50f;
		
		System.out.println("Name	:	"+emp.name);
		System.out.println("Age	:	"+emp.age);
		System.out.println("Empid	:	"+emp.empid);
		System.out.println("Salary	:	"+emp.salary);
	}
}
