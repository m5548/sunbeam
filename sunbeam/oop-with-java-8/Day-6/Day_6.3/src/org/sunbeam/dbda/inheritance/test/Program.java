package org.sunbeam.dbda.inheritance.test;

class A{
	private int num1;
	public int getNum1() {
		return num1;
	}
	public void setNum1(int num1) {
		this.num1 = num1;
	}
}
class B extends A{
	public void print( ) {
		System.out.println(this.getNum1());
	}
}
public class Program {
	public static void main(String[] args) {
		B b = new B( );
		b.setNum1( 20 );
		b.print();
	}
	public static void main2(String[] args) {
		B b = new B( );
		b.setNum1( 20 );
		System.out.println(b.getNum1());
	}
	public static void main1(String[] args) {
		A a = new A( );
		a.setNum1(10);
		System.out.println("Num1	:	"+a.getNum1());
	}
}
