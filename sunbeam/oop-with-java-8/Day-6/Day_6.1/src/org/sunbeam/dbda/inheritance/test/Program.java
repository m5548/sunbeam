package org.sunbeam.dbda.inheritance.test;

public class Program {
	public static void main(String[] args) {
		//public Integer(int value)
		Integer i1 = new Integer(10);
		System.out.println(i1);
		
		//public Integer(String s)throws NumberFormatException
		Integer i2 = new Integer("10");
		System.out.println(i2);
		
		Integer i3 = new Integer("Sandeep"); //NumberFormatException
		
		//Integer i4 = new Integer( );	//The constructor Integer() is undefined
	}
}
